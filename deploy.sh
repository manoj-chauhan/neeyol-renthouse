PYTHON_VIRTUAL_ENVIRONMENT_PATH=~/aws
AWS_PROFILE=renthouse

source $PYTHON_VIRTUAL_ENVIRONMENT_PATH/bin/activate

npm run build
aws --profile $AWS_PROFILE s3 cp --recursive ./build s3://renthouse-webapp
aws --profile $AWS_PROFILE cloudfront create-invalidation \
    --distribution-id E7FPD7NVEMHI9 \
    --paths "/*"
