import { createStore, applyMiddleware } from "redux";
import rootReducer from "./_reducers";
import thunk from 'redux-thunk';

export default createStore(rootReducer, applyMiddleware(thunk));
