import config from "../config";
import handleResponseError from "./error.handler";

export const fileUtil = {
  uploadFile,
};

function uploadFile(file) {
  return new Promise((resolve, reject) => {
    const pica = require("pica")();
    var image = new Image();

    var objectUrl = window.URL.createObjectURL(file);
    image.src = objectUrl;

    image.addEventListener("load", () => {
      const resizedCanvas = document.createElement("canvas");
      resizedCanvas.height = 500;
      resizedCanvas.width = 500;

      pica
        .resize(image, resizedCanvas, {
          unsharpAmount: 80,
          unsharpRadius: 0.6,
          unsharpThreshold: 2,
        })
        .then((result) => pica.toBlob(result, "image/jpeg", 0.9))
        .then((blob) => {
          const formData = new FormData();
          formData.append("file", blob);
          return fetch(`${config.api.BASE_URL}/uploadPhoto`, {
            method: "POST",
            body: formData,
          })
            .then(handleResponseError)
            .then((response) => {
              console.log(response);
              resolve(response);
            });
        });
    });
  });
}
