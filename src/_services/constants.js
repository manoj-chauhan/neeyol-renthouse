export const errors = {
    AUTH_FAILURE: 'auth_failure',
    SERVICE_NOT_AVAILABLE: 'service_not_available',
    NETWORK_PROBLEM: 'network_problem',
    SOMETHING_WENT_WRONG: 'server_error'
}