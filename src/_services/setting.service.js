import config from '../config';
import {userData} from '../_helpers'
import handleResponseError from './error.handler';

export const settingService = {
    getSetting,
    setSetting
};


function getSetting(){
    const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
      };
      return fetch(`${config.api.BASE_URL}/settings`, requestOptions).then(
        handleResponseError
      );
}

function setSetting(setting){
    const requestOptions = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
        body: JSON.stringify({
            rent:parseInt(setting.rent),
            rentCycle:setting.rentCycle,
            electricityUnitCharges:parseInt(setting.electricity)
        })
      };
      return fetch(`${config.api.BASE_URL}/settings`, requestOptions).then(
        handleResponseError
      );
}