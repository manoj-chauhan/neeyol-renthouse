import { userService, errors as serviceErrors } from "../_services";
export default function handleResponseError(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = {
        code: serviceErrors.SOMETHING_WENT_WRONG,
        message: "something went wrong",
      };
      if (response.status === 401 || response.status === 403) {
        // auto logout if 401 response returned from api
        userService.logout();
        // location.reload(true);
        error.code = serviceErrors.AUTH_FAILURE;
      }
      error.message = (data && data.message) || response.statusText;
      return Promise.reject(error.message);
    }

    return data;
  });
}
