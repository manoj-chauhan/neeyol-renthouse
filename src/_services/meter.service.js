import config from "../config";
import { userData } from "../_helpers";
import handleResponseError from "./error.handler";
import { fileUtil } from "../_services/file.service";

export const meterService = {
  getAllMeters,
  getMeter,
  addMeter,
  allocateMeter,
  removeMeter,
  deallocateMeter,
  getRoomAllocationHistory,
  meterReading,
  getMetereReadingDetail,
  uploadReadingPhoto,
  updateMeter,
};

function getAllMeters() {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/meter`, requestOptions).then(
    handleResponseError
  );
}

function getMeter(meterId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/meter/` + meterId, requestOptions).then(
    handleResponseError
  );
}

function addMeter(meter) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      name: meter.name,
      initialReading: meter.initialReading,
      imageList: [],
    }),
  };

  return fetch(`${config.api.BASE_URL}/meter`, requestOptions).then(
    handleResponseError
  );
}

function allocateMeter(meter, imageId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      meterId: parseInt(meter.meterId),
      roomId: parseInt(meter.roomId),
      reading: parseInt(meter.initialReading),
      meterReadingImage: imageId,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/meter/` + meter.meterId + `/allocate`,
    requestOptions
  ).then(handleResponseError);
}

function deallocateMeter(meter, imageId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      reading: meter.LastReading,
      imageId: imageId,
      caption: meter.caption,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/meter/` + meter.meterId + `/deallocate`,
    requestOptions
  ).then(handleResponseError);
}

function getRoomAllocationHistory(meterId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/meter/` + meterId + `/allocationHistory`,
    requestOptions
  ).then(handleResponseError);
}

function meterReading(meterReadingInfo, imageId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      reading: parseInt(meterReadingInfo.Reading),
      imageId: imageId,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/meter/` + meterReadingInfo.meterId + `/reading`,
    requestOptions
  ).then(handleResponseError);
}

function getMetereReadingDetail(meterId, pageNo) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  let page = 0;
  if (pageNo) {
    page = page + pageNo;
    console.log("value of page is " + page);
  }
  return fetch(
    `${config.api.BASE_URL}/meter/` +
      meterId +
      `/reading?pageSize=` +
      5 +
      `&pageNo=` +
      page,
    requestOptions
  ).then(handleResponseError);
}

function removeMeter(buildingId) {}

function uploadReadingPhoto(meterReadingInfo) {
  return fileUtil.uploadFile(meterReadingInfo.image);
}

function updateMeter(meterInfo) {
  const requestOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      meterId: meterInfo.meterId,
      name: meterInfo.name,
    }),
  };
  return fetch(`${config.api.BASE_URL}/meter`, requestOptions).then(
    handleResponseError
  );
}
