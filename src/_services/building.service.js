import config from "../config";
import { userData } from "../_helpers";
import handleResponseError from "./error.handler";
import { fileUtil } from "./file.service";

export const buildingService = {
  getAllBuildings,
  getBuilding,
  createBuilding,
  updateBuilding,
  removeBuilding,
  getRooms,
  getRoom,
  addRoom,
  allocateRoom,
  deallocateRoom,
  getRoomAllocationHistory,
  getRoomAllocationDetail,
  updateRoom,
  removeRoom,
  getRoomMeterDetail,
  uploadBuildingPhoto,
  uploadRoomImage,
  getRoomList,
  addBuildindWizard,
};

function getAllBuildings() {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/building`, requestOptions).then(
    handleResponseError
  );
}

function getBuilding(id) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/building/` + id, requestOptions).then(
    handleResponseError
  );
}

function createBuilding(building, imageId) {
  let imageList = [];

  if (imageId) {
    imageList = [
      {
        imageId: imageId,
        caption: building.caption,
      },
    ];
  }
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      name: building.name,
      address: building.address,
      imageList: imageList,
    }),
  };

  return fetch(`${config.api.BASE_URL}/building`, requestOptions).then(
    handleResponseError
  );
}

function updateBuilding(building) {
  const requestOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      buildingId: building.id,
      name: building.name,
      address: building.address,
    }),
  };

  return fetch(`${config.api.BASE_URL}/building`, requestOptions).then(
    handleResponseError
  );
}

function removeBuilding(buildingId) {}

function getRooms(buildingId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  let url = `${config.api.BASE_URL}/building/` + buildingId + `/room`;
  return fetch(url, requestOptions).then(handleResponseError);
}
function getRoom(roomId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  let url = `${config.api.BASE_URL}/room/` + roomId;
  return fetch(url, requestOptions).then(handleResponseError);
}
function addRoom(room, imageId) {
  let imageList = [];

  if (imageId) {
    imageList = [
      {
        imageId: imageId,
        caption: "room image of " + room.name,
      },
    ];
  }

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      name: room.name,
      buildingId: room.buildingId,
      imageList: imageList,
    }),
  };

  return fetch(`${config.api.BASE_URL}/room`, requestOptions).then(
    handleResponseError
  );
}

function allocateRoom(room, imageId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      tenantId: room.tenantId,
      roomId: parseInt(room.roomId),
      amount: room.amount,
      allocationTime: room.allocationTime,
      rentCycle: room.rentCycle,
      electricityUnitCharges: room.electricityUnitCharges,
      reading: room.reading,
      meterReadingImage: imageId,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/room/` + room.roomId + `/allocate`,
    requestOptions
  ).then(handleResponseError);
}

function deallocateRoom(room, imageId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      roomId: parseInt(room.roomId),
      reading: parseInt(room.meterReading),
      imageId: imageId,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/room/` + room.roomId + `/deallocate`,
    requestOptions
  ).then(handleResponseError);
}

function getRoomAllocationHistory(roomId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/room/` + roomId + `/allocationHistory`,
    requestOptions
  ).then(handleResponseError);
}

function getRoomAllocationDetail(roomId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/room/` + roomId + `/allocation`,
    requestOptions
  ).then(handleResponseError);
}

function getRoomMeterDetail(roomId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/room/` + roomId + `/meter/`,
    requestOptions
  ).then(handleResponseError);
}

function updateRoom(roomInfo) {
  const requestOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      roomId: roomInfo.roomId,
      name: roomInfo.name,
      available: roomInfo.isAvailable,
    }),
  };
  return fetch(`${config.api.BASE_URL}/room/`, requestOptions).then(
    handleResponseError
  );
}
function removeRoom() {}

function uploadBuildingPhoto(caption, buildingId, imageId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      imageId: imageId,
      caption: caption,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/building/` + buildingId + `/image/`,
    requestOptions
  ).then(handleResponseError);
}

function uploadRoomImage(roomId, imageId, caption) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      imageId: imageId,
      caption: caption,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/room/` + roomId + `/image`,
    requestOptions
  ).then(handleResponseError);
}

function getRoomList() {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/room/`, requestOptions).then(
    handleResponseError
  );
}

function addBuildindWizard(buildingWizardDetails) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      buildingName: buildingWizardDetails.buildingName,
      buildingAddress: buildingWizardDetails.buildingAddress,
      roomList: buildingWizardDetails.roomList,
    }),
  };

  return fetch(`${config.api.BASE_URL}/buildingWizard`, requestOptions).then(
    handleResponseError
  );
}
