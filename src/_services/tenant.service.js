import config from "../config";
import { userData } from "../_helpers";
import handleResponseError from "./error.handler";
import { fileUtil } from "./file.service";

export const tenantService = {
  getAllTenant,
  getTenant,
  addTenant,
  getTenantAccount,
  getAllocatedRooms,
  tenantdues,
  getTenantDocuments,
  uploadDocument,
  uploadContact,
  getContacts,
  delImage,
  delContact,
  deleteDocument,
  getAddresses,
  uploadAddress,
  deleteAddress,
  updateAddress,
  addRelative,
  getTenantRelatives,
  deleteRelative,
  getRelationships,
  uploadPhoto,
  updateTenantProfile,
};

function getAllTenant() {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/tenant`, requestOptions).then(
    handleResponseError
  );
}

function getTenant(tenantId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId,
    requestOptions
  ).then(handleResponseError);
}

function addTenant(tenant, imageId, caption) {
  let imageList = [];

  if (imageId) {
    imageList = [
      {
        imageId: imageId,
        caption: caption,
      },
    ];
  }
  const imageDetails = {
    imageId: imageId,
    caption: caption,
  };

  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      name: tenant.name,
      imageList: imageList,
    }),
  };

  return fetch(`${config.api.BASE_URL}/tenant`, requestOptions).then(
    handleResponseError
  );
}

function getTenantAccount(tenantId) {}

function getAllocatedRooms(tenantId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/room`,
    requestOptions
  ).then(handleResponseError);
}

function tenantdues() {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/tenant/dues`, requestOptions).then(
    handleResponseError
  );
}

function getTenantDocuments(tenantId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/document`,
    requestOptions
  ).then(handleResponseError);
}

function getContacts(tenantId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/contacts`,
    requestOptions
  ).then(handleResponseError);
}

function uploadDocument(tenantDocumentDetails, tenantId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      tenantId: tenantId,
      documentId: tenantDocumentDetails.tenantDocumentId,
      fileId: tenantDocumentDetails.fileId,
      description: tenantDocumentDetails.description,
    }),
  };

  return fetch(`${config.api.BASE_URL}/document`, requestOptions).then(
    handleResponseError
  );
}

function uploadContact(tenantContactDetails, tenantId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      contactId: tenantContactDetails.tenantContactId,
      phoneNumber: tenantContactDetails.phoneNumber,
      tenantId: tenantId,
      type: tenantContactDetails.type,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/tenant.tenantId/contacts`,
    requestOptions
  ).then(handleResponseError);
}

function delImage(tenant) {
  const requestOptions = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` +
      tenant.tenantId +
      `/image/` +
      tenant.delImage,
    requestOptions
  ).then(handleResponseError);
}

function delContact(tenant) {
  const requestOptions = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` +
      tenant.tenantId +
      `/contacts/` +
      tenant.contactId,
    requestOptions
  ).then(handleResponseError);
}
function deleteDocument(tenantId, documentId) {
  const requestOptions = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/document/` + documentId,
    requestOptions
  ).then(handleResponseError);
}

function getAddresses(tenantId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/address`,
    requestOptions
  ).then(handleResponseError);
}

function uploadAddress(address, tenantId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      addressId: address.tenantAddressId,
      tenantId: tenantId,
      address: address.address,
      type: address.type,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/address`,
    requestOptions
  ).then(handleResponseError);
}

function deleteAddress(tenantId, addressId) {
  const requestOptions = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` +
      tenantId +
      `/address/address/` +
      addressId,
    requestOptions
  ).then(handleResponseError);
}

function updateAddress(tenantId, tenantAddressId, type, address) {
  const requestOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      tenantAddressId: tenantAddressId,
      address: address,
      type: type,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/address`,
    requestOptions
  ).then(handleResponseError);
}

function addRelative(tenantId, relativeId, relationshipId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      relationshipId: relationshipId,
      forwardRelative: tenantId,
      backwardRelative: relativeId,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/relationship`,
    requestOptions
  ).then(handleResponseError);
}
function getTenantRelatives(tenantId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/relationship`,
    requestOptions
  ).then(handleResponseError);
}
function deleteRelative(tenantId, relationshipId) {
  const requestOptions = {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({}),
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` +
      tenantId +
      `/relationship/` +
      relationshipId,
    requestOptions
  ).then(handleResponseError);
}

function getRelationships() {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  return fetch(`${config.api.BASE_URL}/relationship`, requestOptions).then(
    handleResponseError
  );
}

function uploadPhoto(caption, tenantId, imageId) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      caption: caption,
    }),
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/image/` + imageId,
    requestOptions
  ).then(handleResponseError);
}

function updateTenantProfile(tenantProfileInfo) {
  const requestOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      tenantId: tenantProfileInfo.tenantId,
      name: tenantProfileInfo.name,
    }),
  };

  return fetch(`${config.api.BASE_URL}/tenant`, requestOptions).then(
    handleResponseError
  );
}
