import config from '../config';
import {userData} from '../_helpers'
import handleResponseError from './error.handler';
import {fileUtil} from '../_services/file.service';

export const userService = {
    login,
    logout,
    register,
    userDetail,
    uploadUserPhoto,
    changeUserDetails,
    changePassword
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`${config.api.BASE_URL}/api/authenticate`, requestOptions)
      .then(handleResponseError)
      .then((user) => {
        userData.setAuthToken(user.authToken);
        // return user;
       });
}

function logout() {
    userData.clear();
}

function register(user, photoId) {

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            name: user.name,
            gender: user.gender,
            age: user.age,
            dateOfBirth: user.dateOfBirth,
            photoId: photoId,
            userName: user.username,
            password: user.password
        })
    };

    return fetch(`${config.api.BASE_URL}/user`, requestOptions)
    .then(handleResponseError);    
}

function userDetail(){
    const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
      };
      return fetch(`${config.api.BASE_URL}/user`, requestOptions).then(
        handleResponseError
      );
}

function uploadUserPhoto(userInfo) {
   return fileUtil.uploadFile(userInfo.image);
}

function changeUserDetails(userInfo) {
    const requestOptions = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
        body: JSON.stringify({
            name: userInfo.name,
            gender: userInfo.gender,
            dateOfBirth: userInfo.dateOfBirth,
            photoId: userInfo.photoId,
            userName: userInfo.userName
        })
      };
      return fetch(`${config.api.BASE_URL}/user`, requestOptions)
      .then( handleResponseError);   
}

function changePassword(changePasswordInfo) {
    const requestOptions = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
        body: JSON.stringify({
            currentPassword: changePasswordInfo.currentPassword,
            changePassword: changePasswordInfo.changePassword
        })
    };
      return fetch(`${config.api.BASE_URL}/password`, requestOptions)
      .then( handleResponseError);
}