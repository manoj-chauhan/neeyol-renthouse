import config from "../config";
import { userData } from "../_helpers";
import handleResponseError from "./error.handler";
import moment from "moment";

export const bookService = {
  recordPayment,
  recordMaintanance,
  recordUtility,
  recordBalance,
  getPayments,
  getTransactions,
  getStatement,
  getMaintanance,
  getUtility,
  getDiscount,
  getRoomList,
};

function recordPayment(tenantId, amount, comment, Reading, Discount) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
    body: JSON.stringify({
      tenantId: parseInt(tenantId),
      paidAmount: parseInt(amount),
      comment: comment,
      time: moment.utc(),
      meterReadingList: Reading,
      discount: Discount,
    }),
  };

  return fetch(`${config.api.BASE_URL}/payment`, requestOptions).then(
    handleResponseError
  );
}

function recordMaintanance(maintanance) {
  return fetch(`${config.api.BASE_URL}/account`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  })
    .then(handleResponseError)
    .then((response) => {
      let debitAccount = response.filter((info) => {
        return info.accountType === "MAINTENANCE";
      })[0].accountId;

      let creditAccount = response.filter((info) => {
        return info.accountType === "CASH";
      })[0].accountId;

      const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
        body: JSON.stringify({
          creditAccount: creditAccount,
          debitAccount: debitAccount,
          amount: maintanance.amount,
          comment: maintanance.comment,
          transactionTime: maintanance.time,
        }),
      };

      return fetch(`${config.api.BASE_URL}/transactions`, requestOptions).then(
        handleResponseError
      );
    });
}

function recordUtility(utility) {
  return fetch(`${config.api.BASE_URL}/account`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  })
    .then(handleResponseError)
    .then((response) => {
      let creditAccount = response.filter((info) => {
        return info.accountType === "CASH";
      })[0].accountId;

      let debitAccount = response.filter((info) => {
        return info.accountType === "UTILITY";
      })[0].accountId;

      const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
        body: JSON.stringify({
          creditAccount: creditAccount,
          debitAccount: debitAccount,
          amount: utility.amount,
          comment: utility.comment,
          transactionTime: utility.time,
        }),
      };

      return fetch(`${config.api.BASE_URL}/transactions`, requestOptions).then(
        handleResponseError
      );
    });
}

function recordBalance(tenantBalanceDetails) {
  return fetch(`${config.api.BASE_URL}/account`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  })
    .then(handleResponseError)
    .then((response) => {
      let creditAccount = response.filter((info) => {
        return info.accountType === "TENANT_DUES";
      })[0].accountId;

      const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
        body: JSON.stringify({
          creditAccount: creditAccount,
          debitAccount: tenantBalanceDetails.tenantAccountId,
          amount: tenantBalanceDetails.balance,
          comment: tenantBalanceDetails.comment,
          time: tenantBalanceDetails.time,
        }),
      };

      return fetch(`${config.api.BASE_URL}/transactions`, requestOptions).then(
        handleResponseError
      );
    });
}

function getPayments(pageNo) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };

  let page = 0;
  if (pageNo) {
    page = page + pageNo;
    console.log("value of page is " + page);
  }
  return fetch(
    `${config.api.BASE_URL}/payment?pageSize=` + 5 + `&pageNo=` + page,
    requestOptions
  ).then(handleResponseError);
}

function getTransactions(accountId, type) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };

  return fetch(
    `${config.api.BASE_URL}/account/` +
      accountId +
      `/transactions?type=` +
      type,
    requestOptions
  ).then(handleResponseError);
}

function getMaintanance(pageNo) {
  return fetch(`${config.api.BASE_URL}/account`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  })
    .then(handleResponseError)
    .then((response) => {
      let accountId = response.filter((info) => {
        return info.accountType === "MAINTENANCE";
      })[0].accountId;
      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
      };
      console.log("page no is ", pageNo);
      return fetch(
        `${config.api.BASE_URL}/account/` +
          accountId +
          `/transactions?type=debit&pageSize=${5}&pageNo=${pageNo}`,
        requestOptions
      ).then(handleResponseError);
    });
}

function getUtility(pageNo) {
  return fetch(`${config.api.BASE_URL}/account`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  })
    .then(handleResponseError)
    .then((response) => {
      let accountId = response.filter((info) => {
        return info.accountType === "UTILITY";
      })[0].accountId;

      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
      };
      let page = 0;
      if (pageNo) {
        page = page + pageNo;
      }
      return fetch(
        `${config.api.BASE_URL}/account/` +
          accountId +
          `/transactions?type=debit&pageSize=${5}&pageNo=${page}`,
        requestOptions
      ).then(handleResponseError);
    });
}

function getDiscount() {
  return fetch(`${config.api.BASE_URL}/account`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  })
    .then(handleResponseError)
    .then((response) => {
      let accountId = response.filter((info) => {
        return info.accountType === "DISCOUNT";
      })[0].accountId;

      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userData.getAuthToken(),
        },
      };

      return fetch(
        `${config.api.BASE_URL}/account/` +
          accountId +
          `/transactions?type=debit`,
        requestOptions
      ).then(handleResponseError);
    });
}

function getStatement(accountId, pageNo) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };
  let page = 0;
  if (pageNo) {
    page = page + pageNo;
    console.log("value of page is " + page);
  }
  return fetch(
    `${config.api.BASE_URL}/account/` +
      accountId +
      `/transactions?pageSize=` +
      5 +
      `&pageNo=` +
      page,
    requestOptions
  ).then(handleResponseError);
}

function getRoomList(tenantId) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + userData.getAuthToken(),
    },
  };

  return fetch(
    `${config.api.BASE_URL}/tenant/` + tenantId + `/room`,
    requestOptions
  ).then(handleResponseError);
}
