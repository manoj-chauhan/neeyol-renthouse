import React, { useState, useEffect, Fragment } from "react";
import {
  List,
  Typography,
  Grid,
  Paper,
  ListItem,
  Divider,
} from "@material-ui/core";
import { connect } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { bookActions } from "../../_actions";
import moment from "moment";
import Link from "@material-ui/core/Link";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    margin: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    height: "100%",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  infoItem: {
    display: "flex",
    justifyContent: "flex-end",
  },
  editLinkButton: {
    padding: 20,
  },
  styleTenantName: {
    fontWeight: "bold",
  },
  alignNoPaymentIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 150,
  },
  noPaymentIcon: {
    fontSize: 150,
  },
}));

function PaymentList(props) {
  const loadPayments = props.loadPayments;
  const clearPaymentList = props.clearPaymentList;
  const classes = useStyles();
  const [pageNo, setPageNo] = useState(1);

  useEffect(() => {
    clearPaymentList();
    loadPayments();
  }, [loadPayments]);

  const handleSubmit = (e) => {
    setPageNo(pageNo + 1);
    loadPayments(pageNo);
  };

  return (
    <>
      {
        props.paymentList.length == 0 ? (
          <div className={classes.alignNoPaymentIcon}>
            <AttachMoneyIcon className={classes.noPaymentIcon} />
            <Typography>No Payment Has Added Yet</Typography>
          </div>
        ) : (
          <></>
        )
      }

      < List >
        {
          props.paymentList
            .sort((transaction1, transaction2) => {
              let moment1 = moment.utc(transaction1.time);
              let moment2 = moment.utc(transaction2.time);
              return moment1.diff(moment2) * -1;
            })
            .map((row) => (
              <Fragment>
                <ListItem>
                  <Grid container>
                    <Grid item xs={8}>
                      <Typography className={classes.styleTenantName}>
                        {row.tenantName
                          .split(" ")
                          .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                          .join(" ")}
                      </Typography>
                    </Grid>
                    <Grid item xs={4} className={classes.infoItem}>
                      <Typography>
                        {moment.utc(row.time).format("DD MMM YYYY")}
                      </Typography>
                    </Grid>
                    <Grid item xs={6}>
                      <Typography variant="subtitle2">Paid Amount</Typography>
                    </Grid>
                    <Grid item xs={6} className={classes.infoItem}>
                      <Typography>Rs. {row.paidAmount}</Typography>
                    </Grid>
                    <Grid item xs={6}>
                      <Typography variant="subtitle2">Discount</Typography>
                    </Grid>
                    <Grid item xs={6} className={classes.infoItem}>
                      <Typography variant="subtitle2">
                        Rs. {row.discount}
                      </Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="caption">{row.comment}</Typography>
                    </Grid>
                  </Grid>
                </ListItem>
                <Divider />
              </Fragment>
            ))
        }
        < Grid item xs={12} className={classes.editLinkButton} >
          {
            props.paymentList.length === 5 ? (
              <Link onClick={handleSubmit}>load more..</Link>
            ) : ""
          }
        </Grid >
      </List >
    </>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadPayments: (pageNo) => dispatch(bookActions.getPayments(pageNo)),
    clearPaymentList: () => dispatch(bookActions.clearPaymentList()),
  };
};

const mapStateToProps = (state) => {
  return {
    paymentList: state.book.paymentList,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(PaymentList)
);

export { x as PaymentList };
