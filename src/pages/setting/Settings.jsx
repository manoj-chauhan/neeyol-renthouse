import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import { Grid, Paper, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";
import { settingActions } from "../../_actions";
import { Select, MenuItem, Snackbar } from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  personalInfo: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: theme.palette.background.paper,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paper: {},
  fixedHeight: {
    height: 320,
  },
  btn: {
    float: "right",
    marginTop: theme.spacing(2),
  },
}));

function Settings(props) {
  const [rent, setRent] = React.useState(0);
  const [rentError, setRentError] = useState(0)
  const [electricity, setElectricity] = React.useState(0)
  const [electricityError, setElectricityError] = useState(false);
  const [rentCycle, setRentCycle] = React.useState("");
  const [rentCycleError, setRentCycleError] = useState(false);
  const [snackBarOpen, setSnackBarOpen] = React.useState(false);
  const [
    saveChangesButtonDisable,
    setSaveChangesButtonDisable,
  ] = React.useState(true);

  const getSetting = props.getSetting;
  const classes = useStyles();
  const defaultSetting = props.defaultSetting;

  React.useEffect(() => {
    getSetting();
  }, [getSetting]);

  React.useEffect(() => {
    if (defaultSetting) {
      setRentCycle(defaultSetting.rentCycle);
      setRent(defaultSetting.rent);
      setElectricity(defaultSetting.electricityUnitCharges);
    }
  }, [defaultSetting]);

  useEffect(() => {
    if (rent > 0 && electricity > 0 && rentCycle) {
      setSaveChangesButtonDisable(false);
    } else {
      setSaveChangesButtonDisable(true);
    }
  }, [rent, electricity, rentCycle])


  const handleSubmit = () => {
    props.setSetting({
      rent: rent,
      rentCycle: rentCycle,
      electricity: electricity,
    });
    setSaveChangesButtonDisable(true);
    setSnackBarOpen(true);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "rent") {
      setRent(value);
    }
    if (name === "rentCycle") {
      setRentCycle(value);
    }
    if (name === "electricity") {
      setElectricity(value);
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setSnackBarOpen(false);
  };

  return (
    <Grid container spacing={3} className={classes.root}>
      <Grid item xs={12} md={12} lg={12}>
        <FormControl fullWidth>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="rent"
            label="Rent"
            type="number"
            id="rent"
            autoFocus
            onChange={handleChange}
            value={rent}
          />
          <FormControl
            fullWidth
            margin="normal"
            variant="outlined"
            className={classes.formControl}
          >
            <InputLabel id="demo-simple-select-outlined-label">
              Rent Cycle
            </InputLabel>
            <Select
              name="rentCycle"
              displayEmpty
              value={rentCycle}
              onChange={handleChange}
              label="Rent Cycle"
            >
              <MenuItem value="per minute">Per Minute</MenuItem>
              <MenuItem value="daily">Daily</MenuItem>
              <MenuItem value="weekly">Weekly</MenuItem>
              <MenuItem value="monthly">Monthly</MenuItem>
            </Select>
          </FormControl>

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="electricity"
            label="Electricity unit charges"
            type="number"
            id="electricity"
            autoFocus
            onChange={handleChange}
            value={electricity}
          />

          <Button
            disabled={saveChangesButtonDisable}
            color="primary"
            variant="contained"
            className={classes.btn}
            onClick={handleSubmit}
          >
            Save
          </Button>

          <Snackbar
            open={snackBarOpen}
            autoHideDuration={2000}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center",
            }}
          >
            <Alert onClose={handleClose} severity="success">
              Save Changed Successfully!
            </Alert>
          </Snackbar>
        </FormControl>
      </Grid>
    </Grid>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    getSetting: () => dispatch(settingActions.getSetting()),
    setSetting: (setting) => dispatch(settingActions.setSetting(setting)),
  };
};

const mapStateToProps = (state) => {
  return {
    defaultSetting: state.setting.defaultSetting,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(Settings)
);

export { x as Settings };
