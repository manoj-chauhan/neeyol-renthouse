import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  Container,
  makeStyles,
  withStyles,
  Paper,
  Typography,
  CssBaseline,
  Grid,
  Button,
  ListItem,
  List,
  Divider,
  TextField,
  Dialog,
  InputLabel,
  Select,
  DialogContent,
  MenuItem,
  CardMedia,
  Link,
} from "@material-ui/core";
import { useParams } from "react-router-dom";
import { bookActions, tenantActions, meterActions } from "../../_actions";
import config from "../../config";
import moment from "moment";
import { Add } from "@material-ui/icons";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  title: {
    display: "flex",
    justifyContent: "center",
  },
  tenantName: {
    marginTop: 20,
    marginBottom: 20,
  },
  headers: {
    marginLeft: 20,
  },
  paper: {
    marginTop: 30,
    marginBottom: 10,
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing(2),
  },
  alignItems: {
    marginBottom: 10,
  },
  alignListItems: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  },
  payable: {
    marginTop: 10,
    width: "100%",
  },
  submitButton: {
    marginTop: 20,
    width: "100%",
  },
  addElectricityBillButton: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: 40,
  },
  cover: {
    height: 100,
    width: 100,
  },
  dialogPaper: {
    minWidth: "40vh",
    minHeight: "20vh",
  },
  alignTextFields: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

function DocumentDialog(props) {
  const { onClose, open } = props;
  const imageId = props.imageId ? props.imageId : null;
  return (
    <div>
      <Dialog
        onClose={(e) => {
          onClose();
        }}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <img src={config.api.BASE_URL + "/image/" + imageId} alt="" />
      </Dialog>
    </div>
  );
}

function ElectricityBillDialog(props) {
  const classes = useStyles();
  const { open, onClose } = props;
  const tenantAllocateRoomList = props.tenantAllocateRoomList;
  const [meterId, setMeterId] = useState(0);
  const [roomId, setRoomId] = useState(0);
  const [roomName, setRoomName] = useState(null);
  const [previousReading, setPreviousReading] = useState(0);
  const [currentReading, setCurrentReading] = useState(0);
  const [image, setImage] = useState(null);
  const [electricityBillDialogClose, setElectricityBillDialogClose] = useState(
    false
  );
  const [readingError, setReadingError] = useState(false);
  const [selectedRoomError, setSelectedRoomError] = useState(false);

  const readingInfo = props.readings.filter((meterReadingInfo) => {
    return meterReadingInfo.meterId === meterId;
  })[0];
  const { t, i18n } = useTranslation();

  useEffect(() => {
    if (readingInfo) {
      setImage(readingInfo.meterPhotoId);
    }
  }, [readingInfo]);

  const handleMenuItems = (e) => {
    const { name, value } = e.target;

    if (name === "Name") {
      setMeterId(value.meterId);
      setRoomId(value.roomId);
      setRoomName(value.roomName);
      setPreviousReading(value.reading);
      setSelectedRoomError(false);
    }

    if (name === "currentReading") {
      setCurrentReading(value);
    }

    if (name === "image") {
      props.uploadReadingPhoto({
        meterId: meterId,
        image: e.target.files[0],
      });
      setElectricityBillDialogClose(false);
    }
  };

  const validation = () => {
    if (previousReading > currentReading) {
      setReadingError(true);
      return false;
    }
    if (!roomId) {
      setSelectedRoomError(true);
      return false;
    }
    return true;
  };

  const handleSubmit = () => {
    if (validation()) {
      props.recordMeterReading({
        meterId: meterId,
        reading: currentReading,
      });
      props.readingUpdate(
        roomId,
        roomName,
        meterId,
        previousReading,
        currentReading,
        readingInfo ? readingInfo.meterPhotoId : null
      );
      onClose();
      setPreviousReading(0);
      setImage(null);
      setRoomId(0);
      setReadingError(false);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={() => {
        onClose();
        setPreviousReading(0);
        setCurrentReading(0);
        setImage(null);
        setRoomId(0);
        setReadingError(false);
      }}
      classes={{ paper: classes.dialogPaper }}
    >
      <DialogContent>
        <InputLabel id="demo-controlled-open-select-label">Room</InputLabel>
        <Select
          name="Name"
          error={selectedRoomError}
          onChange={handleMenuItems}
          fullWidth
          className={classes.alignItems}
          name="Name"
        >
          {tenantAllocateRoomList
            .filter((room) => {
              return room.meterId != 0;
            })
            .map((room) => (
              <MenuItem key={room.roomId} value={room}>
                {room.roomName}
              </MenuItem>
            ))}
        </Select>

        {selectedRoomError ? (
          <>
            <Typography color="secondary">{t("tenant.roomError")}</Typography>
          </>
        ) : (
          <></>
        )}

        {previousReading ? (
          <Typography className={classes.alignItems}>
            Previous Reading is {previousReading}
          </Typography>
        ) : (
          <></>
        )}

        {roomId ? (
          <>
            <Grid container alignItems="center" className={classes.alignItems}>
              <Grid item xs={6}>
                Current Reading
              </Grid>
              <Grid item xs={6}>
                <TextField
                  error={readingError}
                  name="currentReading"
                  variant="outlined"
                  type="number"
                  size="small"
                  onChange={handleMenuItems}
                />
              </Grid>
            </Grid>

            {readingError ? (
              <>
                <Typography
                  style={{
                    color: "#f44336",
                    fontSize: "0.75rem",
                  }}
                >
                  {t("common.readingError")}
                </Typography>
              </>
            ) : (
              <></>
            )}

            <Grid className={classes.alignItems}>
              {image == null ? (
                <Button variant="contained" color="primary" component="label">
                  Add Photo
                  <input
                    name="image"
                    type="file"
                    hidden
                    onChange={handleMenuItems}
                  />
                </Button>
              ) : (
                <CardMedia
                  xs={0}
                  md={3}
                  className={classes.cover}
                  image={config.api.BASE_URL + "/image/" + image}
                  title="Live from space album cover"
                />
              )}
            </Grid>
          </>
        ) : (
          <></>
        )}

        <Grid className={classes.addElectricityBillButton}>
          <Button variant="contained" color="primary" onClick={handleSubmit}>
            Add
          </Button>
        </Grid>
      </DialogContent>
    </Dialog>
  );
}

function NewPayment(props) {
  const { tenantId } = useParams();
  const classes = useStyles();
  const roomList = props.roomList;
  const loadTenant = props.loadTenant;
  const tenantInfo = props.selectedTenant;
  const tenantdues = props.tenantdues;
  const loadPayments = props.loadPayments;
  const [electricityBillDialog, setElectricityBillDialog] = useState(false);
  const [items, setItems] = useState([]);
  const [totalElectricityBill, setTotalElectricityBill] = useState(0);
  const [meterReadingList, setMeterReadingList] = useState([]);
  let tenantBalance = 0;
  const [payment, setPayment] = useState(0);
  const [paymentError, setPaymentError] = useState(false);
  const [discount, setDiscount] = useState(0);
  let newBalance = payment;
  const [imageDialog, setImageDialog] = useState(false);
  const [image, setImage] = useState(null);
  const [paymentDisable, setPaymentdisable] = useState(false);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    tenantdues();
  }, [tenantdues]);

  useEffect(() => {
    roomList(tenantId);
  }, [roomList, tenantId]);

  useEffect(() => {
    loadTenant(tenantId);
  }, [loadTenant, tenantId]);

  useEffect(() => {
    loadPayments();
  }, [loadPayments]);

  const electricMeter = (
    roomId,
    roomName,
    meterId,
    previousReading,
    currentReading,
    imageId
  ) => {
    if (roomId && roomName && previousReading && currentReading) {
      let bill = (currentReading - previousReading) * 7;
      setTotalElectricityBill(totalElectricityBill + bill);
      setItems([
        ...items.filter((room) => room.roomId != roomId),
        {
          roomId: roomId,
          roomName: roomName,
          previousReading: previousReading,
          currentReading: currentReading,
          imageId: imageId,
        },
      ]);

      setMeterReadingList([
        ...meterReadingList.filter((meter) => meter.meterId != meterId),
        {
          meterId: meterId,
          reading: currentReading,
          caption: "image of" + meterId,
          imageId: imageId,
        },
      ]);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (name === "payment") {
      if (value < 0) {
        setPaymentError(true);
      } else {
        setPayment(Math.round(value));
        setPaymentError(false);
      }
    }

    if (name === "discount") {
      setDiscount(Math.round(value));
    }
  };

  const validation = () => {
    if (payment === 0) {
      setPaymentError(true);
      return false;
    } else {
      return true;
    }
  };

  const handleSubmit = (e) => {
    if (validation()) {
      props.recordPayment(
        tenantId,
        payment,
        "payment of tenant " + tenantId,
        meterReadingList,
        discount
      );
      setPaymentdisable(true);
    }
  };

  const list = props.paymentList.filter(
    (payment) => payment.tenantId == tenantId
  );
  const previousPayment = list[0];

  const tenantDuesList = props.tenantDues.filter(
    (tenant) => tenant.id == tenantId
  );

  const tenantAllocateRoomList = props.tenantAllocateRoomList.filter(
    (roomDetails) => roomDetails.meterId != 0
  );

  if (tenantInfo) {
    if (tenantInfo.balance < 0) {
      tenantBalance = tenantInfo.balance;
    } else {
      tenantBalance = tenantInfo.balance;
    }
  }

  return (
    <Container component="main">
      <CssBaseline />
      {/* <Paper className={classes.paper}> */}
      <Typography
        component="h1"
        variant="h5"
        color="primary"
        className={classes.title}
      >
        New Payment
      </Typography>
      <Typography component="h6" variant="h6" className={classes.tenantName}>
        {tenantInfo.name
          ? tenantInfo.name.slice(0, 1).toUpperCase() +
            tenantInfo.name.slice(1, tenantInfo.name.length)
          : ""}
      </Typography>

      <Grid>
        {previousPayment ? (
          <>
            <Typography component="h5">Last Payment</Typography>
            <Grid container className={classes.alignItems} alignItems="center">
              <Grid item xs={9}>
                <Typography variant="body2" className={classes.alignItems}>
                  Date: {moment.utc(previousPayment.time).format("DD MMM YYYY")}
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <Typography variant="body2" className={classes.alignItems}>
                  Rs {previousPayment.paidAmount}
                </Typography>
              </Grid>
            </Grid>
          </>
        ) : (
          <></>
        )}

        {tenantDuesList.length > 0 ? (
          <>
            <Grid container className={classes.alignTextFields}>
              <Grid>
                <Typography component="h5">Current Balance</Typography>
              </Grid>
              <Grid>
                {props.tenantDues
                  .filter((tenant) => tenant.id == tenantId)
                  .map((tenant) => (
                    <Typography variant="body2" className={classes.alignItems}>
                      Rs: {(tenantBalance = tenant.balance)} Due
                    </Typography>
                  ))}
              </Grid>
            </Grid>
          </>
        ) : tenantInfo.balance < 0 ? (
          <>
            <Grid container className={classes.alignItems}>
              <Grid item xs={7}>
                <Typography component="h5">Current Balance</Typography>
              </Grid>
              <Grid item xs={5}>
                Rs:{tenantInfo.balance * -1} Advance
              </Grid>
            </Grid>
          </>
        ) : (
          ""
        )}
      </Grid>

      {tenantAllocateRoomList.length > 0 ? (
        <>
          <Grid container alignItems="center">
            <Grid item xs={10}>
              <Typography>Electric Bill</Typography>
            </Grid>
            <Grid item xs={2}>
              <Add
                onClick={() => {
                  setElectricityBillDialog(true);
                }}
              />
            </Grid>
          </Grid>
        </>
      ) : (
        <></>
      )}

      <List>
        {items.map((item) => (
          <>
            <ListItem disableGutters={true} className={classes.alignListItems}>
              <Typography component="h5">{item.roomName}</Typography>
              <Grid container className={classes.headers}>
                <Grid item xs={9}>
                  <Typography>currentReading</Typography>
                </Grid>
                <Grid item xs={3}>
                  <Typography> {item.currentReading}</Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography>previousReading</Typography>
                </Grid>
                <Grid item xs={3}>
                  <Typography> {item.previousReading}</Typography>
                </Grid>
                {item.imageId ? (
                  <>
                    <Grid>
                      <Link
                        onClick={(e) => {
                          setImageDialog(true);
                          setImage(item.imageId);
                        }}
                      >
                        <Typography color="primary">View Photo</Typography>
                      </Link>
                    </Grid>
                  </>
                ) : (
                  <></>
                )}
              </Grid>
              <Grid container>
                <Grid item xs={9}>
                  <Typography variant="body2" className={classes.alignItems}>
                    ({item.currentReading} - {item.previousReading}) x 7
                  </Typography>
                </Grid>
                <Grid>
                  Rs {(item.currentReading - item.previousReading) * 7}
                </Grid>
              </Grid>
            </ListItem>
            <Divider />
          </>
        ))}
      </List>

      {totalElectricityBill ? (
        <>
          <Grid container>
            <Grid item xs={9}>
              <Typography>Total Bill</Typography>
            </Grid>
            <Grid>
              <Typography>Rs {Math.round(totalElectricityBill)}</Typography>
            </Grid>
          </Grid>
        </>
      ) : (
        ""
      )}

      <Typography component="h5" className={classes.payable}>
        Discount
      </Typography>
      <Grid container alignItems="center" className={classes.alignTextFields}>
        <Grid>
          <Typography component="h5">Rs:</Typography>
        </Grid>
        <Grid>
          <TextField
            name="discount"
            defaultValue="0"
            size="small"
            variant="outlined"
            type="number"
            inputProps={{ style: { textAlign: "end" } }}
            onChange={handleChange}
          />
        </Grid>
      </Grid>

      <Typography component="h5" className={classes.payable}>
        Payable
      </Typography>

      <Grid container className={classes.alignTextFields}>
        <Grid>
          <Typography variant="body2" className={classes.alignItems}>
            {tenantBalance > 0 ? (
              <>
                ({tenantBalance} + {totalElectricityBill} - {discount})
              </>
            ) : (
              <>
                {/* ({tenantBalance * -1} + {totalElectricityBill} - {discount}) */}
                Nothing Payable
              </>
            )}
          </Typography>
        </Grid>
        <Grid>
          {tenantBalance > 0 ? (
            <>
              Rs:{" "}
              {Math.round(
                tenantBalance + totalElectricityBill - Number(discount)
              )}{" "}
            </>
          ) : (
            <></>
            // <>Nothing Payable</>
          )}
        </Grid>
      </Grid>

      <Typography component="h5" className={classes.payable}>
        Payment
      </Typography>
      <Grid container alignItems="center" className={classes.alignTextFields}>
        <Grid>
          <Typography component="h5">Rs:</Typography>
        </Grid>
        <Grid>
          <TextField
            name="payment"
            error={paymentError}
            defaultValue="0"
            size="small"
            variant="outlined"
            type="number"
            inputProps={{ style: { textAlign: "end" } }}
            onChange={handleChange}
            helperText={paymentError ? t("tenant.paymentError") : ""}
          />
        </Grid>
      </Grid>

      <Grid container alignItems="center" className={classes.alignTextFields}>
        <Grid>
          <Typography component="h5" className={classes.payable}>
            New Balance
          </Typography>
        </Grid>
        <Grid>
          {tenantBalance +
            totalElectricityBill -
            Number(discount) -
            Number(payment) <
          0 ? (
            <>
              Rs:{" "}
              {Math.round(
                (tenantBalance +
                  totalElectricityBill -
                  Number(discount) -
                  Number(payment)) *
                  -1
              )}{" "}
              Advance
            </>
          ) : (
            <>
              Rs:{" "}
              {Math.round(
                tenantBalance +
                  totalElectricityBill -
                  Number(discount) -
                  Number(payment)
              )}{" "}
              Due
            </>
          )}
        </Grid>
      </Grid>

      <div
        style={{
          width: "100%",
        }}
      >
        <Button
          variant="contained"
          disabled={paymentDisable}
          className={classes.submitButton}
          color="primary"
          onClick={handleSubmit}
        >
          Submit
        </Button>
      </div>

      {/* </Paper> */}

      <ElectricityBillDialog
        open={electricityBillDialog}
        onClose={() => {
          setElectricityBillDialog(false);
        }}
        tenantAllocateRoomList={props.tenantAllocateRoomList}
        recordMeterReading={props.recordMeterReading}
        uploadReadingPhoto={props.uploadReadingPhoto}
        readings={props.readings}
        readingUpdate={electricMeter}
      />

      <DocumentDialog
        open={imageDialog}
        onClose={() => {
          setImageDialog(false);
        }}
        imageId={image}
      />
    </Container>
  );
}
const mapDispatchprops = (dispatch) => {
  return {
    loadTenant: (tenantId) => {
      dispatch(tenantActions.getTenant(tenantId));
    },
    roomList: (tenantId) => dispatch(bookActions.getRoomList(tenantId)),
    tenantdues: () => dispatch(tenantActions.getTenantdues()),
    recordMeterReading: (meterReadingInfo) =>
      dispatch(meterActions.recordMeterReading(meterReadingInfo)),
    uploadReadingPhoto: (meterPhotoInfo) =>
      dispatch(meterActions.uploadReadingPhoto(meterPhotoInfo)),
    recordPayment: (tenantId, amount, comment, Reading, Discount) => {
      dispatch(
        bookActions.recordPayment(tenantId, amount, comment, Reading, Discount)
      );
    },
    loadPayments: () => dispatch(bookActions.getPayments()),
  };
};

const mapStateToProps = (state) => {
  return {
    selectedTenant: state.tenants.selectedTenant,
    tenantDues: state.tenants.tenantDues,
    tenantAllocateRoomList: state.book.tenantRoomList,
    readings: state.meters.meterReading,
    paymentList: state.book.paymentList,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(NewPayment)
);

export { x as NewPayment };
