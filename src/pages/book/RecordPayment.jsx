import React, { useState, useEffect } from "react";
import { Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import { bookActions, tenantActions } from "../../_actions";
import { useParams } from "react-router-dom";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { ElectricityBill } from "./Electricitybill";
import { settingActions } from "../../_actions";
import Dialog from "@material-ui/core/Dialog";
import {
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from "@material-ui/core";
import Preview from "./Preview";
// import PreviewDialog from "./Preview";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  tenantName: {
    marginBottom: 20,
  },
  previewButton: {
    marginBottom: 20,
  },
}));

function PreviewDialog(props) {
  const { open, onClose } = props;
  const amount = props.amount;
  const requestData = props.requestData;
  const defaultSetting = props.defaultSetting;

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => {
          onClose();
        }}
      >
        <Preview
          amount={amount}
          requestData={requestData}
          defaultSetting={defaultSetting}
          // tenantDues={props.tenantDues}
          tenantAllocateRoomList={props.tenantAllocateRoomList}
        />
      </Dialog>
    </div>
  );
}

function RecordPayment(props) {
  const { tenantId } = useParams();
  const [requestData, setRequestData] = useState({
    tenantId: tenantId,
    meterReadings: [],
  });
  const [amountError, setAmountError] = useState(false);
  const [commentError, setCommentError] = useState(false);
  const [DiscountError, setDiscountError] = useState(false);
  const [checked, setChecked] = React.useState(false);
  const [amount, setAmount] = useState(0);
  const [previewDialogOpen, setPreviewDialogOpen] = useState(false);

  const classes = useStyles();

  const roomList = props.roomList;
  const recordPayment = props.recordPayment;
  const loadTenant = props.loadTenant;
  const tenantInfo = props.selectedTenant;
  const tenantDues = props.tenantdues;
  const getSetting = props.getSetting;

  const validation = () => {
    setAmountError(requestData.paidAmount === undefined);
    setCommentError(requestData.comment === undefined);
    return amountError || commentError;
  };

  const previewHandleSubmit = (e) => {
    setPreviewDialogOpen(true);
  };

  const handleSubmit = (e) => {
    if (validation()) {
      recordPayment(
        tenantId,
        requestData.paidAmount,
        requestData.comment,
        requestData.meterReadings,
        requestData.discount
      );
    }
  };

  useEffect(() => {
    roomList(tenantId);
  }, [roomList, tenantId]);

  useEffect(() => {
    loadTenant(tenantId);
  }, [loadTenant, tenantId]);

  useEffect(() => {
    tenantDues();
  }, [tenantDues]);

  useEffect(() => {
    getSetting();
  }, [getSetting]);

  const handleCheckbox = (e) => {
    const { name } = e.target;
    if (name === "Discount") {
      setChecked(e.target.checked);
    }
  };

  const electricMeter = (checked, readingValue, meterId, imageId) => {
    if (checked) {
      setRequestData({
        ...requestData,
        meterReadings: [
          ...requestData.meterReadings.filter(
            (obj) => !(obj.meterId === meterId)
          ),
          {
            meterId: meterId,
            reading: parseInt(readingValue),
            caption: "image of" + meterId,
            imageId: imageId,
          },
        ],
      });
    } else {
      setRequestData({
        ...requestData,
        meterReadings: [
          ...requestData.meterReadings.filter(
            (obj) => !(obj.meterId === meterId)
          ),
        ],
      });
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "amount") {
      setRequestData({
        ...requestData,
        paidAmount: parseInt(value === "" ? "0" : value),
      });
      if (value.length === 0) {
        setAmountError(true);
      } else {
        setAmountError(false);
      }
      setAmount(value);
    }
    if (name === "comment") {
      setRequestData({
        ...requestData,
        comment: value,
      });
      if (value.length === 0) {
        setCommentError(true);
      } else {
        setCommentError(false);
      }
    }
    if (name === "Discount") {
      setRequestData({
        ...requestData,
        discount: parseInt(value === "" ? "0" : value),
      });
      if (value.length === 0) {
        setDiscountError(true);
      } else {
        setDiscountError(false);
      }
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" color="primary">
          New Payment
        </Typography>
        <Typography component="h6" variant="h6" className={classes.tenantName}>
          {tenantInfo.name ? "Tenant " + tenantInfo.name : ""}
        </Typography>

        <form className={classes.form} noValidate>
          <TextField
            error={amountError}
            variant="outlined"
            required
            fullWidth
            id="amount"
            label="Amount"
            name="amount"
            autoFocus
            onChange={handleChange}
            helperText={amountError ? "amount cannot be blank" : " "}
          />

          {props.tenantAllocateRoomList
            .filter((room) => {
              return room.meterId != 0;
            })
            .map((room) => (
              <ElectricityBill
                key={room.meterId.toString()}
                room={room}
                readingUpdate={electricMeter}
              />
            ))}

          <FormControlLabel
            control={<Checkbox onChange={handleCheckbox} name="Discount" />}
            label="Discount"
          />
          <TextField
            error={DiscountError}
            variant="outlined"
            disabled={!checked}
            margin="normal"
            required
            fullWidth
            id="Discount"
            label="Discount"
            name="Discount"
            autoFocus
            onChange={handleChange}
            helperText={DiscountError ? "Discount cannot be blank" : " "}
          />

          <TextField
            error={commentError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="comment"
            label="Comment"
            name="comment"
            autoFocus
            onChange={handleChange}
            helperText={commentError ? "comment cannot be blank" : " "}
          />

          <Button
            onClick={previewHandleSubmit}
            fullWidth
            margin="normal"
            variant="contained"
            color="primary"
            className={classes.previewButton}
          >
            Preview
          </Button>

          <Button
            onClick={handleSubmit}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            RECORD PAYMENT
          </Button>
        </form>

        <PreviewDialog
          open={previewDialogOpen}
          onClose={() => {
            setPreviewDialogOpen(false);
          }}
          // tenantDues={props.tenantDues}
          amount={amount}
          requestData={requestData}
          defaultSetting={props.defaultSetting}
          tenantAllocateRoomList={props.tenantAllocateRoomList}
        />
      </div>
    </Container>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    recordPayment: (tenantId, amount, comment, Reading, Discount) => {
      dispatch(
        bookActions.recordPayment(tenantId, amount, comment, Reading, Discount)
      );
    },
    roomList: (tenantId) => dispatch(bookActions.getRoomList(tenantId)),
    loadTenant: (tenantId) => {
      dispatch(tenantActions.getTenant(tenantId));
    },
    tenantdues: () => dispatch(tenantActions.getTenantdues()),
    getSetting: () => dispatch(settingActions.getSetting()),
  };
};

const mapStateToProps = (state) => {
  return {
    tenantAllocateRoomList: state.book.tenantRoomList,
    selectedTenant: state.tenants.selectedTenant,
    meterReading: state.meters.meterReading,
    tenantDues: state.tenants.tenantDues,
    defaultSetting: state.setting.defaultSetting,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(RecordPayment)
);

export { x as RecordPayment };
