import React, { useState, useEffect } from "react";
import { Button, Grid, ListItem, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { meterActions } from "../../_actions";
import CardMedia from "@material-ui/core/CardMedia";
import config from "../../config";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  bill: {
    width: "100%",
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  addPhotoButton: {
    marginLeft: 21,
  },
  cover: {
    height: 50,
    width: "100%",
  },
}));

function ElectricityBill(props, Reading) {
  const classes = useStyles();
  const [reading, setReading] = useState(0);
  const [checked, setChecked] = useState(false);
  const [image, setImage] = useState(null);

  const readingInfo = props.readings.filter((meterReadingInfo) => {
    return meterReadingInfo.meterId === props.room.meterId;
  })[0];

  useEffect(() => {
    if (readingInfo) {
      setReading(readingInfo.meterReading);
      setChecked(readingInfo.isSelected);
      setImage(readingInfo.meterPhotoId);
      props.readingUpdate(
        readingInfo.isSelected,
        readingInfo.meterReading,
        props.room.meterId,
        readingInfo.meterPhotoId
      );
    }
  }, [readingInfo]);

  useEffect(() => {
    props.recordMeterReading({
      meterId: props.room.meterId,
      meterReading: null,
      meterPhotoId: null,
      isSelected: false,
    });
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "Reading") {
      props.recordMeterReading({
        meterId: props.room.meterId,
        reading: value,
        isSelected: checked,
      });
    }
    if (name === "room") {
      props.recordMeterReading({
        meterId: props.room.meterId,
        reading: reading,
        isSelected: e.target.checked,
      });
    }
    if (name === "image") {
      props.uploadReadingPhoto({
        meterId: props.room.meterId,
        image: e.target.files[0],
      });
    }
  };

  return (
    <React.Fragment key={props.room.roomId}>
      <ListItem disableGutters={true}>
        <Grid container classesName={classes.width}>
          <Grid item xs={5}>
            <FormControlLabel
              control={<Checkbox onChange={handleChange} name="room" />}
              label={props.room.roomName}
            />
          </Grid>
          <Grid item xs={3}>
            <TextField
              disabled={!checked}
              variant="outlined"
              margin="none"
              size="small"
              // required
              // fullWidth = {true}
              id={props.room.meterId}
              label="Reading"
              name="Reading"
              autoFocus
              onChange={handleChange}
              // helperText={commentError ? "comment cannot be blank" : " "}
            />
          </Grid>
          <Grid item xs={3} className={classes.addPhotoButton}>
            {image == null ? (
              <Button variant="contained" color="primary" component="label">
                Add
                <input
                  name="image"
                  type="file"
                  hidden
                  onChange={handleChange}
                />
              </Button>
            ) : (
              <CardMedia
                xs={0}
                md={3}
                className={classes.cover}
                image={config.api.BASE_URL + "/image/" + image}
                title="Live from space album cover"
              />
            )}
          </Grid>
        </Grid>
      </ListItem>
    </React.Fragment>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    uploadReadingPhoto: (meterPhotoInfo) =>
      dispatch(meterActions.uploadReadingPhoto(meterPhotoInfo)),
    recordMeterReading: (meterReadingInfo) =>
      dispatch(meterActions.recordMeterReading(meterReadingInfo)),
  };
};

const mapStateToProps = (state) => {
  return {
    readings: state.meters.meterReading,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(ElectricityBill);

export { x as ElectricityBill };
