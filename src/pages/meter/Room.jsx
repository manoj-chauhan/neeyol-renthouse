import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Title from "../widgets/Title";

const useStyles = makeStyles(theme => ({
}));

function Room(props) {
  
  return (
    <React.Fragment>
      <Title>Room</Title>
    </React.Fragment>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    // loadAllocationDetail: (roomId) => dispatch(roomActions.getAllocationDetail(roomId)),
  };
};

const mapStateToProps = (state) => {
  return {
    // allocationDetail: state.rooms.selectedRoomAllocationDetail,
    
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(Room)
);

export{ x as Room };
