import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";
import { meterActions } from "../../_actions";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function NewMeter(props) {
  const [name, setName] = useState("");
  const [initReading, setInitReading] = useState(0);
  const [nameError, setNameError] = useState(false);
  const [initReadingError, setInitReadingError] = useState(false);
  const [buttonDisabled, setButtonDisabled] = useState(false);

  const classes = useStyles();

  const validation = () => {
    setNameError(name.length === 0);
    setInitReadingError(initReading == 0);
    return !(name.length === 0 || initReading == 0);
  };

  const handleSubmit = (e) => {
    if (validation()) {
      props.addMeter({
        name: name,
        initialReading: initReading,
      });
      setButtonDisabled(true);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "name") {
      setName(value);
      if (value.length === 0) {
        setNameError(true);
      } else {
        setNameError(false);
      }
    }
    if (name === "initialReading") {
      setInitReading(value);
      if (value.length === 0) {
        setInitReadingError(true);
      } else {
        setInitReadingError(false);
      }
    }
  };


  const [t, i18n] = useTranslation();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          New Meter
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            error={nameError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Meter Name"
            name="name"
            autoFocus
            onChange={handleChange}
            helperText={nameError ? t('common.nameError') : " "}
          />
          <TextField
            error={initReadingError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="initialReading"
            label="Initial Reading"
            type="number"
            id="initial_reading"
            autoFocus
            onChange={handleChange}
            helperText={initReadingError ? t('common.initialReadingError') : " "}
          />
          <Button
            disabled={buttonDisabled}
            onClick={handleSubmit}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            ADD Meter
          </Button>
        </form>
      </div>
    </Container>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    addMeter: (meter) => dispatch(meterActions.addMeter(meter)),
  };
};

const mapStateToProps = (state) => {
  return {};
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(NewMeter)
);

export { x as NewMeter };
