import React, { useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";
import { meterActions, roomActions } from "../../_actions";
import { useParams } from "react-router-dom";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Divider from "@material-ui/core/Divider";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { FormControl, FormLabel, OutlinedInput } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Link from "@material-ui/core/Link";
import { MeterAllocationHistory } from "./MeterAllocationHistory";
import { MeterReading } from "./MeterReading";
import moment from "moment";
import EditTwoToneIcon from "@material-ui/icons/EditTwoTone";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
  info: {
    height: 100,
    display: "flex",
    flexDirection: "row",
  },
  allocationDetail: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  allocationTime: {
    display: "flex",
    justifyContent: "flex-end",
  },
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2,
  },
  headerLabel: {
    width: "100%",
  },
  dialogTitle: {
    color: "blue",
  },
}));

function UpdateMeterDetails(props) {
  const { open, onClose } = props;

  const [name, setName] = useState("");

  const classes = useStyles();

  const handleOnChange = (e) => {
    const { name, value } = e.target;
    if (name === "editedName") {
      setName(value);
    }
  };

  const handleSubmit = (e) => {
    props.updateMeterDetails({
      meterId: props.meterId,
      name: name,
    });
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => {
          onClose();
        }}
      >
        <DialogTitle className={classes.dialogTitle}>
          Change Meter Name
        </DialogTitle>
        <DialogContent>
          <TextField
            variant="outlined"
            margin="normal"
            name="editedName"
            label="Name"
            defaultValue={props.name ? props.name : ""}
            onChange={handleOnChange}
          />
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function AllocateSimpleDialog(props) {
  const { meterId } = useParams();
  const [Room, setRoom] = useState("");
  const [roomError, setRoomError] = useState(false);
  const [initialReading, setInitialReading] = useState(0);
  const [initialReadingError, setInitialReadingError] = useState(false);
  const classes = useStyles();
  const { onClose, selectedValue, open, translation } = props;
  const [image, setImage] = useState("");

  const loadRoomList = props.loadRoomList;

  useEffect(() => {
    loadRoomList();
  }, [loadRoomList]);

  const handleSubmit = (e) => {
    if (Room && initialReading && initialReading > props.previousReading) {
      props.allocateMeter({
        roomId: Room,
        initialReading: initialReading,
        meterId: meterId,
        imageId: image,
      });
      props.onClose();
      setRoom("");
      setRoomError(false);
      setInitialReading(0);
      setInitialReadingError(false);
    } else {
      setRoomError(Room.length === 0);
      setInitialReadingError(initialReading === 0);
      if (props.previousReading > initialReading) {
        setInitialReadingError(true);
      }
    }
  };

  const handleClose = () => {
    onClose(selectedValue);
    setRoom("");
    setRoomError(false);
    setInitialReading(0);
    setInitialReadingError(false);
  };

  const handleMenuItemClick = (e) => {
    const { name, value } = e.target;
    if (name === "Name") {
      setRoom(value);
      if (value.length === 0) {
        setRoomError(true);
      } else {
        setRoomError(false);
      }
    }
    if (name === "initialReading") {
      setInitialReading(value);
      console.log("initial reading is", value);
      console.log("previous reading is", props.previousReading);
      if (value.length === 0 || value <= props.previousReading) {
        setInitialReadingError(true);
      } else {
        setInitialReadingError(false);
      }
    }
    if (name === "Image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Meter Allocation
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-controlled-open-select-label">Room</InputLabel>
            <Select
              error={roomError}
              onChange={handleMenuItemClick}
              name="Name"
            >
              {props.roomList
                .filter(
                  (room) =>
                    room.allocated === false && room.roomAvailability === true
                )
                .map((room) => (
                  <MenuItem key={room.id} value={room.id}>
                    {room.name}
                  </MenuItem>
                ))}
            </Select>
            {roomError ? (
              <Typography color="secondary">{translation('common.nameError')}</Typography>
            ) : (
              ""
            )}

            <Typography
              color="primary"
              style={{
                marginTop: 10,
              }}
            >
              Previous reading is {props.previousReading} KWH
            </Typography>

            <TextField
              error={initialReadingError}
              variant="outlined"
              margin="normal"
              name="initialReading"
              label="Initial Reading"
              type="number"
              onChange={handleMenuItemClick}
              helperText={
                initialReadingError
                  ? translation('common.readingError')
                  : " "
              }
            />
            <FormLabel component="legend">Image</FormLabel>
            <OutlinedInput
              label="Image"
              type="file"
              name="Image"
              onChange={handleMenuItemClick}
            />
            <DialogActions>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSubmit}
              >
                Allocate
              </Button>
            </DialogActions>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function DeallocateSimpleDialog(props) {
  const { meterId } = useParams();
  const [lastReading, setLastReading] = useState(0);
  const [lastReadingError, setLastReadingError] = useState(false);
  const [meterReadingImage, setMeterReadingImage] = useState(null);
  const [meterReadingImageCaption, setMeterReadingImageCaption] = useState("");
  const classes = useStyles();
  const { open, onClose, selectedValue, translation, previousReading } = props;
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));

  const validation = () => {

    if (lastReading === 0 || previousReading >= lastReading) {
      setLastReadingError(lastReading === 0 || previousReading >= lastReading);
      return false;
    } else {
      return true;
    }

  };

  const handleSubmit = (e) => {

    if (validation()) {
      props.deallocateMeter({
        LastReading: lastReading,
        meterId: meterId,
        imageId: meterReadingImage,
        caption: meterReadingImageCaption,
      });
      handleClose();
    }

  };

  const handleClose = () => {
    props.onClose();
    setLastReading(0);
    setLastReadingError(false);
    setMeterReadingImage(null);
    setMeterReadingImageCaption("");
  }

  const handleMenuItemClick = (e) => {
    const { name, value } = e.target;
    if (name === "lastReading") {
      setLastReading(value);
      if (value.length === 0 || value <= previousReading) {
        setLastReadingError(true);
      } else {
        setLastReadingError(false);
      }
    }
    if (name === "meterReadingImage") {
      setMeterReadingImage(
        e.target.files.length > 0 ? e.target.files[0] : null
      );
    }
    if (name === "meterReadingImageCaption") {
      setMeterReadingImageCaption(value);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Last Reading
        </DialogTitle>
        <DialogContent>
          <Typography>Previous reading is {previousReading} </Typography>
          <FormControl className={classes.formControl}>
            <TextField
              error={lastReadingError}
              variant="outlined"
              margin="normal"
              name="lastReading"
              label="Last Reading"
              type="number"
              onChange={handleMenuItemClick}
              helperText={
                lastReadingError ? translation('common.readingError') : ""
              }
            />
            <FormLabel>Meter Reading Image</FormLabel>
            <TextField
              variant="outlined"
              name="meterReadingImage"
              type="file"
              onChange={handleMenuItemClick}
            />
            <DialogActions>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSubmit}
              >
                Deallocate
              </Button>
            </DialogActions>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function MeterDetail(props) {
  const [open, setOpen] = React.useState(false);
  const [state, setState] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState("Idea");
  const [updateMeterDialogOpen, setUpdateMeterDialogOpen] = useState(false);

  const { meterId } = useParams();
  const classes = useStyles();

  const meterInfo = props.selectedMeter;
  const deallocateMeter = props.deallocateMeter;

  const { t, i18n } = useTranslation();

  const AllocatehandleClickOpen = () => {
    setOpen(true);
  };

  const DeallocatehandelClickOpen = () => {
    setState(true);
  };

  const DeallocatehandleClose = () => {
    setState(false);
  };

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };

  const loadMeter = props.loadMeter;
  useEffect(() => {
    loadMeter(meterId);
  }, [loadMeter, meterId]);

  let allocationAction = props.selectedMeter.allocationDetail ? (
    <Link
      color="primary"
      onClick={(e) => {
        DeallocatehandelClickOpen();
      }}
    >
      Deallocate
    </Link>
  ) : (
    <Link
      color="primary"
      onClick={(e) => {
        AllocatehandleClickOpen();
      }}
    >
      Allocate
    </Link>
  );

  return (
    <div className={classes.root}>
      <div className={classes.info}>
        <div xs={6}>
          <Typography component="h5" variant="h5" color="textPrimary">
            {meterInfo.name
              .split(" ")
              .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
              .join(" ")}
          </Typography>
          <Typography>
            Current Reading : {meterInfo.currentReading} KWH
          </Typography>
        </div>
        <div xs={6}>
          <EditTwoToneIcon
            onClick={(e) => {
              setUpdateMeterDialogOpen(true);
            }}
          />
        </div>
      </div>

      <div className={classes.allocationDetail}>
        <div className={classes.header}>
          <div className={classes.headerLabel}>
            <Typography component="h6" variant="h6" color="primary">
              Allocation Detail
            </Typography>
          </div>
          <div className={classes.headerNewLinkContainer}>
            {allocationAction}
          </div>
        </div>
        <Divider />
        <div>
          {meterInfo.allocationDetail ? (
            <div>
              <Grid container>
                <Grid item xs={4}>
                  <Typography>{meterInfo.allocationDetail.roomName}</Typography>
                </Grid>
                <Grid item xs={8} className={classes.allocationTime}>
                  <Typography>
                    Allocated on{" "}
                    {moment
                      .utc(meterInfo.allocationDetail.allocationTime)
                      .format("DD MMM YYYY")}
                  </Typography>
                </Grid>
              </Grid>
              <Typography>{meterInfo.allocationDetail.buildingName}</Typography>
              <Typography>
                {meterInfo.allocationDetail.buildingAddress}
              </Typography>
            </div>
          ) : (
            "Not allocated"
          )}
        </div>
      </div>
      <div>
        <MeterReading meterId={meterId} />
      </div>
      <div>
        <MeterAllocationHistory meterId={meterId} />
      </div>
      <AllocateSimpleDialog
        loadRoomList={props.loadRoomList}
        roomList={props.roomList}
        selectedValue={selectedValue}
        open={open}
        onClose={handleClose}
        allocateMeter={props.allocateMeter}
        previousReading={meterInfo.currentReading}
        translation={t}
      />
      <DeallocateSimpleDialog
        selectedValue={selectedValue}
        open={state}
        onClose={DeallocatehandleClose}
        deallocateMeter={deallocateMeter}
        previousReading={meterInfo.currentReading}
        translation={t}
      />
      <UpdateMeterDetails
        open={updateMeterDialogOpen}
        onClose={() => {
          setUpdateMeterDialogOpen(false);
        }}
        meterId={meterId}
        name={meterInfo.name}
        updateMeterDetails={props.updateMeterDetails}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadMeter: (meterId) => {
      dispatch(meterActions.getMeter(meterId));
    },
    allocateMeter: (meter) => {
      dispatch(meterActions.allocateMeter(meter));
    },
    deallocateMeter: (meter) => {
      dispatch(meterActions.deallocatedMeter(meter));
    },
    updateMeterDetails: (meterDetails) => {
      dispatch(meterActions.updateMeter(meterDetails));
    },
    loadRoomList: () => {
      dispatch(roomActions.getRoomList());
    },
  };
};

const mapStateToProps = (state) => {
  return {
    selectedMeter: state.meters.selectedMeter,
    roomList: state.rooms.roomList,
    meterList: state.meters.meterList,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(MeterDetail);

export { x as MeterDetail };
