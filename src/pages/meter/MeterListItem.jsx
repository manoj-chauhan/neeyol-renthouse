import React from "react";
import config from "../../config";
import { ListItem } from "@material-ui/core";
import { history } from "../../_helpers";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  content: {
    // width: "100%",
  },
  title: {},
  cover: {
    width: 80,
  },
  pos: {
    align: "center",
  },
  info: {
    justifyContent: "center",
    alignItems: "flex-end",
    direction: "row",
  },
}));

function MeterListItem(props) {
  const classes = useStyles();
  const meter = props.meterInfo;

  return (
    <ListItem
      onClick={(e) => {
        history.push("/meter/" + meter.id);
      }}
    >
      <Card className={classes.root}>
        <CardContent className={classes.content}>
          <Grid container>
            <Grid item xs={6}>
              <Typography
                className={classes.title}
                color="textPrimary"
                gutterBottom
              >
                <Box fontWeight="fontWeightBold" component="span">
                  {meter.name
                    .split(" ")
                    .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                    .join(" ")}
                </Box>
              </Typography>
            </Grid>
            <Grid
              container
              direction="column"
              item
              className={classes.info}
              xs={6}
            >
              <Typography className={classes.pos}>
                <Box component="span">
                  {meter.allocated ? "Allocated" : "Not Allocated"}
                </Box>
              </Typography>
            </Grid>
            <Grid container direction="column" item xs={12}>
              <Typography>
                <Box component="span">
                  {meter.lastMeterReading} Kwh last recorded on{" "}
                  {moment.utc(meter.lastMeterReadingTime).format("DD-MM-YYYY")}
                </Box>
              </Typography>
            </Grid>
          </Grid>
        </CardContent>

        <CardMedia
          className={classes.cover}
          image={config.api.BASE_URL + "/image/" + meter.imageId + "/thumb"}
          title="Live from space album cover"
        />
      </Card>
    </ListItem>
  );
}

const x = withStyles(useStyles)(MeterListItem);
export { x as MeterListItem };
