import React, { useEffect } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { meterActions } from "../../_actions";
import { connect } from "react-redux";
import moment from "moment";


const useStyles = makeStyles((theme) => ({
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom:2
  },
  headerLabel: {
    width:"100%"
  },
  listItem: {
    padding: 0,
    marginBottom: theme.spacing(2)
  },
  detail: {
    width: "100%"
  },
  readingDate: {
    display: "flex",
    justifyContent: "flex-end"
  }
}));

function MeterAllocationHistory(props) {
  const loadAllocationHistory = props.loadAllocationHistory;
  
  const classes = useStyles();

  useEffect(() => {
    loadAllocationHistory(props.meterId);
  }, [loadAllocationHistory, props.meterId]);

  return (
    <React.Fragment>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
              Allocation History
          </Typography>
        </div>
      </div>
      <Divider/>
      <List>
        {props.allocationHistory.map((row) => (
          <ListItem className={classes.listItem}>
              <div className={classes.detail}>
                <Grid container>
                  <Grid item xs={6}>
                    <Typography>{row.roomName} Kwh</Typography>
                  </Grid>
                  <Grid item xs={6} className={classes.readingDate}>
                    <Typography>From {moment.utc(row.allocationTime).format("DD MMM YYYY")} To {moment.utc(row.deAllocationTime).format("DD MMM YYYY")}</Typography>
                  </Grid>
                </Grid>
                <Typography>{row.comment}</Typography>
              </div>
          </ListItem>
        ))
        }
      </List>
    </React.Fragment>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadAllocationHistory: (meterId) =>
      dispatch(meterActions.getAllocationHistory(meterId)),
  };
};

const mapStateToProps = (state) => {
  return {
    allocationHistory: state.meters.selectedmeterAllocationHistory,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(MeterAllocationHistory);

export { x as MeterAllocationHistory };
