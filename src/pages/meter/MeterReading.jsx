import React, { useState, useEffect } from "react";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import { meterActions } from "../../_actions";
import { connect } from "react-redux";
import moment from "moment";
import { ListItem } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import config from "../../config";
import Dialog from "@material-ui/core/Dialog";

const useStyles = makeStyles((theme) => ({
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2,
  },
  headerLabel: {
    width: "100%",
  },
  listItem: {
    padding: 0,
    marginBottom: theme.spacing(2),
  },
  detail: {
    width: "100%",
  },
  readingDate: {
    display: "flex",
    justifyContent: "flex-end",
  },
  loadMoreLink: {
    display: "flex",
    justifyContent: "flex-start",
  },
}));

const useStylesImageDialog = makeStyles((theme) => ({
  img: {
    height: 600,
  },
}));

function ImageDialog(props) {
  const { onClose, open, imageId } = props;
  const classes = useStylesImageDialog();

  return (
    <div>
      <Dialog
        onClose={() => {
          onClose();
        }}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <img
          className={classes.img}
          src={config.api.BASE_URL + "/image/" + imageId}
          alt=""
        />
      </Dialog>
    </div>
  );
}

function MeterReading(props) {
  const meterReadingList = props.getMeterReadingList;
  const clearMeterReadingList = props.clearMeterReadingList;
  const [imageOpen, setImageOpen] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const classes = useStyles();
  const [pageNo, setPageNo] = useState(1);

  useEffect(() => {
    clearMeterReadingList();
    meterReadingList(props.meterId);
    console.log("list length" + props.meterReadings.length);
  }, [meterReadingList, props.meterId]);

  const handleSubmit = (e) => {
    console.log("value of pageNo in handleSubmit" + pageNo);
    setPageNo(pageNo + 1);
    console.log("pageNo in handleSubmit" + pageNo);
    meterReadingList(props.meterId, pageNo);
  };

  return (
    <React.Fragment>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
            Readings
          </Typography>
        </div>
      </div>
      <Divider />
      <List>
        {props.meterReadings.map((row) => (
          <ListItem className={classes.listItem}>
            <div className={classes.detail}>
              <Grid container>
                <Grid item xs={6}>
                  <Typography>{row.reading} Kwh</Typography>
                </Grid>
                <Grid item xs={6} className={classes.readingDate}>
                  <Typography>
                    {moment
                      .utc(row.time, "YYYY-MM-DD hh:mm")
                      .format("DD MMM YYYY")}
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography>{row.comment}</Typography>
                </Grid>
                <Grid item xs={6} className={classes.readingDate}>
                  {row.imageId ? (
                    <Link
                      color="primary"
                      onClick={(e) => {
                        setSelectedImage(row.imageId);
                        setImageOpen(true);
                      }}
                    >
                      View Photo
                    </Link>
                  ) : (
                    ""
                  )}
                </Grid>
              </Grid>
            </div>
          </ListItem>
        ))}

        {props.meterReadings.length == 5 ? (
          <Link onClick={handleSubmit}>load more..</Link>
        ) : (
          ""
        )}

      </List>
      <ImageDialog
        open={imageOpen}
        onClose={() => {
          setImageOpen(false);
        }}
        imageId={selectedImage}
      />
    </React.Fragment>
  );
}
const mapDispatchprops = (dispatch) => {
  return {
    getMeterReadingList: (meterId, pageNo) => {
      dispatch(meterActions.MetereReadingDetail(meterId, pageNo));
    },
    clearMeterReadingList: () => {
      dispatch(meterActions.clearMeterReadingList());
    },
  };
};

const mapStateToProps = (state) => {
  return {
    meterReadings: state.meters.selectedMeterReadingList,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(MeterReading);

export { x as MeterReading };
