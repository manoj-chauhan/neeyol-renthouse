import React, { useEffect } from "react";
import { List, Grid, Typography, Button } from "@material-ui/core";
import { connect } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { meterActions } from "../../_actions";
import { MeterListItem } from "./MeterListItem";
import TimelineOutlinedIcon from "@material-ui/icons/TimelineOutlined";
import { history } from "../../_helpers";

const useStyles = makeStyles((theme) => ({
  root: {},
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  topBar: {
    padding: 16,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 8,
  },
  meterIcon: {
    fontSize: 150,
    color: "#171414",
  },
  alignMeterIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 150,
  },
  styleOfAddMeter: {
    margin: 10,
  },
}));

function MeterList(props) {
  const classes = useStyles();
  const loadMeters = props.loadMeters;

  useEffect(() => {
    loadMeters();
  }, [loadMeters]);

  return (
    <div>
      <List>
        {props.meterList
          .filter((meter) => meter.name != null && meter.initialReading)
          .sort((firstMeter, secondMeter) => {
            return firstMeter.name.localeCompare(secondMeter.name);
          })
          .map((meter) => (
            <MeterListItem key={meter.id} meterInfo={meter} />
          ))}
      </List>

      {props.meterList.length === 0 ? (
        <Grid items xs={12}>
          <div className={classes.alignMeterIcon}>
            <TimelineOutlinedIcon className={classes.meterIcon} />
            <Typography>No Meter Added Yet</Typography>
            <Button
              variant="contained"
              color="primary"
              size="small"
              className={classes.styleOfAddMeter}
              onClick={() => {
                history.push("/meter/new");
              }}
            >
              Add Meter
            </Button>
          </div>
        </Grid>
      ) : (
        ""
      )}
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadMeters: () => dispatch(meterActions.getMeters()),
  };
};

const mapStateToProps = (state) => {
  return {
    isLoading: state.meters.isLoading,
    meterList: state.meters.meterList,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(MeterList)
);

export { x as MeterList };
