import React from 'react';
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      height: '100%',
    },
    details: {
      display: 'flex',
      flexDirection: 'column',
      width:'100%'
    },
    content: {
      flex: '1 0 auto',
    },
    cover: {
      width: 240,
    },
    coverLarge: {
      width: "100%",
    },
    controls: {
      display: 'flex',
      alignItems: 'center',
      paddingLeft: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
    playIcon: {
      height: 38,
      width: 38,
    },
  }));

  function MeterInfo(props){
    const classes = useStyles();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    return (matches) ? 
  (
    <Card className={classes.root}>
      <Grid container>
        <Grid item xs={12} md className={classes.details}>
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5">
              {props.info.name}
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  )
  :
  (
    <Card className={classes.root}>
      <Grid container>
        <Grid item xs={12} md className={classes.details}>
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5">
            {props.info.name}
            </Typography>
            <br/>
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  );

  }

  export default MeterInfo;