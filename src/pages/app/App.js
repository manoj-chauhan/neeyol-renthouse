import React, { useEffect } from "react";
import { Router, Switch, Route } from "react-router-dom";
import { Main } from "../main";
import { LoginPage, Registration } from "../auth";
import { history, userData } from "../../_helpers";
import { connect } from "react-redux";
import { useSnackbar } from "notistack";
import { alertActions } from "../../_actions";
import { NewMeter } from "../meter";
import { NewTenant } from "../tenant";
import { NewRoom } from "../room";
import { NewBuilding } from "../building";
import { TenantProfile } from "../tenant";
import { NewPayment, RecordPayment } from "../book";
import { MeterDetail } from "../meter";
import { RoomDetail } from "../room";
import { UserProfile } from "../user";
import jwt_decode from "jwt-decode";
import moment from "moment";
import { makeStyles } from "@material-ui/core";
import { BuildingWizard } from "../dashboard/BuildingWizard";

const useStyles = makeStyles((theme) => ({
  root: {
    flex: 1,
    height: "100vh",
    display: "flex",
    backgroundColor: "white",
  },
  loaderStyle: {
    width: "100%",
    position: "absolute",
    backgroundColor: "#dddd",
    width: "100%",
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

function App(props) {
  const { enqueueSnackbar } = useSnackbar();
  const alerts = props.alerts;
  alerts.map((alert) => enqueueSnackbar(alert.message));
  let tokenExpiration = false;
  // const [tokenExpiration, setExpiration] = React.useState(true);
  const token = userData.getAuthToken();

  // useEffect(() => {
  if (token === null) {
    tokenExpiration = true;
    // setExpiration(true);
    if (history.location.pathname === "/auth/registration") {
      history.push("/auth/registration");
    } else {
      history.push("/auth/login");
    }
  } else {
    const decodedToken = jwt_decode(token);
    const expiryDate = moment
      .unix(decodedToken.exp)
      .format("YYYY-MM-DD h:mm:ss a");
    const presentTime = moment().format("YYYY-MM-DD h:mm:ss a");

    if (moment(expiryDate).isAfter(presentTime)) {
      tokenExpiration = false;
      // setExpiration(false);
      // do not put history.push("/")
      // because every time page is refreshed
      // the page will come in the dash dashboard.
    } else {
      tokenExpiration = true;
      // setExpiration(true);
      userData.clear();
      history.push("/auth/login");
    }
  }
  // }, []);

  return (
    <Router history={history}>
      {/* {tokenExpiration ? (
        <>
          <Route path="/auth/login" exact component={LoginPage} />
          <Route path="/auth/registration" exact component={Registration} />
        </>
      ) : (
        <> */}
      <Switch>
        <Route path="/auth/login" exact component={LoginPage} />
        <Route path="/auth/registration" exact component={Registration} />
        <Route path="/user" exact component={UserProfile} />
        <Route path="/tenant/new" exact component={NewTenant} />
        <Route path="/tenant/:tenantId" exact component={TenantProfile} />
        <Route path="/tenant/:tenantId/payment" exact component={NewPayment} />
        <Route path="/building/new" exact component={NewBuilding} />
        <Route path="/buildingWizard" exact component={BuildingWizard} />
        <Route
          path="/building/:buildingId/room/new"
          exact
          component={NewRoom}
        />
        <Route
          path="/building/:buildingId/room/:roomId"
          exact
          component={RoomDetail}
        />
        <Route path="/meter/new" exact component={NewMeter} />
        <Route path="/meter/:meterId" exact component={MeterDetail} />
        <Route path="/" component={Main} />
      </Switch>
      {/* </>
      )} */}
    </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    alerts: state.alert,
  };
};

const mapDispatchprops = (dispatch) => {
  return {
    clearAlerts: () => dispatch(alertActions.clear()),
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(App);

export { x as App };
