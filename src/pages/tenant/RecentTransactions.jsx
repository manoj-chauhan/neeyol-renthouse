import React, { useState, useEffect, Fragment } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import { bookActions } from '../../_actions';
import { connect } from "react-redux";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  list: {
    padding: 0
  },
  listItem: {
    padding: 0,
    display: "block"
  },
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2
  },
  headerLabel: {
    width: "100%"
  },
  transactionTitleLink: {
    display: "flex",
    justifyContent: "flex-end"
  },
  paymentType: {
    display: "flex",
    justifyContent: "center"
  }
}));

function RecentTransactions(props) {

  const classes = useStyles();
  const loadStatement = props.loadStatement;
  const clearTransactions = props.clearTransactions;
  const [pageNo, setPageNo] = useState(1);

  useEffect(() => {
    if (props.accountId !== undefined) {
      clearTransactions();
      loadStatement(props.accountId);
    }
  }, [loadStatement, props.accountId]);

  const transactions = props.transactions.sort((transaction1, transaction2) => {
    let moment1 = moment.utc(transaction1.transactionTime);
    let moment2 = moment.utc(transaction2.transactionTime);
    return moment1.diff(moment2) * -1;
  }).map((item) => {
    let accountId = item.accountCreditId;
    let transactionType = "Due";
    if (item.accountCreditId === props.accountId) {
      accountId = item.accountDebitId;
      transactionType = "Payment";
    }

    return {
      id: item.id,
      comment: item.comment,
      amount: item.amount,
      transactionTime: item.transactionTime,
      accountId: accountId,
      transactionType: transactionType
    }

  });

  const handleSubmit = (e) => {
    setPageNo(pageNo + 1);
    loadStatement(props.accountId, pageNo);
  }

  return (
    <div>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
            Recent Transactions
          </Typography>
        </div>
        <div className={classes.headerNewLinkContainer}></div>
      </div>
      <Divider />
      <List className={classes.list}>
        {transactions.map((transaction) => (
          <Fragment key={transaction.id}>
            <ListItem className={classes.listItem}>
              <Grid container>
                <Grid item xs={4}>
                  <Typography variant="subtitle1">
                    {moment
                      .utc(transaction.date)
                      .utcOffset("+0530")
                      .format("DD MMM YYYY")}
                  </Typography>
                </Grid>
                <Grid item xs={4} className={classes.paymentType}>
                  <Typography variant="subtitle1">
                    <div>{transaction.transactionType}</div>
                  </Typography>
                </Grid>
                <Grid item xs={4} className={classes.transactionTitleLink}>
                  <Typography variant="subtitle1">
                    <div>Rs. {transaction.amount}</div>
                  </Typography>
                </Grid>
              </Grid>
              <div>
                <Typography variant="subtitle2" gutterBottom>
                  {transaction.comment}
                </Typography>
              </div>
            </ListItem>
          </Fragment>
        ))}
        <Grid item xs={12} className={classes.editLinkButton}>
          {props.remainingTransactions > 0 ? (
            <Link onClick={handleSubmit}>load more..</Link>
          ) : (
            <Typography>
              No recent rransactions...
            </Typography>
          )}
        </Grid>
      </List>
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadStatement: (accountId, pageNo) => { dispatch(bookActions.getStatement(accountId, pageNo)) },
    clearTransactions: () => { dispatch(bookActions.clearTransactions()) }
  };
}

const mapStateToProps = state => {
  return {
    transactions: state.book.statement,
    remainingTransactions: state.book.remainingTransactions
  };
};

const x = connect(
  mapStateToProps,
  mapDispatchprops
)(RecentTransactions);

export { x as RecentTransactions };