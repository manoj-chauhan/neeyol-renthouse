import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import Typography from '@material-ui/core/Typography';
import Title from '../widgets/Title';

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

function Balance(props) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Balance</Title>
      <Typography component="p" variant="h4">
        INR 2100
      </Typography>
      <Typography color="textSecondary" className={classes.depositContext}>
        Last Meter Reading on 15 March, 2019
      </Typography>
      <div>
        <Link color="primary" href={"/tenant/" + props.tenantId + "/payment"} onClick={(e) => {
          }}>
          Record Payment
        </Link>
      </div>
    </React.Fragment>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    
  };
}

const mapStateToProps = state => {
  return {
    
  };
};

const x = 
  connect(
    mapStateToProps,
    mapDispatchprops
  )(Balance);

export {x as Balance};
