import React, { useState, useEffect, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { connect } from "react-redux";
import { tenantActions, settingActions, roomActions } from "../../_actions";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import { FormControl, FormLabel, OutlinedInput } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import DialogContent from "@material-ui/core/DialogContent";
import InputLabel from "@material-ui/core/InputLabel";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Select from "@material-ui/core/Select";
import moment from "moment";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  list: {
    padding: 0,
  },
  listItem: {
    padding: 0,
    display: "block",
  },
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2,
  },
  headerLabel: {
    width: "100%",
  },
  headerNewLinkContainer: {
    display: "flex",
    alignItems: "center",
  },
  roomTitle: {
    display: "flex",
    justifyContent: "flex-end",
  },
  roomTitleLabel: {
    width: "100%",
  },
  roomTitleLink: {
    paddingLeft: theme.spacing(2),
    display: "flex",
    alignItems: "center",
  },
  room: {
    width: "100%",
  },
}));

const styleForRoomAllocationDialog = makeStyles((theme) => ({
  select: {
    width: "100%",
  },
  formControl: {
    width: "100%",
  },
  dialogTitle: {
    color: "blue",
  },
}));

function RoomAllocationDialog(props) {
  const [roomId, setRoomId] = useState(0);
  const [roomError, setRoomError] = useState(false);
  const [allocationDate, setAllocationDate] = useState("");
  const [allocationDateError, setAllocationDateError] = useState(false);
  const [unitCharges, setUnitCharges] = useState("");
  const [rentAmount, setRentAmount] = useState("");
  const [rentCycle, setRentCycle] = useState("");
  const [currentReading, setCurrentReading] = useState("");
  const [currentReadingError, setCurrentReadingError] = useState(false);
  const [previousReading, setPreviousReading] = useState(0);
  const [image, setImage] = useState("");

  const { tenantId } = useParams();
  const classes = styleForRoomAllocationDialog();
  const { onClose, open, roomMeterDetail } = props;
  const loadRooms = props.loadRooms;
  const getSetting = props.getSetting;
  const defaultSetting = props.defaultSetting;
  const [roomDetails, setRoomDetails] = useState(null);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    setRentCycle(defaultSetting ? defaultSetting.rentCycle : "");
    setRentAmount(defaultSetting ? defaultSetting.rent : "");
    setUnitCharges(defaultSetting ? defaultSetting.electricityUnitCharges : "");
  }, [defaultSetting]);

  useEffect(() => {
    loadRooms();
  }, [loadRooms]);

  useEffect(() => {
    getSetting();
  }, [getSetting]);

  useEffect(() => {
    if (Object.keys(roomMeterDetail).length !== 0) {
      setPreviousReading(roomMeterDetail.lastMeterReading);
    }
  }, [roomMeterDetail])

  const validate = () => {
    if (roomId === 0 || allocationDate.length === 0) {
      setRoomError(roomId === 0);
      if (currentReading <= previousReading) {
        setCurrentReadingError(currentReading <= previousReading);
      }
      setAllocationDateError(allocationDate.length === 0);
      return false;
    } else if (currentReading <= previousReading && previousReading > 0) {
      setCurrentReadingError(currentReading <= previousReading);
      return false;
    } else {
      return true;
    }
  }

  const handleClose = () => {
    props.onClose();
    setRoomId(0);
    setRoomError(false);
    setAllocationDate("");
    setAllocationDateError(false);
    setCurrentReading("");
    setCurrentReadingError(false);
    setPreviousReading(0);
    setRoomDetails(null);
  }

  const handleSubmit = (e) => {
    if (validate()) {
      let x = moment.utc(allocationDate);
      if (roomId && allocationDate && rentAmount && unitCharges && rentCycle) {
        props.allocateRoom({
          roomId: roomId,
          tenantId: tenantId,
          allocationTime: x.toISOString(),
          amount: rentAmount,
          rentCycle: rentCycle,
          electricityUnitCharges: unitCharges,
          reading: currentReading,
          imageId: image,
        });
        handleClose();
      }
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "Name") {
      if (value.id > 0) {
        setRoomId(value.id);
        setRoomDetails(value);
        setRoomError(false);
        console.log("room id is", value.id);
        props.getRoomMeterAllocationDetail(Number(value.id));
      }
    }
    if (name === "allocationDate") {
      if (value.length !== 0) {
        setAllocationDate(value);
        setAllocationDateError(false);
      }
    }
    if (name === "unitCharges") {
      setUnitCharges(value);
    }
    if (name === "rent") {
      setRentAmount(value);
    }
    if (name === "rentCycle") {
      setRentCycle(value);
    }
    if (name === "currentReading") {
      if (value.length !== 0 && value > previousReading) {
        setCurrentReading(value);
        setCurrentReadingError(false);
      }
    }
    if (name === "Image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Room Allocation
        </DialogTitle>
        <DialogContent>
          <form className={classes.container} noValidate>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Rooms
                  </InputLabel>
                  <Select
                    error={roomError}
                    onChange={handleChange}
                    name="Name"
                    className={classes.select}
                  >
                    {props.roomList
                      .filter(
                        (room) =>
                          room.allocated == false &&
                          room.roomAvailability == true
                      )
                      .map((room) => (
                        <MenuItem key={room.id} value={room}>
                          {room.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
                {
                  roomError ? (
                    <Typography style={
                      {
                        color: "#f44336",
                        fontSize: "0.75rem",
                        marginLeft: "14px"
                      }
                    }>
                      {t('tenant.roomError')}
                    </Typography>
                  ) : ""
                }
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <TextField
                    error={allocationDateError}
                    id="allocationDate"
                    label="Allocation Date"
                    type="date"
                    name="allocationDate"
                    className={classes.textField}
                    onChange={handleChange}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    helperText={
                      allocationDateError ? t('common.dateError') : ""
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Rent Cycle
                  </InputLabel>
                  <Select
                    onChange={handleChange}
                    name="rentCycle"
                    className={classes.select}
                    defaultValue={
                      props.defaultSetting ? props.defaultSetting.rentCycle : ""
                    }
                  >
                    <MenuItem key="monthly" value="monthly">
                      Monthly
                    </MenuItem>
                    <MenuItem key="weekly" value="weekly">
                      Weekly
                    </MenuItem>
                    <MenuItem key="daily" value="daily">
                      Daily
                    </MenuItem>
                    <MenuItem key="per minute" value="per minute">
                      Per Minute
                    </MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <TextField
                    id="rent"
                    label="Rent"
                    type="number"
                    name="rent"
                    defaultValue={
                      props.defaultSetting ? props.defaultSetting.rent : ""
                    }
                    onChange={handleChange}
                  />
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <TextField
                    id="unitCharges"
                    label="Per Unit Charges"
                    type="number"
                    name="unitCharges"
                    defaultValue={
                      props.defaultSetting
                        ? props.defaultSetting.electricityUnitCharges
                        : ""
                    }
                    onChange={handleChange}
                  />
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  {roomDetails ? (
                    roomDetails.meterIsAllocated ? (
                      <>
                        <Typography color="primary">
                          previous reading is {previousReading}
                        </Typography>
                        <TextField
                          error={currentReadingError}
                          id="currentReading"
                          label="Current Reading"
                          type="number"
                          name="currentReading"
                          onChange={handleChange}
                          helperText={
                            currentReadingError ? t('common.readingError') : ""
                          }
                        />
                      </>
                    ) : (
                      <></>
                    )
                  ) : (
                    <></>
                  )}
                </FormControl>
              </Grid>
              <Grid item>
                <FormLabel component="legend">Meter Reading Photo</FormLabel>
                <OutlinedInput
                  label="Meter Reading Photo"
                  type="file"
                  name="Image"
                  onChange={handleChange}
                />
              </Grid>
            </Grid>
          </form>
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit}
            >
              Allocate
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function AllocatedRooms(props) {
  const classes = useStyles();
  const loadAllocatedRooms = props.loadAllocatedRooms;
  const [roomAllocationDialogOpen, setRoomAllocationDialog] = useState(false);
  const [roomAllocationError, setRoomAllocationError] = useState(false);
  const defaultSetting = props.defaultSetting;

  useEffect(() => {
    loadAllocatedRooms(props.tenantId);
  }, [loadAllocatedRooms, props.tenantId]);

  useEffect(() => {
    props.getSetting();
  }, [props.getSetting]);

  useEffect(() => {
    if (defaultSetting.rent
      && defaultSetting.rentCycle
      && defaultSetting.electricityUnitCharges) {
      setRoomAllocationError(false);
    } else {
      setRoomAllocationError(true);
    }
  }, [defaultSetting]);

  return (
    <div>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
            Allocated Rooms
          </Typography>
        </div>
        <div className={classes.headerNewLinkContainer}>
          {
            !roomAllocationError ? (
              <Link
                color="primary"
                onClick={(e) => {
                  setRoomAllocationDialog(true);
                }}
              >
                New
              </Link>
            ) : ""
          }
        </div>
      </div>

      <Divider />
      {
        roomAllocationError ? (
          <Typography color="secondary">
            Please Add Default Settings
          </Typography>
        ) : ""
      }
      <List className={classes.list}>
        {props.allocatedRoomList.map((room) => (
          <Fragment key={room.roomId}>
            <ListItem className={classes.listItem}>
              <div className={classes.roomTitle}>
                <div className={classes.roomTitleLabel}>
                  <Typography variant="subtitle1">{room.roomName}</Typography>
                </div>
                <div className={classes.roomTitleLink}>
                  <Link
                    color="primary"
                    href={
                      "/building/" + room.buildingId + "/room/" + room.roomId
                    }
                  >
                    View
                  </Link>
                </div>
              </div>
              <div>
                {room.meterIsAllocated ? (
                  <Typography variant="subtitle2" gutterBottom>
                    (Meter Reading {room.reading} kwh)
                  </Typography>
                ) : (
                  ""
                )}
              </div>
            </ListItem>
          </Fragment>
        ))}
      </List>
      <RoomAllocationDialog
        open={roomAllocationDialogOpen}
        onClose={() => {
          setRoomAllocationDialog(false);
        }}
        roomList={props.roomList}
        loadRooms={props.loadRooms}
        getSetting={props.getSetting}
        allocateRoom={props.allocateRoom}
        defaultSetting={props.defaultSetting}
        getRoomMeterAllocationDetail={props.getRoomMeterAllocationDetail}
        roomMeterDetail={props.roomMeterDetail}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadAllocatedRooms: (tenantId) => {
      dispatch(tenantActions.getSelectedTenantAllocatedRooms(tenantId));
    },
    allocateRoom: (room) => dispatch(roomActions.allocate(room)),
    getSetting: () => dispatch(settingActions.getSetting()),
    loadRooms: () => dispatch(roomActions.getRoomList()),
    getRoomMeterAllocationDetail: (roomId) => dispatch(roomActions.getRoomMeterAllocationDetail(roomId)),
  };
};

const mapStateToProps = (state) => {
  return {
    allocatedRoomList: state.tenants.allocatedRooms,
    defaultSetting: state.setting.defaultSetting,
    roomList: state.rooms.roomList,
    roomMeterDetail: state.rooms.roomMeterDetail,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(AllocatedRooms);

export { x as AllocatedRooms };
