import React, { useEffect } from 'react';
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { RecentTransactions } from './RecentTransactions';
import { AllocatedRooms } from './AllocatedRooms';
import {TenantPersonalInfo} from './TenantPersonalInfo';
import { tenantActions } from '../../_actions';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  personalInfo: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: theme.palette.background.paper
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeightPaper: {
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column'
  }
}));

function TenantProfile(props) {
  
  const { tenantId } = useParams();
  const classes = useStyles();

  const loadTenant = props.loadTenant;
  useEffect(() => {
    loadTenant(tenantId);
  }, [loadTenant, tenantId]);

  const tenantInfo = props.selectedTenant;
  return (
    <div className={classes.root}>
        <TenantPersonalInfo info={tenantInfo}/>
        <AllocatedRooms tenantId={tenantId} />
        <RecentTransactions accountId={tenantInfo.accountId} />
    </div> 
  );
}

const mapDispatchprops = dispatch => {
  return {
    loadTenant: (tenantId) => {dispatch(tenantActions.getTenant(tenantId))}
  };
};

const mapStateToProps = state => {
  return { 
    selectedTenant: state.tenants.selectedTenant
   };
};

const x = withStyles(useStyles)(
  connect(
    mapStateToProps,
    mapDispatchprops
  )(TenantProfile)
);

export {x as TenantProfile};