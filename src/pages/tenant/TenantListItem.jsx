import React from "react";
import config from "../../config";
import { ListItem, Grid, Avatar } from "@material-ui/core";
import { history } from "../../_helpers";
import { makeStyles } from "@material-ui/core/styles";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "80px",
    margin: 0,
    display: "flex",
  },
  main: {
    padding: 0,
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    width: 100,
  },
  alignCurrentBalance: {
    display: "flex",
    justifyContent: "space-between",
  },
  noProfileImage: {
    height: 80,
    width: 80,
    borderRadius: 20,
    background: "white",
    color: "black",
  },
}));

export function TenantListItem(props) {
  const classes = useStyles();

  const tenant = props.tenantInfo;

  return (
    <ListItem
      onClick={(e) => {
        history.push("/tenant/" + tenant.id);
      }}
    >
      <Card className={classes.root}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography
              className={classes.title}
              color="textPrimary"
              gutterBottom
            >
              {tenant.name
                .split(" ")
                .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                .join(" ")}
            </Typography>
            {tenant.balance > 0 ? (
              <>
                <div className={classes.alignCurrentBalance}>
                  <Typography color="textSecondary">Balance</Typography>
                  <Typography color="textSecondary" variant="body2">
                    Rs {Math.round(tenant.balance)} Due
                  </Typography>
                </div>
              </>
            ) : tenant.balance === 0 ? (
              <>
                <div className={classes.alignCurrentBalance}>
                  <Typography color="textSecondary">Balance</Typography>
                  <Typography color="textSecondary" variant="body2">
                    Rs {Math.round(tenant.balance * -1)}
                  </Typography>
                </div>
              </>
            ) : (
              <>
                <div className={classes.alignCurrentBalance}>
                  <Typography color="textSecondary">Balance</Typography>
                  <Typography color="textSecondary" variant="body2">
                    Rs {Math.round(tenant.balance * -1)} Advance
                  </Typography>
                </div>
              </>
            )}
          </CardContent>
        </div>
        {tenant.imageId ? (
          <CardMedia
            className={classes.cover}
            image={config.api.BASE_URL + "/image/" + tenant.imageId + "/thumb"}
            title="Live from space album cover"
          />
        ) : (
          <div>
            <Avatar
              src="/broken-image.jpg"
              className={classes.noProfileImage}
            />
          </div>
        )}
      </Card>
    </ListItem>
  );
}
