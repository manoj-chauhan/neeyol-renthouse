import React, { useState, useEffect, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import List from "@material-ui/core/List";
import Dialog from "@material-ui/core/Dialog";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { tenantActions } from "../../_actions";
import { ListItem } from "@material-ui/core";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: "100%",
  }
}));

const useAddressListStyles = makeStyles((theme) => ({
  list: {
    padding: 0
  },
  listItem: {
    padding: 0,
    marginBottom: theme.spacing(2),
    display: "block"
  },
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2
  },
  headerLabel: {
    width: "100%"
  },
  headerNewLinkContainer: {
    display: "flex",
    alignItems: "center"
  },
  addressTitle: {
    display: "flex",
    justifyContent: "flex-end",
  },
  addressTitleLabel: {
    width: "100%"
  },
  addressTitleLink: {
    paddingLeft: theme.spacing(2),
    display: "flex",
    alignItems: "center"
  },
  address: {
    width: "100%"
  },
  addressActionLink: {
    paddingLeft: theme.spacing(2),
  }
}));

function NewAddressDialog(props) {
  const [address, setAddress] = useState("");
  const [addressError, setAddressError] = useState(false);
  const [type, setType] = useState("");
  const [typeError, setTypeError] = useState(false);

  const { onClose, open, translation } = props;

  const classes = useStyles();

  const handleSubmit = (e) => {
    if (type && address) {
      props.uploadAddress({
        address: address,
        tenantId: props.tenantId,
        type: type,
      });
      onClose();
      setAddress("");
      setAddressError(false);
      setType("");
      setTypeError(false);
    } else {
      setTypeError(type.length === 0);
      setAddressError(address.length === 0);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "address") {
      setAddress(value);
      if (value.length === 0) {
        setAddressError(true);
      } else {
        setAddressError(false);
      }
    }
    if (name === "type") {
      setType(value);
      if (value.length === 0) {
        setTypeError(true);
      } else {
        setTypeError(false);
      }
    }
  };

  return (
    <div>
      <Dialog
        onClose={() => {
          onClose();
          setAddress("");
          setAddressError(false);
          setType("");
          setTypeError(false);
        }}
        open={open}
      >
        <DialogTitle id="simple-dialog-title">New Address</DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <TextField
              error={typeError}
              variant="outlined"
              margin="normal"
              name="type"
              label="Type"
              type="text"
              onChange={handleChange}
              helperText={typeError ? translation("tenant.address.addressType") : ""}
            />
            <TextField
              error={addressError}
              variant="outlined"
              margin="normal"
              name="address"
              label="Address"
              type="text"
              onChange={handleChange}
              helperText={addressError ? translation("tenant.address.addressError") : ""}
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleSubmit}>ADD</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function DeleteAddressDialog(props) {
  const { onClose, open } = props;

  const classes = useStyles();

  const handleSubmit = (e) => {
    props.deleteAddress(props.tenantId, props.addressId);
    onClose();
  };

  return (
    <div>
      <Dialog
        onClose={() => {
          onClose();
        }}
        open={open}
      >
        <DialogTitle id="simple-dialog-title">Delete Address</DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            Are you sure you want to delete?
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleSubmit}>yes</Button>
          <Button color="secondary" onClick={onClose}>No</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function EditAddressDialog(props) {
  const [address, setAddress] = useState("");
  const [addressError, setAddressError] = useState(false);
  const [type, setType] = useState("");
  const [typeError, setTypeError] = useState(false);

  const { onClose, open } = props;

  useEffect(() => {
    setAddress(props.tenantAddress);
    setType(props.type);
  }, [props]);

  const classes = useStyles();

  const handleSubmit = (e) => {
    if (type && address && props.addressId) {
      props.updateAddress({
        tenantId: props.tenantId,
        tenantAddressId: props.addressId,
        address: address,
        type: type,
      });
      onClose();
      setAddress("");
      setAddressError(false);
      setType("");
      setTypeError(false);
    } else {
      setAddressError(address.length === 0);
      setTypeError(type.length === 0);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "address") {
      setAddress(value);
      if (value.length === 0) {
        setAddressError(true);
      } else {
        setAddressError(false);
      }
    }
    if (name === "type") {
      setType(value);
      if (value.length === 0) {
        setTypeError(true);
      } else {
        setTypeError(false);
      }
    }
  };

  return (
    <div>
      <Dialog
        onClose={() => {
          onClose();
          setAddress("");
          setAddressError(false);
          setType("");
          setTypeError(false);
        }}
        open={open}
      >
        <DialogTitle id="simple-dialog-title">Edit Address</DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <TextField
              error={typeError}
              variant="outlined"
              margin="normal"
              name="type"
              label="Type"
              type="text"
              onChange={handleChange}
              value={type}
              helperText={typeError ? "type cannot be empty" : ""}
            />
            <TextField
              error={addressError}
              variant="outlined"
              margin="normal"
              name="address"
              label="Address"
              type="text"
              onChange={handleChange}
              value={address}
              helperText={addressError ? "address cannot be empty" : ""}
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleSubmit}>Save</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function AddressList(props) {
  const [newAddressDialog, setNewAddressDialog] = useState(false);
  const [editAddressDialog, setEditAddressDialog] = useState(false);
  const [deleteAddressDialog, setDeleteAddressDialog] = useState(false);
  const [addressId, setAddressId] = useState("");
  const [type, setType] = useState();
  const [tenantAddress, setTenantAddress] = useState();

  const { tenantId, loadAddresses } = props;

  const classes = useAddressListStyles();
  const { t, i18n } = useTranslation();

  useEffect(() => {
    console.log("use Effect has called");
    loadAddresses(tenantId);
  }, [loadAddresses, tenantId]);

  return (
    <div>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
            Addresses
          </Typography>
        </div>
        <div className={classes.headerNewLinkContainer}>
          <Link
            color="primary"
            onClick={(e) => {
              setNewAddressDialog(true);
            }}
          >
            New
          </Link>
        </div>
      </div>
      <Divider />
      <List className={classes.list}>
        {
          props.addresses.map((address) => (
            <Fragment key={address.tenantAddressId}>
              <ListItem className={classes.listItem}>
                <div className={classes.addressTitle}>
                  <div className={classes.addressTitleLabel}>
                    <Typography variant="overline" display="block" >
                      <Box lineHeight={2}>
                        {address.type}
                      </Box>
                    </Typography>
                  </div>
                  <div className={classes.addressTitleLink}>
                    <Link
                      className={classes.addressActionLink}
                      color="primary"
                      onClick={(e) => {
                        setType(address.type);
                        setTenantAddress(address.address);
                        setAddressId(address.tenantAddressId);
                        setDeleteAddressDialog(true);
                      }}
                    >
                      Delete
                    </Link>
                    <Link
                      className={classes.addressActionLink}
                      color="primary"
                      onClick={(e) => {
                        loadAddresses(tenantId);
                        setType(address.type);
                        console.log(JSON.stringify(address));
                        setTenantAddress(address.address);
                        console.log("address id set in state", address.tenantAddressId);
                        setAddressId(address.tenantAddressId);
                        setEditAddressDialog(true);
                      }}
                    >
                      Edit
                    </Link>
                  </div>
                </div>
                <div className={classes.address}>
                  <Typography gutterBottom>{address.address}</Typography>
                </div>
              </ListItem>
            </Fragment>
          ))
        }
      </List>
      <NewAddressDialog
        tenantId={tenantId}
        open={newAddressDialog}
        onClose={() => {
          setNewAddressDialog(false);
        }}
        uploadAddress={props.uploadAddress}
        translation={t}
      />
      <DeleteAddressDialog
        tenantId={tenantId}
        open={deleteAddressDialog}
        onClose={() => {
          setDeleteAddressDialog(false);
        }}
        addressId={addressId}
        deleteAddress={props.deleteAddress}
      />
      <EditAddressDialog
        tenantId={tenantId}
        open={editAddressDialog}
        onClose={() => {
          setEditAddressDialog(false);
        }}
        tenantAddress={tenantAddress}
        addressId={addressId}
        type={type}
        updateAddress={props.updateAddress}
      />
    </div >
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadAddresses: (tenantId) => {
      dispatch(tenantActions.getAddresses(tenantId));
    },
    uploadAddress: (address) => {
      dispatch(tenantActions.uploadAddress(address));
    },
    deleteAddress: (tenantId, addressId) => {
      dispatch(tenantActions.deleteAddress(tenantId, addressId));
    },
    updateAddress: (tenant) => {
      dispatch(tenantActions.updateAddress(tenant));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    addresses: state.tenants.tenantAddresses,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(AddressList);

export { x as AddressList };
