import config from "../../config";
import React, { useState, useEffect, Fragment } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Dialog from "@material-ui/core/Dialog";
import FormControl from "@material-ui/core/FormControl";
import { FormLabel, OutlinedInput } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { tenantActions } from "../../_actions";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: "100%",
  },
  documentListItem: {
    padding: 0,
  }
}));

const useDocumentListStyles = makeStyles((theme) => ({
  list: {
    padding: 0
  },
  listItem: {
    padding: 0,
    display: "block"
  },
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2
  },
  headerLabel: {
    width: "100%"
  },
  headerNewLinkContainer: {
    display: "flex",
    alignItems: "center"
  },
  documentTitle: {
    display: "flex",
    justifyContent: "flex-end",
  },
  documentTitleLabel: {
    width: "100%"
  },
  documentTitleLink: {
    paddingLeft: theme.spacing(2),
    display: "flex",
    alignItems: "center"
  },
  document: {
    width: "100%"
  }
}));

function UploadDocumentDialog(props) {
  const [image, setImage] = useState(null);
  const [documentError, setDocumentError] = useState(false);
  const [description, setDescription] = useState("");
  const [descriptionError, setDescriptionError] = useState(false);

  const classes = useStyles();
  const { onClose, open, translation } = props;

  const handleSubmit = (e) => {
    if (image && description) {
      props.uploadDocument({
        tenantId: props.tenantId,
        image: image,
        description: description,
      });
      onClose();
      setImage(null);
      setDocumentError(false);
      setDescription("");
      setDescriptionError(false);
    } else {
      setDocumentError(image === null);
      setDescriptionError(description.length === 0);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "description") {
      setDescription(value);
      if (value.length === 0) {
        setDescriptionError(true);
      } else {
        setDescriptionError(false);
      }
    }
    if (name === "image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
      if (value.files?.length === 0) {
        setDocumentError(true);
      } else {
        setDocumentError(false);
      }
    }
  };

  return (
    <div>
      <Dialog
        onClose={() => {
          onClose();
          setImage(null);
          setDocumentError(false);
          setDescription("");
          setDescriptionError(false);
        }}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title">New Document</DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <FormLabel>Document Name</FormLabel>
            <TextField
              error={documentError}
              id="documentFile"
              variant="outlined"
              name="image"
              type="file"
              onChange={handleChange}
              helperText={documentError ? translation('tenant.document.nameError') : ""}
            />

            <TextField
              error={descriptionError}
              variant="outlined"
              margin="normal"
              name="description"
              label="Description"
              type="text"
              onChange={handleChange}
              helperText={descriptionError ? translation('tenant.document.descriptionError') : ""}
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button color="secondary" onClick={handleSubmit}>Upload</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function DocumentDialog(props) {
  const { onClose, open } = props;
  const imageId = (props.document) ? props.document.fileId : null;
  return (
    <div>
      <Dialog
        onClose={(e) => {
          onClose();
        }}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <img src={config.api.BASE_URL + "/image/" + imageId} alt="" />
      </Dialog>
    </div>
  );
}

function DocumentList(props) {
  const [docOpenDialog, setDocOpenDialog] = useState(false);
  const [openUploadDialog, setOpenUploadDialog] = useState(false);
  const [selectedDocument, setSelectedDocument] = useState(null);

  const classes = useDocumentListStyles();
  const { tenantId, loadDocuments } = props;
  const { t, i18n } = useTranslation();

  useEffect(() => {
    loadDocuments(tenantId);
  }, [loadDocuments, tenantId]);


  return (
    <div>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
            Documents
          </Typography>
        </div>
        <div className={classes.headerNewLinkContainer}>
          <Link
            color="primary"
            onClick={(e) => {
              setOpenUploadDialog(true)
            }}
          >
            New
          </Link>
        </div>
      </div>
      <Divider />
      <List className={classes.list}>
        {
          props.documents.map((document) => (
            <Fragment key={document.tenantDocumentId}>
              <ListItem className={classes.listItem}>
                <div className={classes.documentTitle}>
                  <div className={classes.documentTitleLabel}>
                    <Link
                      color="primary"
                      onClick={(e) => {
                        setSelectedDocument(document);
                        setDocOpenDialog(true);
                      }}
                    >
                      <Typography color="primary">
                        {document.description}
                      </Typography>
                    </Link>
                  </div>
                  <div className={classes.documentTitleLink}>
                    <Link
                      color="primary"
                      onClick={(e) => {
                        props.deleteDocument(tenantId, document.tenantDocumentId)
                      }}
                    >
                      Delete
                    </Link>
                  </div>
                </div>
              </ListItem>
            </Fragment>
          ))
        }
      </List>
      <DocumentDialog
        open={docOpenDialog}
        onClose={() => {
          setDocOpenDialog(false);
        }}
        document={selectedDocument}
      />
      <UploadDocumentDialog
        tenantId={tenantId}
        open={openUploadDialog}
        onClose={() => {
          setOpenUploadDialog(false)
        }}
        uploadDocument={props.uploadDocument}
        translation={t}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    uploadDocument: (tenantInfo) => {
      dispatch(tenantActions.uploadDocument(tenantInfo));
    },
    loadDocuments: (tenantId) => {
      dispatch(tenantActions.getTenantDocuments(tenantId));
    },
    deleteDocument: (tenantId, documentId) => {
      dispatch(tenantActions.deleteDocument(tenantId, documentId));
    }
  };
};

const mapStateToProps = (state) => {
  return {
    documents: state.tenants.tenantDocuments
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(DocumentList);

export { x as DocumentList };
