import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";
import { tenantActions } from "../../_actions";
import {
  FormControl,
  FormLabel,
  OutlinedInput,
  CircularProgress,
  Grid,
} from "@material-ui/core";
import { useTranslation } from "react-i18next";
const pica = require('pica')();

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function NewTenant(props) {
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState(false);
  const [addressError, setAddressError] = useState(false);
  const [phoneError, setPhoneError] = useState(false);
  const [image, setImage] = useState("");
  const [submitButtonDisabled, setSubmitButtonDisable] = useState(false);

  const classes = useStyles();
  const { t, i18n } = useTranslation();

  useEffect(() => {
    if (props.tenant.loaderVisible) {
      setSubmitButtonDisable(true);
    } else {
      setSubmitButtonDisable(false);
    }
  }, [props.tenant.loaderVisible]);

  const validateInput = () => {
    if (name.length === 0) {
      setNameError(name.length === 0);
      return false;
    }
    return true;
  };

  const handleSubmit = (e) => {
    if (validateInput()) {
      props.addTenant({
        name: name,
        imageId: image ? props.tenant.tenantPhotoId : null,
        caption: "Tenant Photo Of " + name,
      });
      setSubmitButtonDisable(true);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (name === "name") {
      setName(value);
      if (value.length === 0) {
        setNameError(true);
      } else {
        setNameError(false);
        setSubmitButtonDisable(false);
      }
    } else if (name === "image") {
      setImage(e.target.files.length > 0 ? e.target.files[0].name : null)
      if (e.target.files.length > 0) {
        props.uploadTenantPhotoWithOutTenantId(e.target.files[0]);
      }
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          New Tenant
        </Typography>
        <form className={classes.form} noValidate>
          <Grid item xs={props.tenant.imageFieldSize}>
            <TextField
              error={nameError}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="name"
              label="Name"
              name="name"
              autoFocus
              onChange={handleChange}
              helperText={nameError ? t("common.nameError") : " "}
            />
          </Grid>
          <Grid container alignItems="center">
            <Grid item xs={props.tenant.imageFieldSize}>
              <FormLabel component="legend">Tenant Image</FormLabel>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="image"
                type="file"
                id="tenantImage"
                autoFocus
                onChange={handleChange}
              />
            </Grid>
            {props.tenant.loaderVisible ? <CircularProgress size={30} /> : ""}
          </Grid>

          <Button
            disabled={submitButtonDisabled}
            onClick={handleSubmit}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            ADD TENANT
          </Button>
        </form>
      </div>
    </Container>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    addTenant: (tenant) => dispatch(tenantActions.addTenant(tenant)),
    uploadTenantPhotoWithOutTenantId: (tenantPhotoId) =>
      dispatch(tenantActions.uploadTenantPhotoWithOutTenantId(tenantPhotoId)),
  };
};

const mapStateToProps = (state) => {
  return {
    tenant: state.tenants,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(NewTenant)
);

export { x as NewTenant };
