import React, { useState, useEffect, Fragment } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Dialog from "@material-ui/core/Dialog";
import FormControl from "@material-ui/core/FormControl";
import { InputLabel, Select, MenuItem } from "@material-ui/core";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { tenantActions } from "../../_actions";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import config from "../../config";
import Avatar from "@material-ui/core/Avatar";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: "100%",
  },
  relativeListItem: {
    padding: 0,
  },
}));

const useRelativeListStyles = makeStyles((theme) => ({
  list: {
    padding: 0,
  },
  listItem: {
    padding: 0,
    display: "block",
  },
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2,
  },
  headerLabel: {
    width: "100%",
  },
  headerNewLinkContainer: {
    display: "flex",
    alignItems: "center",
  },
  relativeTitle: {
    display: "flex",
    justifyContent: "flex-end",
  },
  relativeTitleLabel: {
    width: "100%",
    display: "flex",
    padding: 2,
  },
  relativeTitleLink: {
    paddingLeft: theme.spacing(2),
    display: "flex",
    alignItems: "center",
  },
  relative: {
    width: "100%",
  },
  relativeImage: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    marginRight: 10,
  },
}));

function AddRelativeDialog(props) {
  const [relationshipId, setRelationshipId] = useState(null);
  const [relationshipIdError, setRelationshipIdError] = useState(false);
  const [relativeId, setRelativeId] = useState(null);
  const [relativeIdError, setRelativeIdError] = useState(false);
  const classes = useStyles();
  const { onClose, open, tenantName, translation } = props;

  const tenantId = parseInt(props.tenantId);

  const handleSubmit = (e) => {
    if (relationshipId && relativeId) {
      props.addRelative(tenantId, relativeId, relationshipId);
      onClose();
    } else {
      setRelationshipIdError(relationshipId === null);
      setRelativeIdError(relativeId === null);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "relationship") {
      setRelationshipId(value);
      setRelationshipIdError(false);
    }
    if (name === "relative") {
      setRelativeId(value);
      setRelativeIdError(false);
    }
  };

  return (
    <div>
      <Dialog
        fullWidth
        onClose={() => {
          onClose();
          setRelationshipId(null);
          setRelationshipIdError(false);
          setRelativeId(null);
          setRelativeIdError(false);
        }}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title">Relative</DialogTitle>
        <DialogContent>
          <FormControl
            fullWidth
            margin="normal"
            variant="outlined"
            className={classes.formControl}
            error={relativeIdError}
          >
            <InputLabel id="demo-simple-select-outlined-label">
              Relative Name
            </InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              name="relative"
              onChange={handleChange}
              label="Relative Name"
            >
              {props.tenants
                .filter((tenant) => tenant.name != tenantName)
                .map((tenant) => (
                  <MenuItem key={tenant.id} value={tenant.id}>
                    {tenant.name}
                  </MenuItem>
                ))}
            </Select>
            {relativeIdError ? (
              <Typography style={
                {
                  color: "#f44336",
                  fontSize: "0.75rem",
                  marginLeft: "14px"
                }
              }>
                {translation('tenant.relative.nameError')}
              </Typography>
            ) : (
              ""
            )}
          </FormControl>
          <FormControl
            fullWidth
            margin="normal"
            variant="outlined"
            className={classes.formControl}
            error={relationshipIdError}
          >
            <InputLabel id="demo-simple-select-outlined-label">
              Relationship
            </InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              name="relationship"
              onChange={handleChange}
              label="Relationship"
            >
              {props.relationships.map((relationship) => {
                return (
                  <MenuItem
                    key={relationship.relationshipId}
                    value={relationship.relationshipId}
                  >
                    {relationship.forwardRelation}
                  </MenuItem>
                );
              })}
            </Select>
            {relationshipIdError ? (
              <Typography style={
                {
                  color: "#f44336",
                  fontSize: "0.75rem",
                  marginLeft: "14px"
                }
              }>
                {translation('tenant.relative.relationShipError')}
              </Typography>
            ) : (
              ""
            )}
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button variant="primary" color="primary" onClick={handleSubmit}>
            ADD
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function RelativeList(props) {
  const [openUploadDialog, setOpenUploadDialog] = useState(false);
  const { tenantId, loadRelatives, getRelationships, loadTenants, tenantName } = props;
  const classes = useRelativeListStyles();
  const { t, i18n } = useTranslation();

  useEffect(() => {
    loadRelatives(tenantId);
    getRelationships();
  }, [loadRelatives, getRelationships, tenantId]);

  useEffect(() => {
    loadTenants();
  }, [loadTenants]);

  return (
    <div>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
            Relatives
          </Typography>
        </div>
        <div className={classes.headerNewLinkContainer}>
          <Link
            color="primary"
            onClick={(e) => {
              setOpenUploadDialog(true);
            }}
          >
            New
          </Link>
        </div>
      </div>
      <Divider />
      <List className={classes.list}>
        {props.relatives.map((relative) => (
          <Fragment key={relative.relativeId}>
            <ListItem className={classes.listItem}>
              <div className={classes.relativeTitle}>
                <div className={classes.relativeTitleLabel}>
                  <Avatar
                    className={classes.relativeImage}
                    alt="profile"
                    src={
                      config.api.BASE_URL +
                      "/image/" +
                      relative.relativePhotoId +
                      "/thumb"
                    }
                  />
                  <Link color="primary" href={"/tenant/" + relative.relativeId}>
                    <Typography color="primary">
                      {relative.relativeName
                        .split(" ")
                        .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                        .join(" ")}
                    </Typography>
                  </Link>
                </div>
                <div className={classes.relativeTitleLink}>
                  <Link
                    color="primary"
                    onClick={(e) => {
                      console.log(relative);
                      props.deleteRelative(
                        relative.tenantId,
                        relative.relationshipId
                      );
                    }}
                  >
                    Delete
                  </Link>
                </div>
              </div>
            </ListItem>
          </Fragment>
        ))}
      </List>
      <AddRelativeDialog
        tenantId={tenantId}
        tenantName={tenantName}
        tenants={props.tenants}
        open={openUploadDialog}
        onClose={() => {
          setOpenUploadDialog(false);
        }}
        addRelative={props.addRelative}
        relationships={props.relationships}
        translation={t}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadRelatives: (tenantId) => {
      dispatch(tenantActions.getTenantRelatives(tenantId));
    },
    deleteRelative: (tenantId, relationshipId) => {
      dispatch(tenantActions.deleteRelative(tenantId, relationshipId));
    },
    getRelationships: () => {
      dispatch(tenantActions.getRelationships());
    },
    addRelative: (tenantId, relativeId, relationshipId) => {
      dispatch(tenantActions.addRelative(tenantId, relativeId, relationshipId));
    },
    loadTenants: () => {
      dispatch(tenantActions.getTenants());
    },
  };
};

const mapStateToProps = (state) => {
  return {
    relatives: state.tenants.tenantRelatives,
    tenants: state.tenants.tenantList,
    relationships: state.tenants.relationships,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(RelativeList);

export { x as RelativeList };
