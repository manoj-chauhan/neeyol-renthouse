import React, { useState, useEffect, Fragment } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Dialog from "@material-ui/core/Dialog";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { connect } from "react-redux";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Box from "@material-ui/core/Box";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { tenantActions } from "../../_actions";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: "100%",
  },
  dialogTitle: {
    color: "blue",
  },
}));

const useContactListStyles = makeStyles((theme) => ({
  list: {
    padding: 0,
  },
  listItem: {
    padding: 0,
    marginBottom: theme.spacing(2),
    display: "block",
  },
  header: {
    padding: 0,
    display: "flex",
    justifyContent: "flex-end",
    gutterBottom: 2,
  },
  headerLabel: {
    width: "100%",
  },
  headerNewLinkContainer: {
    display: "flex",
    alignItems: "center",
  },
  contactTitle: {
    display: "flex",
    justifyContent: "flex-end",
  },
  contactTitleLabel: {
    width: "100%",
  },
  contactTitleLink: {
    paddingLeft: theme.spacing(2),
    display: "flex",
    alignItems: "center",
  },
  contact: {
    width: "100%",
  },
}));

function NewContactDialog(props) {
  const [contact, setContact] = useState("");
  const [contactError, setContactError] = useState(false);
  const [type, setType] = useState("");
  const [typeError, setTypeError] = useState(false);

  const { onClose, open, translation } = props;
  const classes = useStyles();

  const handleSubmit = (e) => {
    if (contact && type) {
      props.uploadContact({
        contact: contact,
        tenantId: props.tenantId,
        type: type,
      });
      onClose();
      setType("");
      setTypeError(false);
      setContact("");
      setContactError(false);
    } else {
      setContactError(contact.length === 0);
      setTypeError(type.length === 0);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "contact") {
      setContact(value);
      if (value.length === 0) {
        setContactError(true);
      } else {
        setContactError(false);
      }
    }
    if (name === "type") {
      setType(value);
      if (value.length === 0) {
        setTypeError(true);
      } else {
        setTypeError(false);
      }
    }
  };

  const suggestionOfContactType = ["Home", "Office", "Personal"];

  return (
    <div>
      <Dialog
        onClose={() => {
          onClose();
          setType("");
          setTypeError(false);
          setContact("");
          setContactError(false);
        }}
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          New Contact
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <Autocomplete
              freeSolo
              onChange={(event, newValue) => {
                setType(newValue);
                setTypeError(false);
              }}
              options={suggestionOfContactType}
              getOptionLabel={(option) => option}
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={typeError}
                  variant="outlined"
                  margin="normal"
                  name="type"
                  label="Type"
                  type="text"
                  onChange={handleChange}
                  helperText={typeError ? translation('tenant.contact.typeError') : ""}
                />
              )}
            />

            <TextField
              error={contactError}
              variant="outlined"
              margin="normal"
              name="contact"
              label="Phone Number"
              type="text"
              onChange={handleChange}
              helperText={contactError ? translation('tenant.contact.numberError') : ""}
            />
            <DialogActions>
              <Button
                color="secondary"
                variant="contained"
                onClick={handleSubmit}
              >
                ADD
              </Button>
            </DialogActions>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function ContactList(props) {
  const [openNewContactDialog, setOpenNewContactDialog] = useState(false);

  const classes = useContactListStyles();
  const { tenantId, loadContacts } = props;
  const { t, i18n } = useTranslation();

  useEffect(() => {
    loadContacts(tenantId);
  }, [loadContacts, tenantId]);

  return (
    <div>
      <div className={classes.header}>
        <div className={classes.headerLabel}>
          <Typography component="h6" variant="h6" color="primary">
            Contacts
          </Typography>
        </div>
        <div className={classes.headerNewLinkContainer}>
          <Link
            color="primary"
            onClick={(e) => {
              setOpenNewContactDialog(true);
            }}
          >
            New
          </Link>
        </div>
      </div>
      <Divider />
      <List className={classes.list}>
        {props.contacts.map((contact) => (
          <Fragment key={contact.tenantContactId}>
            <ListItem className={classes.listItem}>
              <div className={classes.contactTitle}>
                <div className={classes.contactTitleLabel}>
                  <Typography variant="overline" display="block">
                    <Box lineHeight={2}>{contact.type}</Box>
                  </Typography>
                </div>
                <div className={classes.contactTitleLink}>
                  <Link
                    color="primary"
                    onClick={(e) => {
                      props.deleteContact({
                        tenantId: tenantId,
                        contactId: contact.tenantContactId,
                      });
                    }}
                  >
                    Delete
                  </Link>
                </div>
              </div>
              <div className={classes.contact}>
                <Typography gutterBottom>{contact.phoneNumber}</Typography>
              </div>
            </ListItem>
          </Fragment>
        ))}
      </List>

      <NewContactDialog
        tenantId={tenantId}
        open={openNewContactDialog}
        onClose={() => {
          setOpenNewContactDialog(false);
        }}
        uploadContact={props.uploadContact}
        translation={t}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadContacts: (tenantId) => {
      dispatch(tenantActions.getContacts(tenantId));
    },
    uploadContact: (tenant) => {
      dispatch(tenantActions.uploadContact(tenant));
    },
    deleteContact: (tenant) => {
      dispatch(tenantActions.delContact(tenant));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    contacts: state.tenants.tenantContacts,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(ContactList);

export { x as ContactList };
