import config from "../../config";
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useParams } from "react-router-dom";
import CardMedia from "@material-ui/core/CardMedia";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { bookActions, tenantActions } from "../../_actions";
import { DocumentList } from "./DocumentList";
import { ContactList } from "./ContactList";
import { AddressList } from "./AddressList";
import { RelativeList } from "./RelativeList";
import IconButton from "@material-ui/core/IconButton";
import Edit from "@material-ui/icons/Edit";
import {
  Avatar,
  CircularProgress,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { history } from "../../_helpers";
import moment from "moment";
import { validate } from "@material-ui/pickers";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100%",
  },
  identity: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: theme.spacing(1),
  },
  identityName: {
    display: "flex",
  },
  details: {},
  content: {},
  section: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  cover: {
    width: 90,
    height: 100,
    transform: "rotate(0deg)",
  },
  coverLarge: {
    width: "100%",
  },
  controls: {
    display: "flex",
    alignItems: "center",
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  subSectionHeader: {
    alignItems: "center",
  },
  linkContainer: {
    display: "flex",
    width: 80,
    justify: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  info: {
    paddingTop: 16,
  },
  address: {
    paddingTop: 16,
    height: 160,
  },
  name: {
    height: 80,
  },
  editButton: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 10,
    marginRight: 10,
  },
  tenantName: {
    paddingTop: 20,
  },
  availability: {
    marginTop: 10,
    marginLeft: 60,
  },
  alignCurrentBalance: {
    marginTop: 20,
  },
  noProfileImage: {
    height: 100,
    width: 100,
    backgroundColor: "#111",
    borderRadius: 20,
  },
  imageLoaderSize: {
    position: "absolute",
    zIndex: 1,
    top: 40,
    right: 40
  }
}));

const styleOfUpdateTenantProfile = makeStyles((theme) => ({
  dialogTitle: {
    color: "Blue",
  },
}));

function AddTenantBalance(props) {
  const { open, onClose } = props;
  const [balance, setBalance] = useState(0);
  const [balanceError, setBalanceError] = useState(false);
  const [date, setDate] = useState("");
  const classes = styleOfUpdateTenantProfile();

  const { t, i18n } = useTranslation();

  const validate = () => {
    if (balance.length === undefined) {
      setBalanceError(true);
      return false;
    }
    return true;
  };

  const handleSubmit = () => {
    if (validate()) {
      let x = moment.utc(date);
      props.recordBalance({
        tenantAccountId: props.tenantAccountId,
        balance: balance,
        comment: "add balance of tenant " + props.tenantAccountId,
        time: x.toISOString(),
      });
      onClose();
      setBalance(0);
      setBalanceError(false);
    }
  };

  const handleChange = (e) => {
    if (e.target.name === "balance") {
      setBalance(e.target.value);
      setBalanceError(false);
    }
    if (e.target.name === "date") {
      setDate(e.target.value);
    }
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => {
          onClose();
          setBalance(0);
          setBalanceError(false);
        }}
      >
        <DialogTitle className={classes.dialogTitle}>Add Balance</DialogTitle>
        <DialogContent>
          <TextField
            error={balanceError}
            variant="outlined"
            fullWidth
            margin="normal"
            name="balance"
            label="Balance"
            defaultValue={0}
            onChange={handleChange}
            helperText={
              balanceError ? t('tenant.balanceError') : ""
            }
          />

          <TextField
            id="date"
            variant="outlined"
            label="Date"
            fullWidth
            type="date"
            name="date"
            margin="normal"
            defaultValue={moment.utc(moment()).toISOString().substring(0, 10)}
            onChange={handleChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit}
            >
              Add
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function UpdateTenantProfile(props) {
  const { open, onClose, translation } = props;
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState(false);
  const classes = styleOfUpdateTenantProfile();

  const handleSubmit = () => {
    if (name) {
      props.updateTenantProfile({
        tenantId: props.tenantId,
        name: name,
      });
    } else {
      setNameError(name.length === 0);
    }
  };

  const handleOnChange = (e) => {
    if (e.target.name === "editedName") {
      setNameError(e.target.value.length === 0);
      setName(e.target.value.length !== 0 ? e.target.value : "");
    }
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => {
          onClose();
          setName("");
          setNameError(false);
        }}
      >
        <DialogTitle className={classes.dialogTitle}>
          Update Tenant Profile
        </DialogTitle>
        <DialogContent>
          <TextField
            error={nameError}
            variant="outlined"
            margin="normal"
            name="editedName"
            label="Name"
            defaultValue={props.name ? props.name : ""}
            onChange={handleOnChange}
            helperText={nameError ? translation('common.nameError') : ""}
          />
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit}
            >
              Save
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const useStylesImageDialog = makeStyles((theme) => ({
  img: {
    height: 600,
  },
}));

function ImageDialog(props) {
  const { onClose, open } = props;
  const classes = useStylesImageDialog();

  return (
    <div>
      <Dialog
        onClose={() => {
          onClose();
        }}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <img
          className={classes.img}
          src={config.api.BASE_URL + "/image/" + props.imageId}
          alt=""
        />
      </Dialog>
    </div>
  );
}

function TenantPersonalInfo(props) {
  const classes = useStyles();
  const { tenantId } = useParams();
  const [imageOpen, setImageOpen] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");
  const [image, setImage] = useState(null);
  const [
    updateTenantProfileDialogOpen,
    setUpdateTenantProfileDialogOpen,
  ] = useState(false);
  const [addBalanceDialogOpen, setAddBalanceDialogOpen] = useState(false);
  const [loaderSize, setLoaderSize] = useState(50);
  const [imageId, setImageId] = useState(null);
  const { i18n, t } = useTranslation();

  useEffect(() => {
    if (props.info.imageList && props.info.imageList.length > 0) {
      const length = props.info.imageList.length;
      // imageId = props.info.imageList[length - 1].imageId;
      setImageId(props.info.imageList[length - 1].imageId);
      // setLoader(true);
    }
  }, [props.info.imageList]);


  const handleChange = (e) => {
    const { name } = e.target;
    if (name === "image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
      props.uploadPhoto({
        image: e.target.files[0],
        caption: "tenant image" + tenantId,
        tenantId: tenantId,
      });
    }
  };

  const handleUpdateTenantName = () => {
    setUpdateTenantProfileDialogOpen(true);
  };


  return (
    <div className={classes.root}>
      <Grid xs={12} className={classes.identity}>
        <Grid>
          <Grid className={classes.identityName}>
            {props.info.name ? (
              props.info.name.length > 10 ? (
                <>
                  <Grid className={classes.alignTenantName}>
                    <Typography component="h5" variant="h6">
                      {props.info.name
                        .split(" ")
                        .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                        .join(" ")}
                    </Typography>
                  </Grid>
                </>
              ) : (
                <>
                  <Grid className={classes.tenantName}>
                    <Typography component="h5" variant="h5">
                      {props.info.name
                        .split(" ")
                        .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                        .join(" ")}
                    </Typography>
                  </Grid>
                </>
              )
            ) : (
              ""
            )}
            <Grid>
              <IconButton
                className={classes.editIcon}
                onClick={handleUpdateTenantName}
              >
                <Edit />
              </IconButton>
            </Grid>
          </Grid>
          <Grid
            item
            xs={12}
            alignItems="center"
            className={classes.alignCurrentBalance}
          >
            <Typography>
              Balance:
              {props.info.balance === 0 ? (
                <> Rs {Math.round(props.info.balance)}</>
              ) : props.info.balance > 0 ? (
                <> Rs {Math.round(props.info.balance)} Due</>
              ) : (
                <>
                  Rs {Math.round(props.info.balance * -1)} Advance
                </>
              )}
            </Typography>
          </Grid>
        </Grid>
        <Grid>
          {imageId ? (
            <>
              <CircularProgress disableShrink={true} size={loaderSize} className={classes.imageLoaderSize} />
              <img
                src={config.api.BASE_URL + "/image/" + imageId + "/thumb"}
                onClick={(e) => {
                  setSelectedImage(imageId);
                  setImageOpen(true);
                }}
                onLoad={() => {
                  setLoaderSize(0);
                }}
                className={classes.cover}
              />

            </>
          ) : (
            <Avatar
              src="/broken-image.jpg"
              className={classes.noProfileImage}
            />
          )}
        </Grid>
      </Grid>
      <div className={classes.editButton}>
        <Button
          variant="contained"
          color="primary"
          component="label"
          onClick={() => {
            setAddBalanceDialogOpen(true);
          }}
        >
          Add Balance
        </Button>
        <Button variant="contained" color="primary" component="label">
          Edit
          <input name="image" type="file" hidden onChange={handleChange} />
        </Button>
      </div>
      <div className={classes.btnSection}>
        <Button
          variant="contained"
          color="primary"
          // href={"/tenant/" + tenantId + "/payment"}
          onClick={() => {
            history.push("/tenant/" + tenantId + "/payment");
          }}
        >
          New Payment
        </Button>
      </div>
      <div className={classes.section}>
        <AddressList tenantId={tenantId} />
      </div>
      <div className={classes.section}>
        <ContactList tenantId={tenantId} />
      </div>
      <div className={classes.section}>
        <DocumentList tenantId={tenantId} />
      </div>
      <div className={classes.section}>
        <RelativeList
          tenantId={tenantId}
          tenantName={props.info.name}
        />
      </div>

      <ImageDialog
        open={imageOpen}
        onClose={() => {
          setImageOpen(false);
        }}
        imageId={selectedImage}
      />

      <UpdateTenantProfile
        open={updateTenantProfileDialogOpen}
        onClose={() => {
          setUpdateTenantProfileDialogOpen(false);
        }}
        name={props.info.name}
        tenantId={tenantId}
        updateTenantProfile={props.updateTenantProfile}
        translation={t}
      />

      <AddTenantBalance
        open={addBalanceDialogOpen}
        onClose={() => {
          setAddBalanceDialogOpen(false);
        }}
        tenantAccountId={props.info.accountId}
        recordBalance={props.recordBalance}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    delImage: (tenant) => {
      dispatch(tenantActions.delImage(tenant));
    },
    uploadPhoto: (tenantInfo) => {
      dispatch(tenantActions.uploadPhoto(tenantInfo));
    },
    updateTenantProfile: (tenantProfileInfo) => {
      dispatch(tenantActions.updateTenantProfile(tenantProfileInfo));
    },
    recordBalance: (tenantBalanceDetails) => {
      dispatch(bookActions.recordBalance(tenantBalanceDetails));
    },
  };
};

const mapStateToProps = (state) => {
  return {};
};

const x = connect(mapStateToProps, mapDispatchprops)(TenantPersonalInfo);

export { x as TenantPersonalInfo };
