import React, { useEffect } from "react";
import { List, Grid, Typography, Button } from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { tenantActions } from "../../_actions";
import { TenantListItem } from "./TenantListItem";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { history } from "../../_helpers";

const useStyles = makeStyles((theme) => ({
  tenantList: {
    width: "100%",
  },
  divider: {
    height: "5px",
  },
  root: {},
  paper: {
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  tenantIcon: {
    fontSize: 150,
    marginRight: 40,
    color: "#171414",
  },
  alignTenantIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 150,
  },
  styleOfAddTenant: {
    margin: 10,
  },
}));

function TenantList(props) {
  const loadTenants = props.loadTenants;

  useEffect(() => {
    loadTenants();
  }, [loadTenants]);

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List>
        {props.tenantList
          .sort((firstName, secondName) => {
            return firstName.name.localeCompare(secondName.name);
          })
          .map((tenant) => (
            <TenantListItem key={tenant.id} tenantInfo={tenant} />
          ))}
      </List>

      {props.tenantList.length === 0 ? (
        <Grid items xs={12}>
          <div className={classes.alignTenantIcon}>
            <PersonAddIcon className={classes.tenantIcon} />
            <Typography>No Tenant Added Yet</Typography>
            <Button
              variant="contained"
              color="primary"
              size="small"
              className={classes.styleOfAddTenant}
              onClick={() => {
                history.push("/tenant/new");
              }}
            >
              Add Tenant
            </Button>
          </div>
        </Grid>
      ) : (
        ""
      )}
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadTenants: () => dispatch(tenantActions.getTenants()),
  };
};

const mapStateToProps = (state) => {
  return {
    isLoading: state.tenants.isLoading,
    tenantList: state.tenants.tenantList,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(TenantList)
);

export { x as TenantList };
