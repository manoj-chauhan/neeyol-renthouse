import React from "react";
import config from "../../config";
import { ListItem } from "@material-ui/core";
import { history } from "../../_helpers";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "80px",
    display: "flex",
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  content: {
    flex: "1 0 auto",
  },
  cover: {
    width: 80,
  },
}));

function BuildingListItem(props) {
  const classes = useStyles();

  const building = props.buildingInfo;

  return (
    <ListItem
      onClick={(e) => {
        history.push("/building/" + building.id);
      }}
    >
      <Card className={classes.root}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography
              className={classes.title}
              color="textPrimary"
              gutterBottom
            >
              {building.name
                .split(" ")
                .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                .join(" ")}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {building.address}
            </Typography>
          </CardContent>
        </div>
        <CardMedia
          className={classes.cover}
          image={config.api.BASE_URL + "/image/" + building.imageId + "/thumb"}
          title="Live from space album cover"
        />
      </Card>
    </ListItem>
  );
}

const x = withStyles(useStyles)(BuildingListItem);
export { x as BuildingListItem };
