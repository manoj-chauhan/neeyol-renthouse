import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";
import { buildingActions } from "../../_actions";
import {
  CircularProgress,
  FormLabel,
  OutlinedInput,
  Grid,
} from "@material-ui/core";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function NewBuilding(props) {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [nameError, setNameError] = useState(false);
  const [addressError, setAddressError] = useState(false);
  const [buildingImage, setBuildingImage] = useState("");
  const [addBuildingButtonDisable, setAddBuildingButtonDisable] = useState(
    false
  );
  const classes = useStyles();

  const { t, i18n } = useTranslation();

  useEffect(() => {
    if (props.building.loaderVisible) {
      setAddBuildingButtonDisable(true);
    } else {
      setAddBuildingButtonDisable(false);
    }
  }, [props.building.loaderVisible]);

  const validation = () => {
    setNameError(name.length === 0);
    setAddressError(address.length === 0);
    return !(name.length === 0 || address.length === 0);
  };

  const handleSubmit = (e) => {
    if (validation()) {
      props.createBuilding({
        name: name,
        address: address,
        imageId: props.building.buildingPhotoId,
        caption: "Building Name Is " + name,
      });
      setAddBuildingButtonDisable(true);
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    if (name === "name") {
      setName(value);
      if (value.length === 0) {
        setNameError(true);
      } else {
        setNameError(false);
      }
    } else if (name === "address") {
      setAddress(value);
      if (value.length === 0) {
        setAddressError(true);
      } else {
        setAddressError(false);
      }
    } else if (name === "buildingImage") {
      setBuildingImage(
        event.target.files.length > 0 ? event.target.files[0].name : null
      );
      if (event.target.files.length > 0) {
        props.uploadBuildingPhotoWithoutBuildingId(event.target.files[0]);
        setAddBuildingButtonDisable(true);
      }
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          New Building
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            error={nameError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Building Name"
            name="name"
            autoFocus
            onChange={handleChange}
            helperText={nameError ? t('common.nameError') : " "}
          />
          <TextField
            error={addressError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="address"
            label="Building Address"
            type="address"
            id="address"
            autoFocus
            onChange={handleChange}
            helperText={addressError ? t('building.addressError') : " "}
          />

          <Grid container alignItems="center">
            <Grid item xs={props.building.imageFieldSize}>
              <FormLabel component="legend">
                Building Image (Optional)
              </FormLabel>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="buildingImage"
                type="file"
                id="buildingImage"
                autoFocus
                onChange={handleChange}
              />
            </Grid>
            {props.building.loaderVisible ? <CircularProgress size={30} /> : ""}
          </Grid>

          <Button
            disabled={addBuildingButtonDisable}
            className={classes.submit}
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleSubmit}
          >
            ADD Building
          </Button>
        </form>
      </div>
    </Container>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    uploadBuildingPhotoWithoutBuildingId: (buildingPhotoId) =>
      dispatch(
        buildingActions.uploadBuildingPhotoWithoutBuildingId(buildingPhotoId)
      ),
    createBuilding: (building) =>
      dispatch(buildingActions.createBuilding(building)),
  };
};

const mapStateToProps = (state) => {
  return {
    building: state.buildings,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(NewBuilding)
);

export { x as NewBuilding };
