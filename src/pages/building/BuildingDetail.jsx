import config from "../../config";
import React, { useState, useEffect, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import CardMedia from "@material-ui/core/CardMedia";
import { connect } from "react-redux";
import { buildingActions } from "../../_actions";
import { useParams } from "react-router-dom";
import { Divider, Grid, Avatar, CircularProgress } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Edit from "@material-ui/icons/Edit";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { history } from "../../_helpers";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { roomActions } from "../../_actions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { FormControl, FormLabel, OutlinedInput } from "@material-ui/core";
import HomeWorkIcon from "@material-ui/icons/HomeWork";
import HomeIcon from "@material-ui/icons/Home";
import { useTranslation } from "react-i18next";
import { TrainRounded } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  personalInfo: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: theme.palette.background.paper,
  },
  cover: {
    width: 80,
    height: 100,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 150,
  },
  info: {
    height: 100,
    justifyContent: "space-between",
  },
  newButton: {
    display: "flex",
    justifyContent: "flex-end",
  },
  btn: {
    marginLeft: 16,
  },
  roomSecondaryInfo: {
    width: "100%",
  },
  meterInfo: {
    display: "flex",
    justifyContent: "flex-end",
  },
  editButton: {
    display: "flex",
    justifyContent: "flex-end",
    margin: 5,
  },
  formControl: {
    width: "100%",
  },
  noProfileImage: {
    width: 80,
    height: 100,
    borderRadius: 20,
    background: "#111",
    color: "white",
  },
  imageLoader: {
    position: "absolute"
  }
}));

function BuildingDetail(props) {
  const [image, setImage] = useState(null);
  const [imageOpen, setImageOpen] = useState(false);
  const [loaderSize, setLoaderSize] = useState(70);
  const [selectedImage, setSelectedImage] = useState("");
  const [editedName, setEditedName] = useState("");
  const [editedAddress, setEditedAddress] = useState("");
  const [updateBuildingDialogOpen, setUpdateBuildingDialogOpen] = useState(
    false
  );
  const [nameError, setNameError] = useState(false);
  const [addressError, setAddressError] = useState(false);
  const { i18n, t } = useTranslation();

  let imageId = null;
  let { buildingId } = useParams();

  const loadBuilding = props.loadBuilding;
  const loadRooms = props.loadRooms;

  useEffect(() => {
    setEditedName(props.building.name);
    setEditedAddress(props.building.address);
  }, [props.building]);

  useEffect(() => {
    loadBuilding(buildingId);
  }, [loadBuilding, buildingId]);

  useEffect(() => {
    loadRooms(buildingId);
  }, [loadRooms, buildingId]);
  const classes = useStyles();

  const handleNewRoom = (e) => {
    history.push("/building/" + buildingId + "/room/new");
  };

  const handleChange = (e) => {
    const { name } = e.target;
    if (name === "image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
      props.uploadBuildingPhoto({
        image: e.target.files[0],
        caption: "building image of" + buildingId,
        buildingId: buildingId,
      });
    }
  };

  const handleOnChange = (e) => {
    const { name, value } = e.target;
    if (name === "editedName") {
      setNameError(value.length === 0);
      setEditedName(value.length !== 0 ? value : "");
    } else if (name === "editedAddress") {
      setAddressError(value.length === 0);
      setEditedAddress(value.length !== 0 ? value : "");
    }
  };

  const handleOnSave = (e) => {
    if (editedName && editedAddress) {
      props.updateBuilding({
        id: buildingId,
        name: editedName,
        address: editedAddress,
      });
    } else {
      setNameError(editedName.length == 0);
      setAddressError(editedAddress.length === 0);
    }
  };

  const handleOnCancel = (e) => {
    setUpdateBuildingDialogOpen(false);
    setNameError(false);
    setAddressError(false);
    setEditedName("");
    setEditedAddress("");
  };

  const handleOnBuildingEditClick = (e) => {
    setUpdateBuildingDialogOpen(true);
  };

  if (props.building.imageList && props.building.imageList.length > 0) {
    const length = props.building.imageList.length;
    imageId = props.building.imageList[length - 1].imageId;
  }

  return (
    <div className={classes.root}>
      <Grid container className={classes.info}>
        <Grid item>
          <Grid container>
            <Grid item>
              <Typography component="h6" variant="h6" color="textPrimary">
                {props.building.name
                  ? props.building.name
                    .split(" ")
                    .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                    .join(" ")
                  : ""}
              </Typography>
            </Grid>
            <Grid item>
              <IconButton
                onClick={handleOnBuildingEditClick}
                style={{
                  paddingTop: 0,
                }}
              >
                <Edit />
              </IconButton>
            </Grid>
          </Grid>
          <Typography>{props.building.address}</Typography>
        </Grid>

        <Grid item>
          {imageId ? (
            <>
              <CircularProgress disableShrink={true} size={loaderSize} className={classes.imageLoader} />
              <img
                src={config.api.BASE_URL + "/image/" + imageId + "/thumb"}
                onClick={(e) => {
                  setSelectedImage(imageId);
                  setImageOpen(true);
                }}
                onLoad={() => {
                  setLoaderSize(0);
                }}
                className={classes.cover}
              />
            </>
          ) : (
            <div>
              <HomeIcon className={classes.noProfileImage} />
            </div>
          )}
        </Grid>
      </Grid>
      <Grid className={classes.editButton}>
        <Button variant="contained" color="primary" component="label">
          Edit
          <input name="image" type="file" hidden onChange={handleChange} />
        </Button>
      </Grid>
      <Grid container>
        <Grid item xs={6}>
          <Typography component="h6" variant="h6" color="primary">
            Rooms
          </Typography>
        </Grid>
        <Grid item xs={6} className={classes.newButton}>
          <Button variant="outlined" color="primary" onClick={handleNewRoom}>
            Add Room
          </Button>
        </Grid>
      </Grid>
      <List className={classes.list}>
        {props.roomList.map((room) => (
          <Fragment key={room.id}>
            <ListItem
              disableGutters
              onClick={(e) => {
                history.push("/building/" + buildingId + "/room/" + room.id);
              }}
            >
              <div className={classes.roomSecondaryInfo}>
                <div>
                  <Typography variant="subtitle1">{room.name}</Typography>
                </div>
                <Grid container>
                  <Grid item xs={8}>
                    <Typography variant="subtitle2">
                      <div>
                        {room.allocated
                          ? "Allocated to " + room.tenantName
                          : room.roomAvailability
                            ? "Available"
                            : !room.roomAvailability ? "Not available"
                              : "Not Allocated"}
                      </div>
                    </Typography>
                  </Grid>
                  <Grid item xs={4} className={classes.meterInfo}>
                    <Typography variant="subtitle2">
                      <div>
                        {room.meterIsAllocated ? room.meterName : "No Meter"}
                      </div>
                    </Typography>
                  </Grid>
                </Grid>
              </div>
            </ListItem>
            <Divider />
          </Fragment>
        ))}
      </List>
      <Dialog
        // onClose={handleClose}
        // aria-labelledby="simple-dialog-title"
        open={updateBuildingDialogOpen}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Update Building
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <TextField
              error={nameError}
              variant="outlined"
              margin="normal"
              name="editedName"
              label="Name"
              value={editedName}
              defaultValue=" "
              onChange={handleOnChange}
              helperText={nameError ? t('common.nameError') : ""}
            />
          </FormControl>
          <FormControl className={classes.formControl}>
            <TextField
              error={addressError}
              variant="outlined"
              margin="normal"
              name="editedAddress"
              label="Address"
              value={editedAddress}
              defaultValue=" "
              onChange={handleOnChange}
              helperText={addressError ? t('building.addressError') : ""}
            />
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleOnSave}>
            Save
          </Button>
          <Button color="primary" onClick={handleOnCancel}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadBuilding: (id) => dispatch(buildingActions.getBuilding(id)),
    loadRooms: (buildingId) => dispatch(roomActions.getRooms(buildingId)),
    uploadBuildingPhoto: (buildingInfo) =>
      dispatch(buildingActions.uploadBuildingPhoto(buildingInfo)),
    updateBuilding: (building) =>
      dispatch(buildingActions.updateBuilding(building)),
  };
};

const mapStateToProps = (state) => {
  return {
    building: { ...state.buildings.building },
    roomList: state.rooms.roomList,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(BuildingDetail);

export { x as BuildingDetail };
