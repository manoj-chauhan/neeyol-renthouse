import React, { useEffect } from "react";
import { List, Grid, Button, Typography } from "@material-ui/core";
// import AddIcon from '@material-ui/icons/Add';
import { connect } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { buildingActions } from "../../_actions";
import { BuildingListItem } from "./BuildingListItem";
import HomeWorkRoundedIcon from "@material-ui/icons/HomeWorkRounded";
import { history } from "../../_helpers";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  topBar: {
    padding: 16,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 8,
  },
  homeIcon: {
    fontSize: 150,
    color: "#171414",
  },
  alignHomeIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 150,
  },
  styleOfAddBuilding: {
    margin: 10,
  },
}));

function BuildingList(props) {
  const classes = useStyles();
  const loadBuildings = props.loadBuildings;

  useEffect(() => {
    loadBuildings();
  }, [loadBuildings]);

  return (
    <div>
      <List>
        {props.buildingList.map((building) => (
          <BuildingListItem key={building.id} buildingInfo={building} />
        ))}
      </List>
      {props.buildingList.length === 0 ? (
        <Grid items xs={12}>
          <div className={classes.alignHomeIcon}>
            <HomeWorkRoundedIcon className={classes.homeIcon} />
            <Typography>No Building Added Yet</Typography>
            <Button
              variant="contained"
              color="primary"
              size="small"
              className={classes.styleOfAddBuilding}
              onClick={() => {
                history.push("/building/new");
              }}
            >
              Add Building
            </Button>
          </div>
        </Grid>
      ) : (
        ""
      )}
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadBuildings: () => dispatch(buildingActions.getBuildings()),
  };
};

const mapStateToProps = (state) => {
  return {
    // isLoading: state.buildings.isLoading,
    buildingList: state.buildings.buildingList,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(BuildingList)
);

export { x as BuildingList };
