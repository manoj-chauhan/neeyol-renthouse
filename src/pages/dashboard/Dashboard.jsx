import React, { useEffect } from "react";
// import clsx from 'clsx';
import { makeStyles } from "@material-ui/core/styles";
import {
  Button,
  Grid,
  Paper,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import { connect } from "react-redux";
import { PendingPayment } from "./PendingPayments";
import { RecentTransaction } from "./RecentTransaction";
import { buildingActions, settingActions, tenantActions } from "../../_actions";
import HomeWorkRoundedIcon from "@material-ui/icons/HomeWorkRounded";
import { history } from "../../_helpers";
import SettingsIcon from "@material-ui/icons/Settings";
import TimelineOutlinedIcon from "@material-ui/icons/TimelineOutlined";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 16,
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  personalInfo: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: theme.palette.background.paper,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  homeIcon: {
    fontSize: 150,
  },
  alignHomeIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 150,
  },
  styleOfAddBuilding: {
    margin: 10,
  },
}));

function Dashboard(props) {
  const classes = useStyles();

  const loadBuildings = props.loadBuildings;
  const buildingList = props.buildingList;

  useEffect(() => {
    loadBuildings();
  }, [loadBuildings]);

  const getSetting = props.getSetting;
  const userDefaultSettings = props.userDefaultSettings;

  useEffect(() => {
    getSetting();
  }, [getSetting]);

  const loadTenants = props.loadTenants;
  const tenantList = props.tenantList;

  useEffect(() => {
    loadTenants();
  }, [loadTenants]);

  return (
    <Grid container spacing={2} className={classes.root}>
      {buildingList.length == 0 ? (
        <Grid items xs={12}>
          <div className={classes.alignHomeIcon}>
            <HomeWorkRoundedIcon className={classes.homeIcon} />
            <Typography>To Get Started Add A Building</Typography>
            <Button
              variant="contained"
              color="primary"
              className={classes.styleOfAddBuilding}
              onClick={() => {
                // history.push("/building/new");
                history.push("/buildingWizard");
              }}
            >
              Add Building
            </Button>
          </div>
        </Grid>
      ) : (
        <>
          {Object.keys(userDefaultSettings).length !== 0 &&
            buildingList.length > 0 &&
            tenantList.length > 0 ? (
            <>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <PendingPayment />
                </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <RecentTransaction />
                </Paper>
              </Grid>
            </>
          ) : (
            <>
              {" "}
              {Object.keys(userDefaultSettings).length === 0 ? (
                <Grid items xs={12}>
                  <div className={classes.alignHomeIcon}>
                    <SettingsIcon className={classes.homeIcon} />
                    <Typography>Started To Room Allocation</Typography>
                    <Typography>Add A Room Allocation Setting</Typography>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.styleOfAddBuilding}
                      onClick={() => {
                        history.push("/settings");
                      }}
                    >
                      Add Settings
                    </Button>
                  </div>
                </Grid>
              ) : (
                <Grid items xs={12}>
                  <div className={classes.alignHomeIcon}>
                    <TimelineOutlinedIcon className={classes.homeIcon} />
                    <Typography>Started To Room Allocation</Typography>
                    <Typography>Add A Tenant</Typography>
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.styleOfAddBuilding}
                      onClick={() => {
                        history.push("/tenant/new");
                      }}
                    >
                      Add Tenants
                    </Button>
                  </div>
                </Grid>
              )}
            </>
          )}
        </>
      )}
    </Grid>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadBuildings: () => dispatch(buildingActions.getBuildings()),
    getSetting: () => dispatch(settingActions.getSetting()),
    loadTenants: () => dispatch(tenantActions.getTenants()),
  };
};

const mapStateToProps = (state) => {
  return {
    buildingList: state.buildings.buildingList,
    userDefaultSettings: state.setting.defaultSetting,
    tenantList: state.tenants.tenantList,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(Dashboard);

export { x as Dashboard };
