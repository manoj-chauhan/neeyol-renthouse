import React from "react";
import { tenantActions } from "../../_actions";
import { connect } from "react-redux";
import { Button, Divider, List } from "@material-ui/core";
import { Grid, CardContent, ListItem, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Title from "../widgets/Title";
import { history } from "../../_helpers";
import Avatar from "@material-ui/core/Avatar";
import config from "../../config";

function createData(name, balance, address, id) {
  return { name, balance, address, id };
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  button: {
    color: "white",
  },
  itemDetails: {
    display: "flex",
  },
  avatarImage: {
    marginRight: 20,
  },
}));

function PendingPayment(props) {
  const classes = useStyles();
  const tenantdues = props.tenantdues;

  React.useEffect(() => {
    tenantdues();
  }, [tenantdues]);
  return (
    <React.Fragment>
      <Title>Pending Payments</Title>
      <List>
        {props.tenantDues.length > 0 ? (
          props.tenantDues.map((row) => (
            <>
              <ListItem disableGutters={true}>
                <Grid container spacing={1} className={classes.container}>
                  <Grid item xs={8}>
                    <Grid className={classes.itemDetails}>
                      <Avatar
                        className={classes.avatarImage}
                        src={
                          config.api.BASE_URL +
                          "/image/" +
                          row.imageId +
                          "/thumb"
                        }
                      />
                      <Grid>
                        <Typography>
                          {row.name.slice(0, 1).toUpperCase() +
                            row.name.slice(1, row.name.length)}
                        </Typography>
                        <Typography>Rs: {Math.round(row.balance)}</Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid>
                    <Button
                      variant="contained"
                      color="secondary"
                      size="small"
                      className={classes.button}
                      onClick={(e) => {
                        history.push("/tenant/" + row.id + "/payment");
                      }}
                    >
                      Record
                    </Button>
                  </Grid>
                </Grid>
              </ListItem>
              <Divider />
            </>
          ))
        ) : (
          <Typography>No pending payments.</Typography>
        )}
      </List>
    </React.Fragment>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    tenantdues: () => dispatch(tenantActions.getTenantdues()),
  };
};

const mapStateToProps = (state) => {
  return {
    tenantDues: state.tenants.tenantDues,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(PendingPayment);

export { x as PendingPayment };
