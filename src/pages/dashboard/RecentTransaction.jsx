import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  List,
  ListItem,
  Typography,
  Grid,
  Divider,
  Box,
} from "@material-ui/core";
import { bookActions } from "../../_actions";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Title from "../widgets/Title";
import moment from "moment";
import Link from "@material-ui/core/Link";
import { history } from "../../_helpers";

const useStyles = makeStyles((theme) => ({
  infoItem: {
    display: "flex",
    justifyContent: "flex-end",
  },
  heading: {
    color: "#808e95",
  },
  styleTenantName: {
    fontWeight: "bold",
  },
}));

function RecentTransaction(props) {
  const loadPayments = props.loadPayments;
  const classes = useStyles();
  const [pageNo, setPageNo] = useState(1);
  const clearPaymentList = props.clearPaymentList;

  useEffect(() => {
    clearPaymentList();
    loadPayments();
  }, [loadPayments]);

  return (
    <div>
      <React.Fragment>
        <Title>Recent Transactions</Title>
        <List>
          {props.paymentList.length > 0 && props.paymentList ? (
            props.paymentList
              .filter((value, index) => index < 5)
              .sort((transaction1, transaction2) => {
                let moment1 = moment.utc(transaction1.transactionTime);
                let moment2 = moment.utc(transaction2.transactionTime);
                return moment1.diff(moment2) * -1;
              })
              .map((row) => (
                <>
                  <ListItem disableGutters={true}>
                    <Grid container>
                      <Grid item xs={8}>
                        <Typography
                          variant="body1"
                          className={classes.styleTenantName}
                        >
                          {row.tenantName
                            .split(" ")
                            .map(
                              (w) => w.slice(0, 1).toUpperCase() + w.slice(1)
                            )
                            .join(" ")}
                        </Typography>
                      </Grid>
                      <Grid item xs={4} className={classes.infoItem}>
                        <Typography variant="subtitle2">
                          {moment.utc(row.time).format("DD MMM YYYY")}
                        </Typography>
                      </Grid>
                      <Grid item xs={6}>
                        <Typography variant="subtitle2">
                          Discount(Rs)
                        </Typography>
                      </Grid>

                      <Grid item xs={6} className={classes.infoItem}>
                        <Typography variant="subtitle2">
                          {row.discount}
                        </Typography>
                      </Grid>

                      <Grid item xs={6}>
                        <Typography variant="subtitle2">
                          Paid Amount(Rs)
                        </Typography>
                      </Grid>

                      <Grid item xs={6} className={classes.infoItem}>
                        <Typography variant="subtitle2">
                          {row.paidAmount}
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <Typography variant="caption">{row.comment}</Typography>
                      </Grid>
                    </Grid>
                  </ListItem>
                  <Divider />
                </>
              ))
          ) : (
            <Typography>No transaction has added yet.</Typography>
          )}
        </List>
        {props.paymentList.length === 5 ? (
          <Link
            onClick={() => {
              history.push("/payment");
            }}
            className={classes.infoItem}
          >
            See All
          </Link>
        ) : (
          ""
        )}
      </React.Fragment>
    </div>
  );
}
const mapDispatchprops = (dispatch) => {
  return {
    loadPayments: (pageNo) => dispatch(bookActions.getPayments(pageNo)),
    clearPaymentList: () => dispatch(bookActions.clearPaymentList()),
  };
};

const mapStateToProps = (state) => {
  return {
    paymentList: state.book.paymentList,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(RecentTransaction);

export { x as RecentTransaction };
