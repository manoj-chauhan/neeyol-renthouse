import React, { useEffect, useState } from "react";
import {
  Step,
  StepLabel,
  Stepper,
  Typography,
  Button,
  Paper,
  TextField,
  Container,
  Grid,
  List,
  ListItem,
  Dialog,
  DialogContent,
  DialogActions,
} from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { buildingActions } from "../../_actions";
import { history } from "../../_helpers";
import { validate } from "@material-ui/pickers";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100vh",
  },
  alignButtons: {
    flex: 1,
    justifyContent: "flex-end",
    marginBottom: 36,
  },
  arrangeButtons: {
    margin: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  alignBuildingForm: {
    marginTop: 40,
    height: "390px",
    display: "flex",
    flexDirection: "column",
    // alignItems: "",
  },
  marginTopOfImageField: {
    marginTop: 10,
  },
  addMarginForListItems: {
    marginLeft: 20,
    height: "430px",
  },
  addScrollOfListItems: {
    maxHeight: "400px",
    overflowY: "scroll",
  },
  alignListItems: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-Start",
  },
  alignAddRoomButton: {
    marginRight: 15,
    marginBottom: 10,
  },
  // final Steps of wizard
  rootOfFinalStep: {
    display: "flex",
    flexDirection: "column",
    height: "430px",
  },
  alignBuildingName: {
    display: "flex",
    justifyContent: "center",
  },
  alignRoomListItems: {
    display: "flex",
    justifyContent: "space-around",
  },
  dialogPaperForMeterListDialog: {
    minWidth: "35vh",
    minHeight: "45vh",
  },
  scrollBarForMeterItemsInMeterListDialog: {
    maxHeight: "200px",
    overflowY: "scroll",
  },
}));

function NewBuilding(props) {
  const classes = useStyles();

  const [buildingName, setBuildingName] = useState("");
  const [buildingAddress, setBuildingAddress] = useState("");

  const buildingPreview = props.buildingDetailsForPreview;

  useEffect(() => {
    if (buildingPreview) {
      setBuildingName(props.buildingDetailsForPreview.buildingName);
      setBuildingAddress(props.buildingDetailsForPreview.buildingAddress);
    }
  }, [props.buildingDetailsForPreview]);

  useEffect(() => {
    props.addBuildingDetails(buildingName, buildingAddress);
  }, [buildingName, buildingAddress]);

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (name === "buildingName") {
      setBuildingName(value);
    }

    if (name === "buildingAddress") {
      setBuildingAddress(value);
    }
  };

  return (
    <Container>
      <div>
        <form className={classes.alignBuildingForm} noValidate>
          <Typography component="h5" variant="h5">
            New Building
          </Typography>

          <TextField
            id="name"
            name="buildingName"
            label="Building Name"
            type="name"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            defaultValue={
              props.buildingDetailsForPreview
                ? props.buildingDetailsForPreview.buildingName
                : ""
            }
            onChange={handleChange}
            autoFocus
          />

          <TextField
            id="address"
            name="buildingAddress"
            label="Building Address"
            type="address"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            defaultValue={
              props.buildingDetailsForPreview
                ? props.buildingDetailsForPreview.buildingAddress
                : ""
            }
            onChange={handleChange}
            autoFocus
          />
        </form>
      </div>
    </Container>
  );
}

function AddRooms(props) {
  const classes = useStyles();

  const [addRoomDialogOpen, setAddRoomDialogOpen] = useState(false);
  const [roomName, setRoomName] = useState("");
  const [roomNameError, setRoomNameError] = useState(false);
  const [roomImageError, setRoomImageError] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (name === "roomName") {
      setRoomName(value);
      setRoomNameError(false);
    }
  };

  const validate = () => {
    if (roomName) {
      return true;
    } else {
      setRoomNameError(roomName.length == 0);
      return false;
    }
  };

  const handleSubmit = () => {
    if (validate()) {
      props.roomDetails(roomName);
      setAddRoomDialogOpen(false);
      setRoomName("");
    }
  };

  return (
    <div className={classes.addMarginForListItems}>
      <Typography component="h5" variant="h5">
        Rooms
      </Typography>
      <div className={classes.addScrollOfListItems}>
        <List>
          {props.getRoomList.map((roomDetails) => (
            <ListItem className={classes.alignListItems} disableGutters="true">
              <Grid container>
                <Grid item xs={9}>
                  <Typography> {roomDetails.roomName}</Typography>
                </Grid>
              </Grid>
            </ListItem>
          ))}

          <Button
            color="primary"
            onClick={() => {
              setAddRoomDialogOpen(true);
            }}
          >
            Add Room
          </Button>
        </List>
      </div>

      <Dialog
        open={addRoomDialogOpen}
        onClose={() => {
          setAddRoomDialogOpen(false);
          setRoomName("");
        }}
      >
        <DialogContent>
          <Typography component="h5" variant="h5">
            New Room
          </Typography>

          <TextField
            id="name"
            name="roomName"
            label="Room Name"
            type="name"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            onChange={handleChange}
            error={roomNameError}
            autoFocus
          />
        </DialogContent>
        <DialogActions className={classes.alignAddRoomButton}>
          <Button variant="contained" color="primary" onClick={handleSubmit}>
            Add Room
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function FinalStepsOfWizard(props) {
  const classes = useStyles();

  const [createMeterDialogOpen, setCreateMeterDialogOpen] = useState(false);

  const [meterName, setMeterName] = useState("");
  const [meterNameError, setMeterNameError] = useState(false);
  const [initialReading, setInitialReading] = useState(0);
  const [initialReadingError, setInitialReadingError] = useState(false);
  const [selectedRoomName, setSelectedRoomName] = useState("");
  const { translation } = props;

  const handleChange = (e) => {
    const { name, value } = e.target;

    if (name === "meterName") {
      if (value.length === 0) {
        setMeterNameError(true);
      } else {
        setMeterName(value);
        setMeterNameError(false);
      }
    }
    if (name === "initialReading") {
      if (value.length === 0) {
        setInitialReadingError(true);
      } else {
        setInitialReading(value);
        setInitialReadingError(false);
      }
    }
  };

  const validate = () => {
    if (meterName.length > 0 && initialReading > 0) {
      return true;
    } else {
      setMeterNameError(meterName.length === 0);
      setInitialReadingError(initialReading === 0);
      return false;
    }
  };

  const handleSubmit = () => {
    /*
     this is used for checking meterName and initial reading in add meter in room.
    */
    if (validate()) {
      /*
        if previewRoomMeter and roomListWithMeter states length is not 0.
        
        NOTE :-
        previewRoomMeter is used for return room list with meter.
        roomListWithMeter is used for enter room details with meter for storing value in state.
      */
      if (
        props.roomListWithMeter.length > 0 &&
        props.previewRoomMeter.filter((room) => room.meterName != null).length >
          0
      ) {
        /*
          if any room details have meterName is same in previousRoomMeter(state).
           when add same meter in new room. same meter is not added in previousRoomMeter.  
        */
        if (
          props.previewRoomMeter.filter(
            (room) =>
              room.meterName ===
              meterName
                .split(" ")
                .map((w) => w.toLowerCase())
                .join("")
          ).length > 0
        ) {
          props.roomListWithMeter([
            ...props.previewRoomMeter.filter(
              (room) => room.roomName != selectedRoomName
            ),
          ]);
          setCreateMeterDialogOpen(false);
        } else {
          /*
          add Room details with meter in roomListWithMeter (state)
          when meter name is not found in left rooms (Bache hue rooms me)
          in roomListWithMeter.                         
        */
          props.roomListWithMeter([
            ...props.previewRoomMeter.filter(
              (room) => room.roomName != selectedRoomName
            ),
            {
              roomName: selectedRoomName,
              meterName: meterName,
              initialReading: initialReading,
            },
          ]);
          setCreateMeterDialogOpen(false);
        }
      } else {
        /*
       this else condition only one time run.
       if roomListWithMeter and previewWithMeter (states) is empty.
      */
        props.roomListWithMeter([
          ...props.roomList.filter((room) => room.roomName != selectedRoomName),
          {
            roomName: selectedRoomName,
            meterName: meterName,
            initialReading: initialReading,
          },
        ]);
        setCreateMeterDialogOpen(false);
      }
    }
  };

  return (
    <div className={classes.rootOfFinalStep}>
      <Typography
        component="h5"
        variant="h5"
        className={classes.alignBuildingName}
      >
        {props.building.buildingName}
      </Typography>
      <List className={classes.addScrollOfListItems}>
        {props.roomList.map((roomDetails) => (
          <ListItem>
            <Grid
              container
              className={classes.alignRoomListItems}
              alignItems="center"
            >
              <Grid item xs={5}>
                <Typography>{roomDetails.roomName}</Typography>
              </Grid>
              <Grid item xs={4}>
                {props.previewRoomMeter.filter(
                  (room) =>
                    room.roomName == roomDetails.roomName && room.meterName
                ).length > 0 ? (
                  props.previewRoomMeter
                    .filter(
                      (room) =>
                        room.roomName == roomDetails.roomName && room.meterName
                    )
                    .map((room) => <Typography>{room.meterName}</Typography>)
                ) : (
                  <Button
                    color="primary"
                    onClick={() => {
                      setCreateMeterDialogOpen(true);
                      setSelectedRoomName(roomDetails.roomName);
                    }}
                  >
                    Add Meter
                  </Button>
                )}
              </Grid>
            </Grid>
          </ListItem>
        ))}
      </List>

      <Dialog
        open={createMeterDialogOpen}
        onClose={() => {
          setCreateMeterDialogOpen(false);
          setMeterName("");
          setMeterNameError(false);
          setInitialReading(0);
          setInitialReadingError(false);
        }}
      >
        <DialogContent>
          <Typography component="h5" variant="h5">
            New Meter
          </Typography>

          <TextField
            error={meterNameError}
            id="name"
            name="meterName"
            label="meter Name"
            type="name"
            variant="outlined"
            margin="normal"
            onChange={handleChange}
            required
            fullWidth
            autoFocus
            helperText={meterNameError ? translation("common.nameError") : ""}
          />

          <TextField
            error={initialReadingError}
            id="reading"
            name="initialReading"
            label="Initial Reading"
            type="number"
            variant="outlined"
            margin="normal"
            onChange={handleChange}
            required
            fullWidth
            autoFocus
            helperText={
              initialReadingError
                ? translation("common.initialReadingError")
                : ""
            }
          />
        </DialogContent>
        <DialogActions className={classes.alignAddRoomButton}>
          <Button variant="contained" color="primary" onClick={handleSubmit}>
            Add Meter
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function getSteps() {
  return ["Create Building", "Create Rooms", "Add Meters"];
}

function BuildingWizard(props) {
  const classes = useStyles();
  const steps = getSteps();

  const [activeStep, setActiveStep] = React.useState(0);
  const [buildingDetails, setBuildingDetails] = useState(null);
  const [roomList, setRoomList] = useState([]);
  const [roomListWithMeter, setRoomListWithMeter] = useState([]);
  const [nextButtonDisabled, setNextButtonDisabled] = useState(false);
  const [finishButtonDisabled, setFinishButtonDisable] = useState(false);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    if (activeStep == 0) {
      setNextButtonDisabled(true);
    }
    if (activeStep == 1 && roomList.length == 0) {
      setNextButtonDisabled(true);
    }
    if (activeStep == 2 && roomList.length != roomListWithMeter.length) {
      setFinishButtonDisable(true);
    }
  }, [activeStep]);

  useEffect(() => {
    if (roomList.length > 0) {
      setNextButtonDisabled(false);
    }
  }, [roomList]);

  const building = (buildingName, buildingAddress) => {
    setNextButtonDisabled(true);
    if (buildingName && buildingAddress) {
      setBuildingDetails({
        buildingName: buildingName,
        buildingAddress: buildingAddress,
      });
      setNextButtonDisabled(false);
    }
  };

  const addRoomDetailsInList = (roomName) => {
    if (roomName) {
      setRoomList([
        ...roomList.filter((room) => room.roomName != roomName),
        {
          roomName: roomName,
        },
      ]);
      setNextButtonDisabled(false);
    }
  };

  const addMeterInRoomList = (roomListWithMeter) => {
    if (roomListWithMeter.length > 0) {
      setRoomListWithMeter(roomListWithMeter);
      if (roomList.length === roomListWithMeter.length) {
        setFinishButtonDisable(false);
      }
    }
  };

  const handleNext = () => {
    setActiveStep(activeStep + 1);
  };

  const handleSubmit = () => {
    props.addBuildingWizard({
      buildingName: buildingDetails.buildingName,
      buildingAddress: buildingDetails.buildingAddress,
      roomList: roomListWithMeter,
    });
    setFinishButtonDisable(true);
  };

  return (
    <div className={classes.root}>
      <Stepper alternativeLabel activeStep={activeStep}>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      {activeStep === 0 ? (
        <NewBuilding
          addBuildingDetails={building}
          buildingDetailsForPreview={buildingDetails}
          uploadBuildingPhotoWithoutBuildingId={
            props.uploadBuildingPhotoWithoutBuildingId
          }
          building={props.building}
        />
      ) : activeStep === 1 ? (
        <AddRooms roomDetails={addRoomDetailsInList} getRoomList={roomList} />
      ) : (
        <FinalStepsOfWizard
          building={buildingDetails}
          roomList={roomList}
          roomListWithMeter={addMeterInRoomList}
          previewRoomMeter={roomListWithMeter}
          translation={t}
        />
      )}
      <div className={classes.alignButtons}>
        <div className={classes.arrangeButtons}>
          {activeStep > 0 ? (
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setActiveStep(activeStep - 1);
              }}
            >
              Back
            </Button>
          ) : (
            ""
          )}

          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              history.push("/");
            }}
            className={classes.backButton}
          >
            Cancel
          </Button>

          {activeStep == 2 ? (
            <Button
              disabled={finishButtonDisabled}
              variant="contained"
              color="primary"
              onClick={handleSubmit}
            >
              Finish
            </Button>
          ) : (
            <Button
              disabled={nextButtonDisabled}
              variant="contained"
              color="primary"
              onClick={handleNext}
            >
              Next
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    uploadBuildingPhotoWithoutBuildingId: (buildingPhotoId) =>
      dispatch(
        buildingActions.uploadBuildingPhotoWithoutBuildingId(buildingPhotoId)
      ),
    addBuildingWizard: (buildingWizardDetails) =>
      dispatch(buildingActions.addBuildingWizard(buildingWizardDetails)),
  };
};

const mapStateToProps = (state) => {
  return {
    building: state.buildings,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(BuildingWizard)
);

export { x as BuildingWizard };
