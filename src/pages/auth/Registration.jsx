import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { userActions } from "../../_actions";
import { connect } from "react-redux";
import moment from "moment";
import { Snackbar } from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";
import { useTranslation } from "react-i18next";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="http://neeyol.com/">
        Neeyol Inc.
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  profileImage: {
    margin: theme.spacing(1),
    width: theme.spacing(7),
    height: theme.spacing(7),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  formControl: {
    margin: theme.spacing(3),
  },
}));

function Registration(props) {
  const classes = useStyles();

  const [name, setName] = React.useState("");
  const [nameError, setNameError] = React.useState(false);
  const [profileImage, setProfileImage] = React.useState("");
  const [gender, setGender] = React.useState("");
  const [genderError, setGenderError] = React.useState(false);
  const [dateOfBirth, setDateOfBirth] = React.useState(null);
  const [dateOfBirthError, setDateOfBirthError] = React.useState(false);
  const [username, setUsername] = React.useState("");
  const [userNameError, setUserNameError] = React.useState(false);
  const [password, setPassword] = React.useState("");
  const [passwordError, setPasswordError] = React.useState(false);
  const [confirmPassword, setConfirmPassword] = React.useState("");
  const [confirmPasswordError, setConfirmPasswordError] = React.useState(false);
  const [disabled, setDisabled] = React.useState(false);
  const [snackBarOpen, setSnackBarOpen] = React.useState(false);
  const { t, i18n } = useTranslation();
  const specialCharacters = /^[0-9a-zA-Z]+$/g;

  const handleChange = (event) => {
    if (event.target.id == "name") {
      if (event.target.value) {
        setName(event.target.value);
        setNameError(false);
      } else {
        setNameError(true);
      }
    } else if (event.target.id === "username") {
      if (event.target.value.length) {
        const userName = event.target.value
          .split(" ")
          .map((w) => w.toLowerCase())
          .join("");
        if (userName.match(specialCharacters)) {
          setUsername(userName);
          setUserNameError(false);
        } else {
          setUserNameError(true);
        }
      }
    } else if (event.target.id === "password") {
      if (event.target.value.length) {
        if (event.target.value.length >= 8) {
          if (event.target.value.split(" ").length === 1) {
            setPassword(event.target.value);
            setPasswordError(false);
          } else {
            setPasswordError(true);
          }
        } else {
          setPasswordError(true);
        }
      }
    } else if (event.target.id === "confirmPassword") {
      if (event.target.value.length) {
        if (event.target.value.length >= 8) {
          if (event.target.value.split(" ").length === 1) {
            setConfirmPassword(event.target.value);
            setConfirmPasswordError(false);
          } else {
            setConfirmPasswordError(true);
          }
        } else {
          setConfirmPasswordError(true);
        }
      }
    } else if (event.target.id === "profileImage") {
      setProfileImage(
        event.target.files.length > 0 ? event.target.files[0] : null
      );
    } else if (event.target.name === "dateOfBirth") {
      if (
        moment(event.target.value).isAfter(
          moment(moment().format("YYYY-MM-DD"))
        )
      ) {
        setDateOfBirthError(true);
      } else {
        setDateOfBirth(event.target.value);
        setDateOfBirthError(false);
      }
    } else if (event.target.name === "gender") {
      setGender(event.target.value);
      setGenderError(false);
    } else {
      setGenderError(true);
    }
  };

  const validate = () => {
    if (name && username && password && confirmPassword && gender) {
      return true;
    } else {
      setNameError(name.length === 0);
      setUserNameError(username.length === 0);
      setPasswordError(password.length === 0);
      setConfirmPasswordError(confirmPassword.length === 0);
      setGenderError(gender.length === 0);
      return false;
    }
  };

  const onFormSubmit = (event) => {
    if (
      validate() &&
      password.length >= 8 &&
      confirmPassword.length >= 8 &&
      password === confirmPassword
    ) {
      props.register({
        name: name,
        age: moment(moment().format("YYYY-MM-DD")).diff(
          moment(dateOfBirth),
          "years"
        ),
        gender: gender,
        dateOfBirth: moment.utc(dateOfBirth).toISOString(),
        username: username,
        password: password,
        profileImage: profileImage,
      });
      setDisabled(true);
      setSnackBarOpen(true);
    }
  };

  const handleClose = () => {
    setSnackBarOpen(false);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          RENTHOUSE
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            error={nameError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Name"
            name="name"
            autoFocus
            onChange={handleChange}
            helperText={nameError ? t('common.nameError') : ""}
          />

          <TextField
            error={userNameError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoFocus
            onChange={handleChange}
            helperText={
              userNameError
                ? t('userDetails.registration.userNameError')
                : ""
            }
          />

          <TextField
            error={passwordError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="password"
            label="Password"
            type="password"
            name="password"
            autoFocus
            onChange={handleChange}
            helperText={
              passwordError
                ? t('userDetails.registration.passwordError')
                : ""
            }
          />

          <TextField
            error={confirmPasswordError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="confirmPassword"
            label="Confirm Password"
            type="password"
            name="password"
            autoFocus
            onChange={handleChange}
            helperText={
              confirmPasswordError
                ? t('userDetails.registration.confirmPasswordError')
                : ""
            }
          />

          <FormControl
            component="fieldset"
            margin="normal"
            required
            error={genderError}
          >
            <FormLabel component="legend">Gender</FormLabel>
            <RadioGroup
              id="gender"
              aria-label="gender"
              name="gender"
              onChange={handleChange}
              row
            >
              <FormControlLabel
                value="female"
                control={<Radio />}
                label="Female"
              />
              <FormControlLabel value="male" control={<Radio />} label="Male" />
            </RadioGroup>
          </FormControl>
          {
            genderError ? (
              <Typography color="secondary">
                gender cannot be empty
              </Typography>
            ) : ""
          }
          <TextField
            error={dateOfBirthError}
            label="Date Of Birth"
            type="date"
            name="dateOfBirth"
            variant="outlined"
            margin="normal"
            fullWidth
            onChange={handleChange}
            InputLabelProps={{
              shrink: true,
            }}
            helperText={
              dateOfBirthError
                ? t('common.dateOfBirthError')
                : null
            }
          />
          <FormLabel component="legend">Profile Image</FormLabel>
          <TextField
            id="profileImage"
            type="file"
            name="image"
            fullWidth
            variant="outlined"
            onChange={handleChange}
          />
          <Button
            disabled={disabled}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={onFormSubmit}
          >
            Register
          </Button>
        </form>
        <Snackbar
          open={snackBarOpen}
          autoHideDuration={2000}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
        >
          <Alert onClose={handleClose} severity="success">
            Please Wait Until Registration!
          </Alert>
        </Snackbar>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    register: (user) => dispatch(userActions.register(user)),
  };
};

const mapStateToProps = (state) => {
  const { registrationInProgress } = state.registration;
  return { registrationInProgress };
};

const x =
  connect(mapStateToProps, mapDispatchprops)(Registration);

export { x as Registration };
