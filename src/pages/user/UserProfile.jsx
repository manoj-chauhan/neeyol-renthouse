import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography, Paper, Button, CircularProgress } from "@material-ui/core";
import { userActions } from "../../_actions";
import { connect } from "react-redux";
import CardMedia from "@material-ui/core/CardMedia";
import config from "../../config";
import EditTwoToneIcon from "@material-ui/icons/EditTwoTone";
import { makeStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import {
  FormControl,
  FormLabel,
  OutlinedInput,
  Avatar,
} from "@material-ui/core";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import moment from "moment";
import Divider from "@material-ui/core/Divider";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { useTranslation } from "react-i18next";
import transitions from "@material-ui/core/styles/transitions";

const useStyles = (theme) => ({
  root: {
    padding: theme.spacing(2),
  },
  personalInfo: {
    padding: theme.spacing(2),
  },
  row: {
    padding: theme.spacing(1),
  },
  signoutButton: {
    // width: "100%"
  },
  cover: {
    width: 90,
    height: 100,
  },
  container: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  editButton: {
    margin: 5,
    marginLeft: 12,
  },
  editIcon: {
    display: "flex",
    justifyContent: "flex-end",
    margin: 5,
    marginTop: 1,
  },
  changePasswordPaper: {
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(2),
  },
  logoutPaper: {
    marginTop: theme.spacing(1),
    paddingTop: theme.spacing(1),
  },
  changePasswordButton: {
    paddingRight: 100,
  },
  noProfileImage: {
    height: 100,
    width: 100,
    backgroundColor: "#111",
    borderRadius: 20,
  },
});

const useStyleForChangePasswordDialog = makeStyles((theme) => ({
  title: {
    color: "blue",
  },
}));

function ChangePasswordDialog(props) {
  const { onClose, open, translation } = props;
  const [currentPassword, setCurrentPassword] = useState("");
  const [currentPasswordError, setCurrentPasswordError] = useState(false);
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [newPasswordError, setNewPasswordError] = useState(false);
  const [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const classes = useStyleForChangePasswordDialog();

  const handleMenuItemClick = (e) => {
    const { name, value } = e.target;
    if (name === "currentPassword") {
      setCurrentPassword(value);
      if (value.length === 0) {
        setCurrentPasswordError(true);
      } else {
        setCurrentPasswordError(false);
      }
    }
    if (name === "newPassword") {
      setNewPassword(value);
      if (value.length === 0) {
        setNewPasswordError(true);
      } else {
        setNewPasswordError(false);
      }
    }
    if (name === "confirmPassword") {
      setConfirmPassword(value);
      if (value.length === 0) {
        setConfirmPasswordError(true);
      } else {
        setConfirmPasswordError(false);
      }
    }
  };

  const handleSubmit = (e) => {
    if (
      currentPassword &&
      newPassword.length >= 8 &&
      confirmPassword.length >= 8 &&
      newPassword === confirmPassword
    ) {
      props.changePassword({
        currentPassword: currentPassword,
        changePassword: confirmPassword,
      });
    } else {
      setCurrentPasswordError(currentPassword.length === 0);
      setNewPasswordError(newPassword.length < 8);
      setConfirmPasswordError(confirmPassword.length < 8 || newPassword !== confirmPassword);
    }

    if (
      currentPassword.length !== 0 &&
      newPassword.length >= 8 &&
      confirmPassword.length >= 8 &&
      newPassword === confirmPassword
    ) {
      props.onClose();
      setCurrentPassword("");
      setCurrentPasswordError(false);
      setNewPassword("");
      setNewPasswordError(false);
      setConfirmPassword("");
      setConfirmPasswordError(false);
    }

  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => {
          onClose();
          setCurrentPassword("");
          setNewPassword("");
          setConfirmPassword("");
          setCurrentPasswordError(false);
          setNewPasswordError(false);
          setConfirmPasswordError(false);
        }}
      >
        <DialogTitle className={classes.title}>Change Password</DialogTitle>
        <DialogContent>
          <FormControl>
            <TextField
              error={currentPasswordError}
              variant="outlined"
              margin="normal"
              name="currentPassword"
              label="Current Password"
              type="password"
              onChange={handleMenuItemClick}
              helperText={
                currentPasswordError ? translation('userDetails.changePasswordError.currentPasswordError') : " "
              }
            />
            <TextField
              error={newPasswordError}
              variant="outlined"
              margin="normal"
              name="newPassword"
              label="New Password"
              type="password"
              onChange={handleMenuItemClick}
              helperText={
                newPasswordError
                  ? translation('userDetails.changePasswordError.newPasswordError')
                  : " "
              }
            />
            <TextField
              error={confirmPasswordError}
              variant="outlined"
              margin="normal"
              name="confirmPassword"
              label="Confirm Password"
              type="password"
              onChange={handleMenuItemClick}
              helperText={
                confirmPasswordError
                  ? translation('userDetails.changePasswordError.confirmPasswordError')
                  : " "
              }
            />
            <DialogActions>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSubmit}
              >
                Reset Password
              </Button>
            </DialogActions>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const useStylesImageDialog = makeStyles((theme) => ({
  cover: {
    height: 100,
    width: 100,
  },
  dialogPaper: {
    width: "100%",
    // minHeight: "90vh",
    // // maxHeight: '80vh',
    // minWidth: "50vh",
  },
  image: {
    display: "flex",
    justifyContent: "center",
    margin: 4,
    marginTop: 2,
  },
  dialogTitle: {
    color: "blue",
    display: "flex",
    justifyContent: "center",
    padding: 6,
  },
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  saveButton: {
    width: "70%",
    marginTop: 20,
  },
  noProfileImage: {
    display: "flex",
    justifyContent: "center",
    margin: 4,
    marginTop: 2,
    height: 100,
    width: 100,
    backgroundColor: "#111",
    borderRadius: 20,
  },
  imageLoaderStyle: {
    backgroundColor: "white",
    opacity: "0.4",
    width: 100,
    height: 100,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

function EditUserProfileDialog(props) {
  const { onClose, open, translation } = props;
  const profile = props.userProfile;

  const [image, setImage] = useState(null);
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState(false);
  const [dateOfBirth, setDateOfBirth] = useState(0);
  const [dateOfBirthError, setDateOfBirthError] = useState(false);
  const [gender, setGender] = useState("");
  const [genderError, setGenderError] = useState(false);
  const [buttonDisabled, setButtonDisabled] = useState(false);

  const classes = useStylesImageDialog();

  let date = null;
  let photoId = null;

  photoId = profile.photoId;

  useEffect(() => {
    console.log("Effect is call");
    setName(profile.name ? profile.name : "");
    setDateOfBirth(profile.dateOfBirth);
    setImage(profile.photoId);
    setGender(profile.gender);
    if (photoId === profile.photoId) {
      setButtonDisabled(false);
    } else {
      setButtonDisabled(true);
    }
    date = profile.dateOfBirth
      ? moment.utc(profile.dateOfBirth).toISOString().substring(0, 10)
      : "";
  }, [profile]);

  const validate = () => {
    if (
      name.length !== 0 &&
      !moment(dateOfBirth).isAfter(moment(moment().format("YYYY-MM-DD"))) &&
      gender.length !== 0
    ) {
      return true;
    } else {
      setNameError(name.length === 0);
      setDateOfBirthError(
        moment(dateOfBirth).isAfter(moment(moment().format("YYYY-MM-DD")))
      );
      setGenderError(gender.length === 0);
      return false;
    }
  }

  const handleSubmit = () => {
    if (validate()) {
      let x = moment.utc(dateOfBirth);
      props.changeUserDetails({
        name: name,
        gender: gender,
        dateOfBirth: x.toISOString(),
        photoId: profile.photoId,
        userName: profile.userName,
      });
      props.onClose();
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "image") {
      setButtonDisabled(true);
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
      props.uploadUserPhoto({
        image: e.target.files[0],
      });
    }
    if (name === "name") {
      setName(value);
      if (value.length === 0) {
        setNameError(true);
      } else {
        setNameError(false);
      }
    }
    if (name === "dateOfBirth") {
      setDateOfBirth(value);
    }
    if (name === "gender") {
      setGender(value);
      setGenderError(false);
    }
  };

  return (
    <div>
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={open}
        onClose={() => {
          onClose();
          setName("");
          setDateOfBirth("");
          setGender("");
          setNameError(false);
          setDateOfBirthError(false);
          setGenderError(false);
        }}
        classes={{ paper: classes.dialogPaper }}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Edit User Profile
        </DialogTitle>
        <div className={classes.root}>
          {profile.photoId ? (
            <div className={classes.image}>
              {props.imageLoader ? (
                <>
                  <CardMedia
                    xs={0}
                    md={3}
                    className={classes.cover}
                    style={{
                      position: "fixed",
                      borderRadius: 20,
                    }}
                    image={
                      config.api.BASE_URL +
                      "/image/" +
                      profile.photoId +
                      "/thumb"
                    }
                    title="Live from space album cover"
                  />
                  <div className={classes.imageLoaderStyle}>
                    <CircularProgress />
                  </div>
                </>
              ) : (
                <CardMedia
                  xs={0}
                  md={3}
                  className={classes.cover}
                  image={config.api.BASE_URL + "/image/" + profile.photoId}
                  title="Live from space album cover"
                />
              )}
            </div>
          ) : (
            <div className={classes.image}>
              {props.imageLoader ? (
                <>
                  <Avatar
                    src="/broken-image.jpg"
                    className={classes.noProfileImage}
                    style={{ position: "fixed" }}
                  />
                  <div className={classes.imageLoaderStyle}>
                    <CircularProgress />
                  </div>
                </>
              ) : (
                <Avatar
                  src="/broken-image.jpg"
                  className={classes.noProfileImage}
                />
              )}
            </div>
          )}

          <div className={classes.image}>
            <Button
              className={classes.editButton}
              variant="contained"
              color="primary"
              component="label"
            >
              Edit
              <input name="image" type="file" onChange={handleChange} hidden />
            </Button>
          </div>

          <FormControl required>
            <TextField
              error={nameError}
              type="Text"
              name="name"
              variant="outlined"
              margin="normal"
              defaultValue={profile.name ? profile.name : ""}
              onChange={handleChange}
              helperText={nameError ? translation('common.nameError') : ""}
            />

            <TextField
              error={dateOfBirthError}
              label="Date Of Birth"
              type="date"
              name="dateOfBirth"
              variant="outlined"
              margin="normal"
              defaultValue={
                profile.dateOfBirth
                  ? moment.utc(profile.dateOfBirth).toISOString().substring(0, 10)
                  : ""
              }
              onChange={handleChange}
              helperText={
                dateOfBirthError
                  ? translation('common.dateOfBirthError')
                  : ""
              }
              InputLabelProps={{
                shrink: true,
              }}
            />
          </FormControl>

          <FormControl error={genderError}>
            <RadioGroup
              aria-label="gender"
              name="gender"
              defaultValue={profile.gender ? profile.gender : ""}
              onChange={handleChange}
            >
              <FormControlLabel
                value="female"
                control={<Radio color="primary" />}
                label="Female"
              />
              <FormControlLabel
                value="male"
                control={<Radio color="primary" />}
                label="Male"
              />
            </RadioGroup>
            {genderError ? (
              <Typography color="secondary">
                {translation('common.genderError')}
              </Typography>
            ) : (
              ""
            )}
          </FormControl>
          <div className={classes.saveButton}>
            <Button
              onClick={handleSubmit}
              fullWidth
              variant="contained"
              color="secondary"
              disabled={buttonDisabled}
              className={classes.submit}
            >
              Save
            </Button>
          </div>
        </div>
      </Dialog>
    </div>
  );
}

function UserProfile(props) {
  const { classes } = props;
  const loadUserProfile = props.loadUserProfile;
  const profile = props.userProfile;
  const [image, setImage] = useState(null);
  const [editUserDialog, setEditUserDialog] = useState(false);
  const [changePasswordDialog, setChangePasswordDialog] = useState(false);
  const { t, i18n } = useTranslation();

  useEffect(() => {
    loadUserProfile();
  }, [loadUserProfile]);

  return (
    <div className={classes.root}>
      <Paper elevation={0} className={classes.personalInfo}>
        <div className={classes.editIcon}>
          <EditTwoToneIcon
            onClick={(e) => {
              console.log("button has clicked");
              setEditUserDialog(true);
            }}
          />
        </div>
        <div className={classes.container}>
          <div>
            <div className={classes.row}>
              <Typography variant="h5">{profile.name}</Typography>
            </div>
            <div className={classes.row}>{profile.userName}</div>
            <div className={classes.row}>
              {profile.gender ? profile.gender.toUpperCase() : ""}
            </div>
            <div className={classes.row}>
              {moment.utc(profile.dateOfBirth).format("DD MMM YYYY")}
            </div>
          </div>
          <div>
            {profile.photoId ? (
              <CardMedia
                xs={0}
                md={3}
                className={classes.cover}
                image={
                  config.api.BASE_URL + "/image/" + profile.photoId + "/thumb"
                }
                title="Live from space album cover"
              />
            ) : (
              <div className={classes.noProfileImage}>
                <Avatar
                  src="/broken-image.jpg"
                  className={classes.noProfileImage}
                />
              </div>
            )}
          </div>
        </div>
      </Paper>
      <Divider />
      <Paper elevation={0} className={classes.changePasswordPaper}>
        <Button
          onClick={() => {
            setChangePasswordDialog(true);
          }}
          className={classes.changePasswordButton}
        >
          Change Password
        </Button>
      </Paper>
      <Divider />
      <Paper elevation={0} className={classes.logoutPaper}>
        <Button
          className={classes.signoutButton}
          onClick={(e) => {
            props.logout();
          }}
        >
          Logout
        </Button>
      </Paper>

      <EditUserProfileDialog
        open={editUserDialog}
        onClose={() => {
          setEditUserDialog(false);
        }}
        userProfile={props.userProfile}
        imageLoader={props.imageLoader}
        uploadUserPhoto={props.uploadUserPhoto}
        changeUserDetails={props.changeUserDetails}
        translation={t}
      />

      <ChangePasswordDialog
        open={changePasswordDialog}
        onClose={() => {
          setChangePasswordDialog(false);
        }}
        changePassword={props.changePassword}
        translation={t}
      />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    userProfile: state.user.userDetail,
    imageLoader: state.user.imageLoader,
  };
};

const mapDispatchprops = (dispatch) => {
  return {
    loadUserProfile: () => dispatch(userActions.userDetail()),
    logout: () => dispatch(userActions.logout()),
    uploadUserPhoto: (userInfo) =>
      dispatch(userActions.uploadUserPhoto(userInfo)),
    changeUserDetails: (userInfo) =>
      dispatch(userActions.changeUserDetails(userInfo)),
    changePassword: (changePasswordInfo) =>
      dispatch(userActions.changePassword(changePasswordInfo)),
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(UserProfile)
);

export { x as UserProfile };
