import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {
  List,
  ListItemText,
  Collapse,
  Typography,
  Divider,
} from "@material-ui/core";
import DashboardIcon from "@material-ui/icons/Dashboard";
import HomeWorkIcon from "@material-ui/icons/HomeWork";
import TimelineIcon from "@material-ui/icons/Timeline";
import FlipCameraAndroidIcon from "@material-ui/icons/FlipCameraAndroid";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import BarChartIcon from "@material-ui/icons/BarChart";
import SettingsIcon from "@material-ui/icons/Settings";
import PeopleIcon from "@material-ui/icons/People";
import { history } from "../../_helpers";
import { MenuItem } from "@material-ui/core";
import { buildingActions } from "../../_actions";
import { connect } from "react-redux";
import AddIcon from "@material-ui/icons/Add";

const useStyles = makeStyles((theme) => ({
  nested: {
    paddingLeft: theme.spacing(6),
  },
  alignAddBuilding: {
    paddingLeft: "30px",
  },
  alignIcon: {
    minWidth: 36,
  },
}));

const ListItems = (props) => {
  const [open, setOpen] = useState(false);
  const [selected, setSelected] = useState("");

  const classes = useStyles();

  const loadBuildings = props.loadBuildings;
  useEffect(() => {
    loadBuildings();
  }, [loadBuildings]);

  const selectedListItem = props.selectedListItem;

  useEffect(() => {
    console.log(
      "selected list item name in drawer in effect",
      selectedListItem
    );
    if (selectedListItem) {
      setSelected(selectedListItem);
    }
  }, [selectedListItem]);

  return (
    <div>
      <MenuItem
        button
        onClick={() => {
          if (history.location.pathname !== "/dashboard") {
            history.push("/dashboard");
            props.name("dashboard");
          }
          if (history.location.pathname === "/dashboard") {
            props.name("dashboard");
          }
        }}
        selected={selected === "dashboard"}
      >
        <ListItemIcon className={classes.alignIcon}>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </MenuItem>
      <Divider />

      <MenuItem
        button
        onClick={() => {
          if (history.location.pathname !== "/tenant") {
            history.push("/tenant");
            props.name("tenant");
          }
          if (history.location.pathname === "/tenant") {
            props.name("tenant");
          }
        }}
        selected={selected === "tenant"}
      >
        <ListItemIcon className={classes.alignIcon}>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Tenants" />
      </MenuItem>
      <Divider />

      <MenuItem button selected={selected === "building"}>
        <ListItemIcon className={classes.alignIcon}>
          <HomeWorkIcon />
        </ListItemIcon>
        <ListItemText primary="Buildings" />
        <AddIcon
          onClick={() => {
            history.push("building/new");
          }}
        />
      </MenuItem>

      <Collapse in={true} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {props.buildingList.length > 0 ? (
            <>
              {props.buildingList.map((building) => (
                <>
                  <ListItem
                    button
                    className={classes.nested}
                    onClick={(e) => {
                      if (
                        history.location.pathname !==
                        "/building/" + building.id
                      ) {
                        history.push("/building/" + building.id);
                      }
                      props.name("/building/" + building.id);
                    }}
                    selected={selected === "/building/" + building.id}
                  >
                    <Typography>
                      {building.name
                        .split(" ")
                        .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                        .join(" ")}
                    </Typography>
                  </ListItem>
                </>
              ))}
            </>
          ) : (
            ""
          )}
        </List>
      </Collapse>

      <Divider />

      <MenuItem
        button
        onClick={() => {
          if (history.location.pathname !== "/meter") {
            history.push("/meter");
            props.name("meter");
          }
          if (history.location.pathname === "/meter") {
            props.name("meter");
          }
        }}
        selected={selected === "meter"}
      >
        <ListItemIcon className={classes.alignIcon}>
          <TimelineIcon />
        </ListItemIcon>
        <ListItemText primary="Meters" />
      </MenuItem>

      <Divider />

      <MenuItem
        button
        onClick={() => {
          if (history.location.pathname !== "/payment") {
            history.push("/payment");
            props.name("payment");
          }
          if (history.location.pathname === "/payment") {
            props.name("payment");
          }
        }}
        selected={selected === "payment"}
      >
        <ListItemIcon className={classes.alignIcon}>
          <MonetizationOnIcon />
        </ListItemIcon>
        <ListItemText primary="Payments" />
      </MenuItem>

      <Divider />

      <MenuItem
        button
        onClick={() => {
          if (history.location.pathname !== "/maintenance") {
            history.push("/maintenance");
            props.name("maintenance");
          }
          if (history.location.pathname === "/maintenance") {
            props.name("maintenance");
          }
        }}
        selected={selected === "maintenance"}
      >
        <ListItemIcon className={classes.alignIcon}>
          <BarChartIcon />
        </ListItemIcon>
        <ListItemText primary="Maintenance" />
      </MenuItem>

      <Divider />

      <MenuItem
        button
        onClick={() => {
          if (history.location.pathname !== "/utility") {
            history.push("/utility");
            props.name("utility");
          }
          if (history.location.pathname === "/utility") {
            props.name("utility");
          }
        }}
        selected={selected === "utility"}
      >
        <ListItemIcon className={classes.alignIcon}>
          <FlipCameraAndroidIcon />
        </ListItemIcon>
        <ListItemText primary="Utility" />
      </MenuItem>

      <Divider />

      <MenuItem
        button
        onClick={() => {
          if (history.location.pathname !== "/settings") {
            props.name("settings");
            history.push("/settings");
          }
          if (history.location.pathname === "/settings") {
            props.name("settings");
          }
        }}
        selected={selected === "settings"}
      >
        <ListItemIcon className={classes.alignIcon}>
          <SettingsIcon />
        </ListItemIcon>
        <ListItemText primary="Settings" />
      </MenuItem>
      <Divider />
    </div>
  );
};

const mapDispatchprops = (dispatch) => {
  return {
    loadBuildings: () => dispatch(buildingActions.getBuildings()),
  };
};

const mapStateToProps = (state) => {
  return {
    buildingList: state.buildings.buildingList,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(ListItems);

export { x as ListItems };
