import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import { BuildingList } from "../building";
import { MeterList } from "../meter";
import { Route, Switch, Redirect } from "react-router";
import { userActions } from "../../_actions";
import { Button, SwipeableDrawer, List, Avatar } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import { connect } from "react-redux";
import { TenantList } from "../tenant";
import { Dashboard } from "../dashboard";
import { PaymentList } from "../payment";
import { Maintanance } from "../maintanance";
import { Discount } from "../discount";
import { Utility } from "../utility";
import { Settings } from "../setting";
import { useLocation } from "react-router-dom";
import { ListItems } from "./listItems";
import { history } from "../../_helpers";
import config from "../../config";
import { BuildingDetail } from "../building/BuildingDetail";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    backgroundColor: "white",
    flex: 1,
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  mainContent: {
    width: "100%",
    marginTop: 50,
  },
  toolBar: {
    position: "absolute",
    zIndex: theme.zIndex.drawer + 1,
    flex: 1,
    width: "-webkit-fill-available",
    display: "flex",
  },
  title: {
    flex: 1,
  },
  drawerContainer: {
    display: "flex",
    flexDirection: "column",
    height: "100%",
  },
  userInfoContainer: {
    padding: theme.spacing(2),
    height: "100%",
    display: "flex",
    alignItems: "flex-end",
    flexGrow: 1,
  },
  userInfo: {
    display: "flex",
    alignItems: "center",
  },
  userName: {
    paddingLeft: theme.spacing(2),
  },
}));

function Main(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  let location = useLocation();
  const [selectedListItemName, setSelectedListItemName] = React.useState("");

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const toggleDrawer = (open) => (event) => {
    setOpen(open);
  };

  const userDetail = props.userDetail;
  React.useEffect(() => {
    userDetail();
  }, [userDetail]);

  const getAppbarTitle = () => {
    if (location.pathname.startsWith("/dashboard")) {
      return "Dashboard";
    } else if (location.pathname.startsWith("/tenant")) {
      return "Tenants";
    } else if (location.pathname.startsWith("/meter")) {
      return "Meters";
    } else if (location.pathname.startsWith("/payment")) {
      return "Payments";
    } else if (location.pathname.startsWith("/discount")) {
      return "Discounts";
    } else if (location.pathname.startsWith("/maintenance")) {
      return "Maintenance";
    } else if (location.pathname.startsWith("/utility")) {
      return "Utility";
    } else if (location.pathname.startsWith("/settings")) {
      return "Settings";
    } else {
      return "";
    }
  };

  const listItemName = (name) => {
    if (name != "building") {
      setSelectedListItemName(name);
      setOpen(false);
    }
  };

  return (
    <div className={classes.root}>
      <Toolbar className={classes.toolBar}>
        <IconButton onClick={handleDrawerOpen}>
          <Menu />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          {getAppbarTitle()}
        </Typography>
        {renderToolBarActions}
      </Toolbar>
      <div className={classes.mainContent}>{renderMainContent}</div>

      <SwipeableDrawer
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
        onClose={toggleDrawer(false)}
      >
        <div className={classes.drawerContainer}>
          <div>
            <List>
              {
                <ListItems
                  name={listItemName}
                  selectedListItem={selectedListItemName}
                />
              }
            </List>
          </div>
          <div className={classes.userInfoContainer}>
            <div
              className={classes.userInfo}
              onClick={(e) => {
                history.push("/user");
              }}
            >
              <Avatar
                src={
                  props.userData.photoId
                    ? config.api.BASE_URL +
                      "/image/" +
                      props.userData.photoId +
                      "/thumb"
                    : ""
                }
              />
              <div className={classes.userName}>
                {props.userData.name
                  ? props.userData.name
                      .split(" ")
                      .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                      .join(" ")
                  : ""}
              </div>
            </div>
          </div>
        </div>
      </SwipeableDrawer>
    </div>
  );
}

const renderMainContent = (
  <Switch>
    <Route path="/dashboard" exact component={Dashboard} />
    <Route path="/building/:buildingId" exact component={BuildingDetail} />
    <Route path="/tenant" exact component={TenantList} />
    <Route path="/meter" exact component={MeterList} />
    <Route path="/payment" exact component={PaymentList} />
    <Route path="/maintenance" exact component={Maintanance} />
    <Route path="/utility" exact component={Utility} />
    <Route path="/discount" exact component={Discount} />
    <Route path="/settings" exact component={Settings} />
    <Route path="/">
      <Redirect to="/dashboard" />
    </Route>
  </Switch>
);

const renderToolBarActions = (
  <Switch>
    <Route
      path="/tenant"
      exact
      render={() => (
        <Button
          onClick={(e) => {
            history.push("/tenant/new");
          }}
        >
          NEW
        </Button>
      )}
    />
    <Route
      path="/meter"
      exact
      render={() => (
        <Button
          onClick={(e) => {
            history.push("/meter/new");
          }}
        >
          NEW
        </Button>
      )}
    />
  </Switch>
);

const mapDispatchprops = (dispatch) => {
  return {
    userDetail: () => dispatch(userActions.userDetail()),
  };
};

const mapStateToProps = (state) => {
  return {
    userData: state.user.userDetail,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(Main);

export { x as Main };
