import React, { useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { Paper } from "@material-ui/core";
import { bookActions } from "../../_actions";
import {
  Grid,
  List,
  ListItem,
  Typography,
  Divider,
} from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    margin: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    height: '100%',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  infoItem: {
    display: "flex",
    justifyContent: "flex-end"
  }
}));

function Discount(props) {
  const classes = useStyles();
  const loadDiscount = props.loadDiscount;

  useEffect(() => {
    loadDiscount();
  }, [loadDiscount]);

  return (
    <Paper className={classes.paper}>
      <List>
        {
          props.discountList.sort((transaction1, transaction2) => {
            let moment1 = moment.utc(transaction1.transactionTime);
            let moment2 = moment.utc(transaction2.transactionTime);
            return moment1.diff(moment2) * -1;
          }).map((row) => (
            <Fragment>
              <ListItem>
                <Grid container>
                  <Grid item xs={6}>
                    <Typography>#{row.id}</Typography>
                  </Grid>
                  <Grid item xs={6} className={classes.infoItem}>
                    <Typography>{moment.utc(row.transactionTime).format("DD MMM YYYY")}</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>{"Tenant" + row.accountCreditId}</Typography>
                  </Grid>
                  <Grid item xs={6} className={classes.infoItem}>
                    <Typography>Rs. {row.amount}</Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography>{row.comment}</Typography>
                  </Grid>
                </Grid>
              </ListItem>
              <Divider/>
            </Fragment>
          ))
        }
      </List>
    </Paper>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadDiscount: () => dispatch(bookActions.getDiscount()),
  };
};

const mapStateToProps = (state) => {
  return {
    discountList: state.book.discountList,
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(Discount)
);

export { x as Discount };
