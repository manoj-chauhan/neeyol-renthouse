import React, { useEffect, useState, Fragment } from "react";
import { connect } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { bookActions } from "../../_actions";
import { Paper } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { Typography, Grid, List, ListItem, Divider } from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import Link from "@material-ui/core/Link";
import FlipCameraAndroidIcon from "@material-ui/icons/FlipCameraAndroid";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    height: "100%",
  },
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    display: "flex",
    overflow: "auto",
    height: "100%",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  btn: {
    marginBottom: 16,
  },
  infoItem: {
    display: "flex",
    justifyContent: "flex-end",
  },
  dialogTitle: {
    color: "blue",
  },
  recordUtilityButton: {
    marginTop: 10,
  },
  loadMoreLink: {
    marginLeft: 10,
  },
  alignUtilityListIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 150,
  },
  utilityListICon: {
    fontSize: 150,
  },
}));

function SimpleDialog(props) {
  const [amount, setAmount] = useState("");
  const [amountError, setAmountError] = useState(false);
  const [comment, setComment] = useState("");
  const [commentError, setCommentError] = useState(false);
  const [date, setDate] = useState("");
  const classes = useStyles();
  const { onClose, open, translation } = props;
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));


  const handleSubmit = (e) => {
    let x = moment.utc(date);
    if (amount && comment) {
      props.recordUtility({
        amount: amount,
        comment: comment,
        time: x.toISOString(),
      });
      onClose();
      setAmount("");
      setAmountError(false);
      setComment("");
      setCommentError(false);
    } else {
      setAmountError(amount.length === 0);
      setCommentError(comment.length === 0);
    }
  };

  const handleClose = () => {
    onClose();
    setAmount("");
    setAmountError(false);
    setComment("");
    setCommentError(false);
  };

  const handelChange = (e) => {
    const { name, value } = e.target;
    if (name === "amount") {
      setAmount(value);
      if (value.length === 0) {
        setAmountError(true);
      } else {
        setAmountError(false);
      }
    }
    if (name === "Comment") {
      setComment(value);
      if (value.length === 0) {
        setCommentError(true);
      } else {
        setCommentError(false);
      }
    }
    if (name === "date") {
      setDate(value);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Utility
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <TextField
              error={amountError}
              variant="outlined"
              margin="normal"
              name="amount"
              label="Amount"
              type="number"
              onChange={handelChange}
              helperText={amountError ? translation('common.amountError') : " "}
            />
            <TextField
              error={commentError}
              variant="outlined"
              margin="normal"
              name="Comment"
              label="Comment"
              type="text"
              onChange={handelChange}
              helperText={commentError ? translation('common.commentError') : " "}
            />
            <TextField
              id="date"
              variant="outlined"
              label="Date"
              type="date"
              name="date"
              defaultValue={moment.utc(moment()).toISOString().substring(0, 10)}
              className={classes.textField}
              onChange={handelChange}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <DialogActions>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSubmit}
                className={classes.recordUtilityButton}
              >
                Record Utility
              </Button>
            </DialogActions>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function Utility(props) {
  const [open, setOpen] = useState(false);
  const [pageNo, setPageNo] = useState(1);
  const classes = useStyles();

  const recordUtility = props.recordUtility;
  const loadUtility = props.loadUtility;
  const clearUtilityList = props.clearUtilityList;
  const { t, i18n } = useTranslation();

  useEffect(() => {
    clearUtilityList();
    loadUtility();
  }, [loadUtility]);

  const handleSubmit = (e) => {
    setPageNo(pageNo + 1);
    loadUtility(pageNo);
  };

  const openDilog = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <div className={classes.infoItem}>
        <Button
          onClick={openDilog}
          variant="contained"
          color="primary"
          className={classes.btn}
        >
          Record Utility
        </Button>
      </div>
      {/* <Paper className={classes.paper}> */}
      {props.utilityList.length == 0 ? (
        <div className={classes.alignUtilityListIcon}>
          <FlipCameraAndroidIcon className={classes.utilityListICon} />
          <Typography>No Utilities has Added Yet</Typography>
        </div>
      ) : (
        <></>
      )}

      <List>
        {props.utilityList
          .sort((transaction1, transaction2) => {
            let moment1 = moment.utc(transaction1.transactionTime);
            let moment2 = moment.utc(transaction2.transactionTime);
            return moment1.diff(moment2) * -1;
          })
          .map((row) => (
            <Fragment>
              <ListItem>
                <Grid container>
                  <Grid item xs={12}>
                    <Typography>#{row.id}</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>
                      {moment.utc(row.transactionTime).format("DD MMM YYYY")}
                    </Typography>
                  </Grid>
                  <Grid item xs={6} className={classes.infoItem}>
                    <Typography>Rs. {row.amount}</Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography>{row.comment}</Typography>
                  </Grid>
                </Grid>
              </ListItem>
              <Divider />
            </Fragment>
          ))}
        {props.remainingUtilityEntries > 0 ? (
          <Link className={classes.loadMoreLink} onClick={handleSubmit}>
            load more..
          </Link>
        ) : (
          ""
        )}
      </List>
      {/* </Paper> */}
      <SimpleDialog
        open={open}
        onClose={handleClose}
        recordUtility={recordUtility}
        translation={t}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    recordUtility: (utility) => dispatch(bookActions.recordUtility(utility)),
    loadUtility: (pageNo) => dispatch(bookActions.getUtility(pageNo)),
    clearUtilityList: () => dispatch(bookActions.clearUtilityList()),
  };
};

const mapStateToProps = (state) => {
  return {
    utilityList: state.book.utilityList,
    remainingUtilityEntries: state.book.remainingUtilityEntries
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(Utility)
);

export { x as Utility };
