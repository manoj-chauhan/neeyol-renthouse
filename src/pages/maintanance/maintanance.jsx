import React, { useEffect, useState, Fragment } from "react";
import { connect } from "react-redux";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { bookActions } from "../../_actions";
import { Paper } from "@material-ui/core";
import { List, ListItem, Grid, Typography, Divider } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import BarChartIcon from "@material-ui/icons/BarChart";
import moment from "moment";
import Link from "@material-ui/core/Link";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    height: "100%",
  },
  divider: {
    height: "5px",
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  paper: {
    display: "flex",
    overflow: "auto",
    height: "100%",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  btn: {
    marginBottom: 16,
  },
  infoItem: {
    display: "flex",
    justifyContent: "flex-end",
  },
  recordMaintananceButton: {
    width: "100%",
    marginTop: 20,
    marginBottom: 20,
  },
  dialogTitle: {
    color: "blue",
  },
  loadMoreLink: {
    margin: 20,
  },
  alignMaintainanceIcon: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: 120,
  },
  maintainanceIconStyle: {
    fontSize: 150,
  },
}));

function SimpleDialog(props) {
  const [amount, setAmount] = useState("");
  const [amountError, setAmountError] = useState(false);
  const [comment, setComment] = useState("");
  const [commentError, setCommentError] = useState(false);
  const [date, setDate] = useState("");
  const classes = useStyles();
  const { onClose, open, translation } = props;
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  const validation = () => {
    setAmountError(amount.length === 0);
    setCommentError(comment.length === 0);
    return !(amount.length === 0 || comment.length === 0);
  };

  const handleSubmit = (e) => {
    if (validation()) {
      let x = moment.utc(date);
      props.recordMaintanance({
        amount: amount,
        comment: comment,
        time: x.toISOString(),
      });
      onClose();
      setAmount("");
      setAmountError(false);
      setComment("");
      setCommentError(false);
    }
  };

  const handleClose = () => {
    onClose();
    setAmount("");
    setAmountError(false);
    setComment("");
    setCommentError(false);
  };

  const handelChange = (e) => {
    const { name, value } = e.target;
    if (name === "amount") {
      setAmount(value);
      if (value.length === 0) {
        setAmountError(true);
      } else {
        setAmountError(false);
      }
    }
    if (name === "Comment") {
      setComment(value);
      if (value.length === 0) {
        setCommentError(true);
      } else {
        setCommentError(false);
      }
    }
    if (name === "date") {
      setDate(value);
    }
  };

  return (
    <div>
      <Dialog
        // fullScreen={fullScreen}
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Maintenance
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <TextField
              error={amountError}
              variant="outlined"
              margin="normal"
              name="amount"
              label="Amount"
              type="number"
              onChange={handelChange}
              helperText={amountError ? translation('common.amountError') : " "}
            />
            <TextField
              error={commentError}
              variant="outlined"
              margin="normal"
              name="Comment"
              label="Comment"
              type="text"
              onChange={handelChange}
              helperText={commentError ? translation('common.commentError') : " "}
            />
            <TextField
              id="date"
              variant="outlined"
              margin="normal"
              label="Date"
              type="date"
              name="date"
              defaultValue={moment.utc(moment()).toISOString().substring(0, 10)}
              className={classes.textField}
              onChange={handelChange}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <Button
              variant="contained"
              color="secondary"
              className={classes.recordMaintananceButton}
              onClick={handleSubmit}
            >
              Record Maintenance
            </Button>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function Maintanance(props) {
  const [open, setOpen] = useState(false);
  const [pageNo, setPageNo] = useState(0);

  const classes = useStyles();
  const clearMaintananceList = props.clearMaintananceList;

  const recordMaintanance = props.recordMaintanance;
  const loadMaintanance = props.loadMaintanance;

  const { t, i18n } = useTranslation();

  useEffect(() => {
    clearMaintananceList();
    loadMaintanance(pageNo);
  }, [loadMaintanance]);

  const handleSubmit = (e) => {
    setPageNo(pageNo + 1);
    loadMaintanance(pageNo + 1);
  };

  const openDilog = () => {
    setOpen(true);
  };
  const handleClose = (value) => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <div className={classes.infoItem}>
        <Button
          variant="contained"
          onClick={openDilog}
          color="primary"
          className={classes.btn}
        >
          NEW Maintenance
        </Button>
      </div>
      {/* <Paper className={classes.paper}> */}
      {props.maintananceList.length == 0 ? (
        <div className={classes.alignMaintainanceIcon}>
          <BarChartIcon className={classes.maintainanceIconStyle} />
          <Typography>No Maintainance Has Added Yet</Typography>
        </div>
      ) : (
        <></>
      )}

      <List>
        {props.maintananceList
          .sort((transaction1, transaction2) => {
            let moment1 = moment.utc(transaction1.transactionTime);
            let moment2 = moment.utc(transaction2.transactionTime);
            return moment1.diff(moment2) * -1;
          })
          .map((row) => (
            <Fragment>
              <ListItem>
                <Grid container>
                  <Grid item xs={12}>
                    <Typography>#{row.id}</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>
                      {moment.utc(row.transactionTime).format("DD MMM YYYY")}
                    </Typography>
                  </Grid>
                  <Grid item xs={6} className={classes.infoItem}>
                    <Typography>Rs. {row.amount}</Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography>{row.comment}</Typography>
                  </Grid>
                </Grid>
              </ListItem>
              <Divider />
            </Fragment>
          ))}
        {props.remainingMaintenanceEntries > 0 ? (
          <Link onClick={handleSubmit} className={classes.loadMoreLink}>
            Load More..
          </Link>
        ) : (
          ""
        )}
      </List>
      {/* </Paper> */}
      <SimpleDialog
        roomList={props.roomList}
        open={open}
        onClose={handleClose}
        recordMaintanance={recordMaintanance}
        translation={t}
      />
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    recordMaintanance: (maintanance) =>
      dispatch(bookActions.recordMaintanance(maintanance)),
    loadMaintanance: (pageNo) => dispatch(bookActions.getMaintanance(pageNo)),
    clearMaintananceList: () => dispatch(bookActions.clearMaintananceList()),
  };
};

const mapStateToProps = (state) => {
  return {
    maintananceList: state.book.maintananceList,
    remainingMaintenanceEntries: state.book.remainingMaintenanceEntries
  };
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(Maintanance)
);

export { x as Maintanance };
