import React from "react";
import { makeStyles } from "@material-ui/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
});

export default function RoomListItem(props) {
  const classes = useStyles();

  // return <div className={classes.root}>{room.name}</div>;
  return (
    <ListItem
      button
      className={classes.root}
      onClick={() => {
        props.onRoomSelected(props.id);
      }}
    >
      <ListItemText
        primary={props.name
          .split(" ")
          .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
          .join(" ")}
        secondary="Second Floor"
      />
    </ListItem>
  );
}
