import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { roomActions } from "../../_actions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import { FormControl, FormLabel, OutlinedInput, Typography } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  profileImage: {
    // width: "100px",
    // height: "100px",
  },
  link: {
    paddingRight: "32px",
  },
  rightSideLink: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  select: {
    width: "100%"
  },
  formControl: {
    width: "100%"
  },
  dialogTitle: {
    color: "blue"
  }
}));

function DeallocationDialog(props) {
  const [meterReading, setMeterReading] = useState(0)
  const [meterReadingError, setMeterReadingError] = useState(0)
  const [image, setImage] = useState("");
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;

  const { t, i18n } = useTranslation();

  const handleClose = () => {
    onClose(selectedValue);
    setMeterReading(0);
    setMeterReadingError(false);
    setImage("");
  };

  const handleSubmit = (e) => {
    if (meterReading > props.previousReading) {
      props.deallocateRoom({
        roomId: props.roomId,
        meterReading: meterReading,
        imageId: image
      });
      handleClose();
    } else {
      setMeterReadingError(meterReading == 0);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "currentMeterReading") {
      setMeterReading(value);
      if (value.length === 0 || value <= props.previousReading) {
        setMeterReadingError(true)
      } else {
        setMeterReadingError(false)
      }
    }
    if (name === "Image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>Room Deallocation</DialogTitle>
        <DialogContent>

          <form className={classes.container} noValidate>
            <FormControl className={classes.formControl}>
              <Typography>
                Previous Reading is {props.previousReading}
              </Typography>

              <TextField
                error={meterReadingError}
                variant="outlined"
                margin="normal"
                name="currentMeterReading"
                label="Reading"
                type="number"
                onChange={handleChange}
                helperText={meterReadingError ? t('common.readingError') : ""}
              />
              <FormLabel component="legend">Meter Reading Photo</FormLabel>
              <OutlinedInput
                variant="outlined"
                margin="normal"
                name="Image"
                label="meter Image"
                type="file"
                onChange={handleChange}
              />
            </FormControl>
          </form>
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit}
            >
              Deallocate
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    deallocateRoom: (room) => dispatch(roomActions.deallocate(room))
  };
};

const mapStateToProps = (state) => {
  return {
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(DeallocationDialog);

export { x as DeallocationDialog };
