import config from "../../config";
import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { roomActions, tenantActions, settingActions } from "../../_actions";
import Grid from "@material-ui/core/Grid";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import { useTheme } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogContent from "@material-ui/core/DialogContent";
import InputLabel from "@material-ui/core/InputLabel";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import moment from "moment";
import { FormControl, FormLabel, OutlinedInput, Typography } from "@material-ui/core";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  profileImage: {
    // width: "100px",
    // height: "100px",
  },
  link: {
    paddingRight: "32px",
  },
  rightSideLink: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  select: {
    width: "100%"
  },
  formControl: {
    width: "100%"
  },
  dialogTitle: {
    color: "blue"
  }
}));


function AllocationDialog(props) {
  const [tenantId, setTenant] = useState("");
  const [tenantError, setTenantError] = useState(false);
  const [allocationDate, setAllocationDate] = useState("");
  const [allocationDateError, setAllocationDateError] = useState(false);
  const [unitCharges, setUnitCharges] = useState("");
  const [rentAmount, setRentAmount] = useState("");
  const [rentCycle, setRentCycle] = useState("");
  const [currentReading, setCurrentReading] = useState("");
  const [currentReadingError, setCurrentReadingError] = useState(false);
  const [image, setImage] = useState("");

  const classes = useStyles();
  const { onClose, selectedValue, open, previousReading } = props;
  const theme = useTheme();
  const loadTenants = props.loadTenants;
  const getSetting = props.getSetting;

  const defaultSetting = props.defaultSetting;

  const { t, i18n } = useTranslation();

  useEffect(() => {
    setRentCycle((defaultSetting) ? defaultSetting.rentCycle : "");
    setRentAmount((defaultSetting) ? defaultSetting.rent : "");
    setUnitCharges((defaultSetting) ? defaultSetting.electricityUnitCharges : "");
  }, [defaultSetting]);

  useEffect(() => {
    loadTenants();
  }, [loadTenants]);

  useEffect(() => {
    getSetting();
  }, [getSetting]);

  const handleClose = () => {
    onClose(selectedValue);
    setTenant("");
    setTenantError(false);
    setCurrentReading("");
    setCurrentReadingError(false);
    setAllocationDate("");
    setAllocationDateError(false);
  };

  const validate = () => {
    if (tenantId.length === 0 || allocationDate.length === 0) {
      setTenantError(tenantId.length === 0);
      if (currentReading <= previousReading) {
        setCurrentReadingError(currentReading <= previousReading);
      }
      setAllocationDateError(allocationDate.length === 0);
      return false;
    } else if (currentReading <= previousReading && previousReading > 0) {
      setCurrentReadingError(currentReading <= previousReading);
      return false;
    } else {
      return true;
    }
  }

  const handleSubmit = (e) => {
    if (validate()) {
      let x = moment.utc(allocationDate);
      if (tenantId && allocationDate && rentAmount && unitCharges && rentCycle) {
        props.allocateRoom({
          roomId: props.roomId,
          tenantId: tenantId,
          allocationTime: x.toISOString(),
          amount: rentAmount,
          rentCycle: rentCycle,
          electricityUnitCharges: unitCharges,
          reading: currentReading,
          imageId: image
        });
        handleClose();
      }
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "Name") {
      if (value.length === 0) {
        setTenantError(true);
      } else {
        setTenant(value);
        setTenantError(false);
      }
    }
    if (name === "allocationDate") {
      if (value.length === 0) {
        setAllocationDateError(true);
      } else {
        setAllocationDate(value);
        setAllocationDateError(false);
      }
    }
    if (name === "unitCharges") {
      setUnitCharges(value);
    }
    if (name === "rent") {
      setRentAmount(value);
    }
    if (name === "rentCycle") {
      setRentCycle(value);
    }
    if (name === "currentReading") {
      if (value.length === 0 || value <= previousReading) {
        setCurrentReadingError(true);
      } else {
        setCurrentReading(value);
        setCurrentReadingError(false);
      }
    }
    if (name === "Image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>Room Allocation</DialogTitle>
        <DialogContent>

          <form className={classes.container} noValidate>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Tenant
                  </InputLabel>
                  <Select onChange={handleChange} name="Name" className={classes.select} error={tenantError}>
                    {props.tenantList.map((tenant) => (
                      <MenuItem key={tenant.id} value={tenant.id}>
                        {tenant.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
                {
                  tenantError ? (
                    <Typography style={
                      {
                        color: "#f44336",
                        fontSize: "0.75rem"
                      }
                    }>
                      {t("common.nameError")}
                    </Typography>
                  ) : ""
                }
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <TextField
                    error={allocationDateError}
                    id="allocationDate"
                    label="Allocation Date"
                    type="date"
                    name="allocationDate"
                    className={classes.textField}
                    onChange={handleChange}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    helperText={
                      allocationDateError ? t("common.nameError") : ""
                    }
                  />
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <InputLabel id="demo-controlled-open-select-label">
                    Rent Cycle
                  </InputLabel>
                  <Select onChange={handleChange} name="rentCycle" className={classes.select}
                    defaultValue={(props.defaultSetting) ? props.defaultSetting.rentCycle : ""}>
                    <MenuItem key="monthly" value="monthly">
                      Monthly
                    </MenuItem>
                    <MenuItem key="weekly" value="weekly">
                      Weekly
                    </MenuItem>
                    <MenuItem key="daily" value="daily">
                      Daily
                    </MenuItem>
                    <MenuItem key="per minute" value="per minute">
                      Per Minute
                    </MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <TextField
                    id="rent"
                    label="Rent"
                    type="number"
                    name="rent"
                    defaultValue={(props.defaultSetting) ? props.defaultSetting.rent : ""}
                    onChange={handleChange}
                  />
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl className={classes.formControl}>
                  <TextField
                    id="unitCharges"
                    label="Per Unit Charges"
                    type="number"
                    name="unitCharges"
                    defaultValue={(props.defaultSetting) ? props.defaultSetting.electricityUnitCharges : ""}
                    onChange={handleChange}
                  />
                </FormControl>
              </Grid>
              {
                previousReading > 0 ? (
                  <>
                    <Grid item>
                      <Typography>
                        previous Reading is {previousReading}
                      </Typography>
                      <FormControl className={classes.formControl}>
                        <TextField
                          error={currentReadingError}
                          id="currentReading"
                          label="Current Reading"
                          variant="outlined"
                          type="number"
                          name="currentReading"
                          onChange={handleChange}
                          helperText={
                            currentReadingError ? t("common.readingError") : ""
                          }
                        />
                      </FormControl>
                    </Grid>
                    <Grid item>
                      <FormLabel component="legend">Meter Reading Photo</FormLabel>
                      <OutlinedInput
                        label="Meter Reading Photo"
                        type="file"
                        name="Image"
                        onChange={handleChange}
                      />
                    </Grid>
                  </>
                ) : (
                  <>
                  </>
                )
              }
            </Grid>
          </form>
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit}
            >
              Allocate
          </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}



const mapDispatchprops = (dispatch) => {
  return {
    allocateRoom: (room) => dispatch(roomActions.allocate(room)),
    getSetting: () => dispatch(settingActions.getSetting()),
    loadTenants: () => dispatch(tenantActions.getTenants())
  };
};

const mapStateToProps = (state) => {
  return {
    defaultSetting: state.setting.defaultSetting,
    tenantList: state.tenants.tenantList,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(AllocationDialog);

export { x as AllocationDialog };
