import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";
import { roomActions } from "../../_actions";
import { useParams } from "react-router";
import { FormControl, FormLabel, OutlinedInput } from "@material-ui/core";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function NewRoom(props) {
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState(false);
  const [roomImage, setRoomImage] = useState(null);

  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const pica = require('pica')();

  let { buildingId } = useParams();

  const handleSubmit = (e) => {
    if (name && buildingId) {
      props.addRoom({
        name: name,
        buildingId: buildingId,
        roomImage: roomImage,
      });
    } else {
      setNameError(name.length === 0);
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    if (name === "name") {
      setName(value);
      if (value.length === 0) {
        setNameError(true);
      } else {
        setNameError(false);
      }
    } else if (event.target.id === "roomImage") {

      if (event.target.files.length > 0) {
        setRoomImage(event.target.files[0]);
      }

    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          New Room
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            error={nameError}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="name"
            label="Name"
            name="name"
            autoFocus
            onChange={handleChange}
            helperText={nameError ? t('common.nameError') : ""}
          />
          <FormLabel component="legend">Room Image</FormLabel>
          <TextField
            id="roomImage"
            type="file"
            fullWidth
            name="image"
            variant="outlined"
            onChange={handleChange}
          />
          <Button
            onClick={handleSubmit}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            ADD ROOM
          </Button>
        </form>
      </div>
    </Container>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    addRoom: (room) => dispatch(roomActions.addRoom(room)),
  };
};

const mapStateToProps = (state) => {
  return {};
};

const x = withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(NewRoom)
);

export { x as NewRoom };
