import config from "../../config";
import React, { useEffect, useState } from "react";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { roomActions, settingActions } from "../../_actions";
import Grid from "@material-ui/core/Grid";
import { Typography, Divider, Dialog, DialogTitle, DialogContent, DialogActions, Button } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import { AllocationDialog } from "./AllocationDialog";
import { DeallocationDialog } from "./DeallocationDialog";
import moment from "moment";

const emails = ["username@gmail.com", "user02@gmail.com"];

const useStyles = makeStyles((theme) => ({
  profileImage: {
    // width: "100px",
    // height: "100px",
  },
  link: {
    paddingRight: "32px",
  },
  rightSideLink: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  select: {
    width: "100%",
  },
  formControl: {
    width: "100%",
  },

  // style of Room deAllocation withOut reading
  dialogTitle: {
    color: "blue",
  }
}));

function RoomDeAllocationWithoutReading(props) {

  const { open, onclose } = props;
  const style = useStyles();

  return (
    <Dialog
      open={open}
      onClose={() => {
        onclose();
      }}
    >
      <DialogTitle className={style.dialogTitle} >
        Room Deallocation
        </DialogTitle>
      <DialogContent>
        You want to sure for deallocate room.
        </DialogContent>
      <DialogActions>
        <Button
          color="secondary"
          variant="contained"
          onClick={() => {
            props.roomDeAllocate({
              roomId: props.roomId
            }
            )
            onclose();
          }}
        >
          Deallocate
        </Button>
      </DialogActions>
    </Dialog>
  );
}


function Tenant(props) {
  const [Open, setOpen] = useState(false);
  const [State, setState] = useState(false);
  const [selectedValue, setSelectedValue] = useState(emails[1]);
  const [roomDeAllocationDialog, setRoomDeAllocationDialog] = useState(false);
  const loadAllocationDetail = props.loadAllocationDetail;
  const loadTenants = props.loadTenants;

  useEffect(() => {
    loadAllocationDetail(props.roomId);
  }, [loadAllocationDetail, props.roomId]);

  const roomMeterAllocationDetail = props.roomMeterAllocationDetail;
  useEffect(() => {
    roomMeterAllocationDetail(props.roomId);
  }, [roomMeterAllocationDetail, props.roomId]);

  useEffect(() => {
    props.loadRoomSettings();
  }, []);

  const allocateRoom = props.allocateRoom;
  const deallocateRoom = props.deallocateRoom;

  const allocateHandleClickOpen = () => {
    setOpen(true);
  };
  const deAllocateHandleClickOpen = () => {
    setState(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    setState(false);
    setSelectedValue(value);
  };

  const classes = useStyles();

  const isRoomAllocated =
    props.allocationDetail != null && props.allocationDetail.id > 0;

  const toggleAllocationButton = isRoomAllocated ? (
    <Link
      color="primary"
      onClick={(e) => {
        if (props.roomMeterDetail.lastMeterReading > 0) {
          console.log("open room deAllocation dialog of meter reading");
          deAllocateHandleClickOpen();
        } else {
          console.log("open room deAllocation dialog of meter reading");
          setRoomDeAllocationDialog(true);
        }
      }}
    >
      DeAllocate Room
    </Link>
  ) : (
    <Link
      color="primary"
      onClick={(e) => {
        allocateHandleClickOpen();
      }}
    >
      Allocate Room
    </Link>
  );

  let content = isRoomAllocated ? (
    <div>
      <Link
        className={classes.link}
        color="primary"
        href={"/tenant/" + props.allocationDetail.id}
      >
        <Grid container spacing={1}>
          <Grid item>
            <Avatar
              className={classes.profileImage}
              alt="profile"
              src={
                config.api.BASE_URL +
                "/image/" +
                props.allocationDetail.imageId +
                "/thumb"
              }
            />
          </Grid>
          <Grid item>
            <Typography variant="h6" component="h6">
              {props.allocationDetail.name
                .split(" ")
                .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                .join(" ")}
            </Typography>
          </Grid>
        </Grid>
      </Link>
      <Grid container direction="column" justify="space-betweens">
        <Grid container item justify="space-between">
          <Grid item>Room rent</Grid>
          <Grid item>
            Rs. {props.allocationDetail.roomRent}{" "}
            {props.allocationDetail.rentCycle}
          </Grid>
        </Grid>
        <Grid container item justify="space-between">
          <Grid item>Electricity Unit Charge</Grid>
          <Grid item>
            Rs. {props.allocationDetail.electricityUnitCharges}/unit.
          </Grid>
        </Grid>
        <Grid container item justify="space-between">
          <Grid item>Room Allocation Date</Grid>
          <Grid item>
            {moment
              .utc(props.allocationDetail.roomAllocationDate)
              .format("DD MMM YYYY")}
            .
          </Grid>
        </Grid>
      </Grid>
      <DeallocationDialog
        selectedValue={selectedValue}
        open={State}
        onClose={handleClose}
        deallocateRoom={deallocateRoom}
        roomId={props.roomId}
        previousReading={props.roomMeterDetail.lastMeterReading}
      />
      <RoomDeAllocationWithoutReading
        open={roomDeAllocationDialog}
        onclose={() => {
          setRoomDeAllocationDialog(false);
        }}
        roomDeAllocate={props.deallocateRoom}
        roomId={props.roomId}
      />
    </div>
  ) : (
    <div>
      <Typography>This room is not allocated to any tenant.</Typography>
      <AllocationDialog
        open={Open}
        onClose={handleClose}
        roomId={props.roomId}
        previousReading={props.roomMeterDetail.lastMeterReading}
      />
    </div>
  );

  return (
    <React.Fragment>
      <Grid container>
        {props.roomDetail.roomAvailability ? (
          <>
            <Divider />
            <Grid item xs={6}>
              <Typography component="h6" variant="h6" color="primary">
                Tenant
              </Typography>
            </Grid>
            {
              Object.keys(props.roomSettings).length !== 0 ? (
                <Grid item xs={6} className={classes.rightSideLink}>
                  <div>{toggleAllocationButton}</div>
                </Grid>
              ) : ""
            }
          </>
        ) : (
          ""
        )}
      </Grid>

      {
        Object.keys(props.roomSettings).length === 0 ? (
          <Typography>
            Default settings has not added.
          </Typography>
        ) : ""
      }

      {props.roomDetail.roomAvailability ? (
        <>
          <Divider />
          {content}
        </>
      ) : (
        <>
          <Typography color="primary">
            Room
          </Typography>
          <Divider />
          <Typography>Room Is Not Available</Typography>
        </>
      )}
    </React.Fragment>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    loadAllocationDetail: (roomId) =>
      dispatch(roomActions.getAllocationDetail(roomId)),
    roomMeterAllocationDetail: (roomId) => {
      dispatch(roomActions.getRoomMeterAllocationDetail(roomId));
    },
    deallocateRoom: (room) => dispatch(roomActions.deallocate(room)),
    loadRoomSettings: () => dispatch(settingActions.getSetting())
  };
};

const mapStateToProps = (state) => {
  return {
    allocationDetail: state.rooms.selectedRoomAllocationDetail,
    roomDetail: state.rooms.selectedRoomDetail,
    roomMeterDetail: state.rooms.roomMeterDetail,
    roomSettings: state.setting.defaultSetting
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(Tenant);

export { x as Tenant };
