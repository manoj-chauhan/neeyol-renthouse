import React from 'react';
import List from '@material-ui/core/List';
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

class Room extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      roomlist:[],
    }
  }

  componentDidMount(){

    const url ="http://localhost:8080";
  
    //unallocated meterlist
    axios.get(url + "/unallocatedRoomList")
        .then(res => {
          const room = res.data;
          this.setState({ roomlist:room});
        }).catch((error) => {
        })
    }

    handleRoom = () =>{
        this.props.onAllocation();
    }

    handleClick = (room) =>{
      let url = "http://localhost:8080"
      let data = {
          tenantId: "1",
          roomId: room.id
        };
        let body = JSON.stringify(data);
    
        let myHeaders = { "Content-Type": "application/json" };
        let heading = {
          headers: myHeaders
        };
    
        let onSuccess = response => {
          alert("Room Allocated");
          this.props.onAllocation();
        };
    
        let onerror = error => {
        };
    
        axios
          .post(url + "/Allocateroom", body, heading)
          .then(onSuccess)
          .catch(onerror);
    }

  render(){
    const {classes} = this.props;
  return (
    <div className={classes.root}>
      <List component="nav" aria-label="Main mailbox folders">
      {this.state.roomlist.map((room, index) => [
       <ListItem button  className={classes.root} onClick={e => this.handleClick(room)}>
       <ListItemText primary={room.name} secondary = "Second Floor"/>   
   </ListItem>
      ])}
      </List>
    </div>
  );
  }
}
export default withStyles(useStyles)(Room);