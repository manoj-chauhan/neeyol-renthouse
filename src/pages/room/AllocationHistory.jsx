import React, { useEffect } from "react";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import { Grid, Link } from "@material-ui/core";
import { roomActions } from "../../_actions";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  personalInfo: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: theme.palette.background.paper,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  row: {
    marginBottom: 24,
  },
  duration: {
    display: "flex",
    justifyContent: "flex-end",
  },
}));

function AllocationHistory(props) {
  const classes = useStyles();
  const loadAllocationHistory = props.loadAllocationHistory;

  useEffect(() => {
    loadAllocationHistory(props.roomId);
  }, [loadAllocationHistory, props.roomId]);

  return (
    <React.Fragment>
      <Typography component="h6" variant="h6" color="primary">
        Allocation History
      </Typography>
      <Divider />
      {props.allocationHistory.map((row) => (
        <div className={classes.row}>
          <div>
            <Link href={"/tenant/" + row.tenantId}>
              {row.tenantName
                .split(" ")
                .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                .join(" ")}
            </Link>
          </div>
          <div>
            Rs. {row.amount} {row.rentCycle}
          </div>

          <Grid container>
            <Grid item xs={6}>
              From{" "}
              {moment(row.allocationTime, "YYYY-MM-DD hh:mm").format(
                "DD MMM YYYY"
              )}
            </Grid>
            <Grid item xs={6} className={classes.duration}>
              Till{" "}
              {moment(row.deAllocationTime, "YYYY-MM-DD hh:mm")
                .utc()
                .format("DD MMM YYYY")}
            </Grid>
          </Grid>
        </div>
      ))}

      {props.allocationHistory.length === 0 ? (
        <Typography>No history available...</Typography>
      ) : (
        ""
      )}
    </React.Fragment>
  );
}
const mapDispatchprops = (dispatch) => {
  return {
    loadAllocationHistory: (roomId) =>
      dispatch(roomActions.getAllocationHistory(roomId)),
  };
};

const mapStateToProps = (state) => {
  return {
    allocationHistory: state.rooms.selectedRoomAllocationHistory,
  };
};

const x = connect(mapStateToProps, mapDispatchprops)(AllocationHistory);

export { x as AllocationHistory };
