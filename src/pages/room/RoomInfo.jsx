import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Grid,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  TextField,
  CircularProgress
} from "@material-ui/core";
import { roomActions } from "../../_actions";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import CardMedia from "@material-ui/core/CardMedia";
import config from "../../config";
import { Edit, HomeOutlined } from "@material-ui/icons";
import { useTranslation } from "react-i18next";

const useStyles = (theme) => ({
  root: {},
  cover: {
    height: 100,
    width: 100,
    borderRadius: 10
  },
  editButton: {
    margin: 5,
    marginLeft: 8,
  },
  available: {
    marginTop: 44,
  },
  noProfileImage: {
    background: "#333",
    height: 100,
    width: 80,
    borderRadius: 20,
  },
  loaderSize: {
    position: "absolute",
    top: 40,
    right: 40
  }
});

function UpdateRoomInfo(props) {
  const { open, onClose, translation } = props;
  const { roomId } = useParams();
  const [roomName, setRoomName] = useState("");
  const [roomNameError, setRoomNameError] = useState(false);

  const handleChange = (e) => {
    if (e.target.name === "name") {
      if (e.target.value) {
        setRoomName(e.target.value);
        setRoomNameError(false);
      } else {
        setRoomNameError(true);
      }
    }
  };

  const handleSubmit = () => {
    if (roomName) {
      props.updateRoom({
        roomId: roomId,
        name: roomName,
        isAvailable: props.roomAvailable,
      });
      onClose();
    } else {
      setRoomNameError(true);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={() => {
        onClose();
        setRoomName("");
        setRoomNameError(false);
      }}
    >
      <DialogTitle color="primary">Update Room Name</DialogTitle>
      <DialogContent>
        <TextField
          error={roomNameError}
          name="name"
          label="Room Name"
          variant="outlined"
          margin="normal"
          defaultValue={props.roomName}
          onChange={handleChange}
          helperText={roomNameError ? translation('common.nameError') : ""}
        />
      </DialogContent>
      <DialogActions>
        <Button variant="contained" color="secondary" onClick={handleSubmit}>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

function RoomInfo(props) {
  const { classes } = props;

  const [roomAvailable, setRoomAvailable] = useState(false);
  const [updateRoom, setUpdateRoom] = useState(false);
  const { roomId } = useParams();
  const fetchRoomDetail = props.fetchRoomDetail;
  const [image, setImage] = useState(null);
  const [loaderSize, setLoaderSize] = useState(50);
  const [editRoomNameDialog, setEditRoomNameDialog] = useState(false);
  const roomName = props.roomDetail.name;
  const { i18n, t } = useTranslation();

  let imageId = null;

  useEffect(() => {
    fetchRoomDetail(roomId);
  }, [fetchRoomDetail, roomId]);

  useEffect(() => {
    if (updateRoom) {
      props.updateRoom({
        roomId: roomId,
        name: roomName,
        isAvailable: roomAvailable,
      });
      setUpdateRoom(false);
    }
  }, [updateRoom]);

  const handleChange = (e) => {
    const { name } = e.target;
    if (name === "image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
      props.uploadRoomImage({
        image: e.target.files[0],
        caption: "room image" + roomId,
        roomId: roomId,
      });
    }
  };

  if (props.roomDetail.imageList && props.roomDetail.imageList.length > 0) {
    const length = props.roomDetail.imageList.length;
    imageId = props.roomDetail.imageList[length - 1].imageId;

  }

  return (
    <div className={classes.root}>
      <Grid container className={classes.info}>
        <Grid item xs={8}>
          <Grid container>
            <Grid item>
              <Typography component="h5" variant="h5" color="textPrimary">
                {props.roomDetail.name
                  ? props.roomDetail.name
                    .split(" ")
                    .map((w) => w.slice(0, 1).toUpperCase() + w.slice(1))
                    .join(" ")
                  : ""}
              </Typography>
              <Typography component="h5" variant="h5" color="textPrimary">
                {"#" + roomId}
              </Typography>
            </Grid>
            <Grid item>
              <Edit
                onClick={() => {
                  setEditRoomNameDialog(true);
                }}
              />
            </Grid>
          </Grid>

          {!props.roomDetail.roomAvailability ? (
            <Button
              name="roomAvailabilityTrue"
              variant="contained"
              color="primary"
              className={classes.available}
              onClick={() => {
                setUpdateRoom(true);
                setRoomAvailable(true);
              }}
            >
              Make Available
            </Button>
          ) : props.roomDetail.roomAvailability & props.roomDetail.allocated ? (
            ""
          ) : (
            <Button
              name="roomAvailabilityFalse"
              variant="contained"
              color="secondary"
              className={classes.available}
              onClick={() => {
                setUpdateRoom(true);
                setRoomAvailable(false);
              }}
            >
              Make Unavailable
            </Button>
          )}
        </Grid>
        <Grid
          item
          xs={4}
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
          }}
        >
          {imageId ? (
            <>
              <CircularProgress
                className={classes.loaderSize}
                size={loaderSize}
                disableShrink={true}
              />
              <img
                src={config.api.BASE_URL + "/image/" + imageId + "/thumb"}
                onLoad={() => {
                  setLoaderSize(0);
                }}
                className={classes.cover}
              />
            </>
          ) : (
            <>
              <HomeOutlined className={classes.noProfileImage} />
            </>
          )}
          <Button
            variant="contained"
            color="primary"
            component="label"
            className={classes.editButton}
          >
            Edit
            <input name="image" type="file" hidden onChange={handleChange} />
          </Button>
        </Grid>
      </Grid>
      <UpdateRoomInfo
        open={editRoomNameDialog}
        onClose={() => {
          setEditRoomNameDialog(false);
        }}
        roomName={props.roomDetail.name}
        roomAvailable={props.roomDetail.roomAvailability}
        updateRoom={props.updateRoom}
        translation={t}
      />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    roomDetail: state.rooms.selectedRoomDetail,
  };
};

const mapDispatchprops = (dispatch) => {
  return {
    fetchRoomDetail: (roomId) => {
      dispatch(roomActions.getRoom(roomId));
    },
    uploadRoomImage: (roomInfo) => {
      dispatch(roomActions.uploadRoomImage(roomInfo));
    },
    updateRoom: (roomInfo) => {
      dispatch(roomActions.updateRoom(roomInfo));
    },
  };
};

export default withStyles(useStyles)(
  connect(mapStateToProps, mapDispatchprops)(RoomInfo)
);
