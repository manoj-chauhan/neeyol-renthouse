import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";
import { AllocationHistory } from './AllocationHistory';
import { useParams } from "react-router-dom";
import {AllocatedMeter} from './AllocatedMeter';
import { Tenant } from './Tenant';
import RoomInfo from './RoomInfo';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  },
  section: {
    marginTop:  theme.spacing(4)
  }
}));

export function RoomDetail(props) {
  
  const classes = useStyles();

  let { roomId } = useParams();

  return (
    <div className={classes.root}>
      <div>
        <RoomInfo roomId={roomId}/>
      </div>
      <div className={classes.section}>
        <AllocatedMeter roomId={roomId}/>
      </div>
      <div className={classes.section}>
        <Tenant roomId={roomId}/>
      </div>
      <div className={classes.section}>
        <AllocationHistory roomId={roomId}/>
      </div>
    </div>  
  );
}
