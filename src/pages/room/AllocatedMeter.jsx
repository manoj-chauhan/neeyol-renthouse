import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import { connect } from "react-redux";
import { meterActions } from "../../_actions";
import { roomActions } from "../../_actions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { FormControl, FormLabel, OutlinedInput } from "@material-ui/core";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { useParams } from "react-router-dom";
import moment from "moment";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  personalInfo: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: theme.palette.background.paper,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
  rightSideLink: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  dialogTitle: {
    color: "blue",
  },
}));

function RecordReadingDialog(props) {
  const [reading, setReading] = useState(0);
  const [readingError, setReadingError] = useState(false);
  const [image, setImage] = useState(null);
  const [meterImageError, setMeterImageError] = useState(false);
  const classes = useStyles();
  const { onClose, selectedValue, open, previousReading } = props;

  const meterId = props.roomMeterDetail;
  const { t, i18n } = useTranslation();

  const handleSubmit = (e) => {
    if (reading > 0 && reading > previousReading) {
      props.recordReading({
        image: image,
        Reading: reading,
        meterId: meterId,
        roomId: props.roomId,
      });
      handleClose();
    } else {
      setReadingError(reading == 0 || reading <= previousReading);
    }
  };

  const handleClose = () => {
    onClose(selectedValue);
    setReading(0);
    setReadingError(false);
    setImage(null);
    setMeterImageError(false);
  };

  const handleMenuItemClick = (e) => {
    const { name, value } = e.target;
    if (name === "reading") {
      setReading(value);
      if (value.length === 0 || value <= previousReading) {
        setReadingError(true);
      } else {
        setReadingError(false);
      }
    }
    if (name === "image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
      setMeterImageError(false);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Reading
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <Typography color="primary">
              Previous reading is {previousReading}
            </Typography>
            <TextField
              error={readingError}
              variant="outlined"
              margin="normal"
              name="reading"
              label="Reading"
              type="number"
              onChange={handleMenuItemClick}
              helperText={readingError ? t('common.readingError') : ""}
            />
            <FormLabel>Meter Image</FormLabel>
            <TextField
              error={meterImageError}
              variant="outlined"
              name="image"
              type="file"
              onChange={handleMenuItemClick}
              helperText={meterImageError ? t('addReading.meterImage') : ""}
            />
          </FormControl>
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={handleSubmit}
            >
              Add
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function MeterAllocationDialog(props) {
  const { roomId } = useParams();
  const [reading, setReading] = React.useState(0);
  const [readingError, setReadingError] = React.useState(false);
  const [meter, setMeter] = React.useState(" ");
  const [meterError, setMeterError] = React.useState(false);
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;
  const [image, setImage] = useState("");
  const [previousMeterReading, setPreviousMeterReading] = useState(0);

  const { t, i18n } = useTranslation();

  const allocationSubmitAction = (e) => {
    if (meter && reading && reading > previousMeterReading) {
      props.allocateMeter({
        initialReading: reading,
        meterId: meter,
        roomId: roomId,
        imageId: image,
      });
      handleClose();
    }
    setMeterError(meter.length === 0 || meter.length === 1); // default value of meter is 1 when page load.
    setReadingError(reading === 0 || reading < previousMeterReading);
  };

  const handleClose = () => {
    onClose(selectedValue);
    setMeter("");
    setMeterError(false);
    setImage("");
    setReading(0);
    setReadingError(false);
    setPreviousMeterReading(0);
  };

  const handleMenuItemClick = (e) => {
    const { name, value } = e.target;
    if (name === "reading") {
      setReading(value);
      if (value.length === 0) {
        setReadingError(true);
      } else {
        setReadingError(false);
      }
      if (previousMeterReading > value || previousMeterReading == value) {
        setReadingError(true);
      }
    }
    if (name === "Name") {
      setMeter(value.id);
      setPreviousMeterReading(value.lastMeterReading);
      setMeterError(false);
    }
    if (name === "Image") {
      setImage(e.target.files.length > 0 ? e.target.files[0] : null);
    }
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Meter Allocation
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-controlled-open-select-label">
              Meter
            </InputLabel>
            <Select
              error={meterError}
              variant="outlined"
              margin="normal"
              name="Name"
              label="Meter"
              onChange={handleMenuItemClick}
              helperText={meterError ? "Meter cannot be empty" : " "}
            >
              {props.AllocatedMeterList.map((meter) => (
                <MenuItem key={meter.id} value={meter}>
                  {meter.name}
                </MenuItem>
              ))}
            </Select>
            {
              meterError ?
                <Typography style={{
                  color: "#f44336",
                  fontSize: "0.75rem",
                  marginLeft: "14px"
                }}>
                  {t('common.nameError')}
                </Typography>
                : ""
            }

            {previousMeterReading ? (
              <Typography>Previous Reading is {previousMeterReading}</Typography>
            ) : (
              ""
            )}

            <TextField
              error={readingError}
              variant="outlined"
              margin="normal"
              name="reading"
              label="Reading"
              type="number"
              onChange={handleMenuItemClick}
              helperText={
                readingError ? t('common.readingError') : ""
              }
            />

            <FormLabel component="legend">Image</FormLabel>
            <OutlinedInput
              label="Image"
              type="file"
              name="Image"
              onChange={handleMenuItemClick}
            />
            <DialogActions>
              <Button
                variant="contained"
                color="secondary"
                onClick={allocationSubmitAction}
              >
                Allocate
              </Button>
            </DialogActions>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function DeallocateSimpleDialog(props) {

  const [lastReading, setLastReading] = useState(0);
  const [meterReadingImage, setMeterReadingImage] = useState(null);
  const [meterReadingImageError, setMeterReadingImageError] = useState(false);
  const [meterReadingImageCaption, setMeterReadingImageCaption] = useState("");
  const [lastReadingError, setLastReadingError] = useState(false);

  const classes = useStyles();
  const { open, onClose, selectedValue } = props;
  const theme = useTheme();
  const { t, i18n } = useTranslation();

  const validation = () => {
    setLastReadingError(lastReading.length === 0);
    // setMeterReadingImageError(meterReadingImage === null);
    return !(lastReadingError || meterReadingImageError);
  };

  const meterId = props.roomMeterDetail;
  const handleSubmit = (e) => {
    if (validation() && lastReading > props.currentReading) {
      props.deallocateMeter({
        LastReading: lastReading,
        meterId: meterId,
        imageId: meterReadingImage,
        caption: meterReadingImageCaption,
      });
      handleClose();
    } else {
      setLastReadingError(lastReading === 0);
      // setMeterReadingImageError(meterReadingImage === null);
      if (props.currentReading >= lastReading) {
        setLastReadingError(true);
      }
    }

  };

  const handleMenuItemClick = (e) => {
    const { name, value } = e.target;
    if (name === "lastReading") {
      if (value.length === 0 || value <= props.currentReading) {
        setLastReadingError(true);
      } else {
        setLastReading(value);
        setLastReadingError(false);
      }
    }
    if (name === "meterReadingImage") {
      setMeterReadingImage(
        e.target.files.length > 0 ? e.target.files[0] : null
      );
      setMeterReadingImageError(false);
    }
    if (name === "meterReadingImageCaption") {
      setMeterReadingImageCaption(value);
    }
  };

  const handleClose = () => {
    onClose(selectedValue);
    setLastReading(0);
    setLastReadingError(false);
    setMeterReadingImage(null);
    setMeterReadingImageError(false);
    setMeterReadingImageCaption("");
  };

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
      >
        <DialogTitle id="simple-dialog-title" className={classes.dialogTitle}>
          Meter Deallocate
        </DialogTitle>
        <DialogContent>
          <FormControl className={classes.formControl}>
            <Typography color="primary">
              Previous Reading is {props.currentReading} KWH
            </Typography>
            <TextField
              error={lastReadingError}
              variant="outlined"
              margin="normal"
              name="lastReading"
              label="Current Reading"
              type="number"
              onChange={handleMenuItemClick}
              helperText={
                lastReadingError
                  ? t("common.readingError")
                  : ""
              }
            />
            <FormControl
              component="fieldset"
              margin="normal"
              required
              fullWidth
            >
              <FormLabel>Meter Reading Image</FormLabel>
              <OutlinedInput
                error={meterReadingImageError}
                // label="Meter Reading Image"
                type="file"
                name="meterReadingImage"
                onChange={handleMenuItemClick}
              />
              {meterReadingImageError ? (
                <Typography color="secondary">Meter reading image is not be empty</Typography>
              ) : (
                ""
              )}
            </FormControl>
            <TextField
              variant="outlined"
              margin="normal"
              name="meterReadingImageCaption"
              label="Meter Reading  Caption"
              type="text"
              onChange={handleMenuItemClick}
            />
            <DialogActions>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleSubmit}
              >
                Deallocate
              </Button>
            </DialogActions>
          </FormControl>
        </DialogContent>
      </Dialog>
    </div>
  );
}

function AllocatedMeter(props) {
  const [open, setOpen] = React.useState(false);
  const [state, setState] = React.useState(false);
  const [deallocateDialogOpen, setDeallocateDialogOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState("Idea");
  const classes = useStyles();

  const roomMeterAllocationDetail = props.roomMeterAllocationDetail;
  useEffect(() => {
    roomMeterAllocationDetail(props.roomId);
  }, [roomMeterAllocationDetail, props.roomId]);

  const loadMeters = props.loadMeters;
  useEffect(() => {
    loadMeters();
  }, [loadMeters]);

  const loadAllocationDetail = props.loadAllocationDetail;
  useEffect(() => {
    loadAllocationDetail(props.roomId);
  }, [loadAllocationDetail, props.roomId]);

  const handleRecordReadingLinkClick = (e) => {
    setOpen(true);
  };
  const handleRecordReadingDialogClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };
  const handleAllocateLinkClickEvent = (e) => {
    setState(true);
  };
  const handleMeterAllocationDialogClose = (value) => {
    setState(false);
    setSelectedValue(value);
  };
  const handleDeallocateLinkClickEvent = (e) => {
    setState(true);
    // setDeallocateDialogOpen(true);
  };
  const handleMeterDeAllocationDialogClose = (value) => {
    setState(false);
    // setSelectedValue(value);
  };


  return (
    <React.Fragment>

      {
        props.roomDetail.roomAvailability && Object.keys(props.roomMeterDetail) != 0 ? (
          <>
            <Grid container>
              <Grid item xs={6}>
                <Typography component="h6" variant="h6" color="primary">
                  Meter
          </Typography>
              </Grid>
              <Grid item xs={6} className={classes.rightSideLink}>
                {props.roomMeterDetail.id !== 0 ? (
                  <Typography>
                    <Link onClick={handleDeallocateLinkClickEvent}>Deallocate</Link>
                  </Typography>
                ) : (
                  <Typography>
                    <Link onClick={handleAllocateLinkClickEvent}>Allocate</Link>
                  </Typography>
                )}
              </Grid>
            </Grid>
            <Divider />
            {props.roomMeterDetail.id !== 0 ? (
              <div>
                <Typography>
                  <Typography color="textPrimary" className={classes.depositContext}>
                    {props.roomMeterDetail.name}
                  </Typography>
                  <Typography color="textPrimary">
                    <strong>{props.roomMeterDetail.lastMeterReading} KWH</strong> Last
              reading dated on{" "}
                    {moment
                      .utc(props.roomMeterDetail.lastMeterReadingTime)
                      .format("DD MMM YYYY")}
                  </Typography>
                  {
                    props.roomDetail.allocated ? (
                      <Typography>
                        <Link onClick={handleRecordReadingLinkClick}>Add Reading</Link>
                      </Typography>
                    ) : ""
                  }
                  <RecordReadingDialog
                    open={open}
                    selectedValue={selectedValue}
                    onClose={handleRecordReadingDialogClose}
                    recordReading={props.recordReading}
                    roomMeterDetail={props.roomMeterDetail.id}
                    previousReading={props.roomMeterDetail.lastMeterReading}
                    roomId={props.roomId}
                  />
                </Typography>
                <DeallocateSimpleDialog
                  open={state}
                  // selectedValue={selectedValue}
                  onClose={handleMeterDeAllocationDialogClose}
                  deallocateMeter={props.deallocateMeter}
                  roomMeterDetail={props.roomMeterDetail.id}
                  currentReading={props.roomMeterDetail.lastMeterReading}
                />
              </div>
            ) : (
              <div>
                <Typography>Meter is not allocated in this room</Typography>

                <MeterAllocationDialog
                  open={state}
                  selectedValue={selectedValue}
                  onClose={handleMeterAllocationDialogClose}
                  roomMeterDetail={props.roomMeterDetail.id}
                  AllocatedMeterList={props.meterList.filter(
                    (meter) => meter.allocated === false
                  )}
                  allocateMeter={props.allocateMeter}
                />
              </div>
            )}
          </>
        ) : (
          <>
            <Typography color="primary">
              Meter
             </Typography>
            <Divider />
            <Typography>
              Room Is Not Allocated Any Tenant
          </Typography>
          </>
        )
      }
    </React.Fragment>
  );
}

const mapDispatchprops = (dispatch) => {
  return {
    recordReading: (meterReadingInfo) => {
      dispatch(meterActions.recordReading(meterReadingInfo));
    },
    roomMeterAllocationDetail: (roomId) => {
      dispatch(roomActions.getRoomMeterAllocationDetail(roomId));
    },
    loadMeters: () => {
      dispatch(meterActions.getMeters());
    },
    loadAllocationDetail: (roomId) => {
      dispatch(roomActions.getAllocationDetail(roomId));
    },
    allocateMeter: (meter) => {
      dispatch(meterActions.allocateMeter(meter));
    },
    deallocateMeter: (meter) => {
      dispatch(meterActions.deallocatedMeter(meter));
    },
    getRoom: (roomId) => {
      dispatch(roomActions.getRoom(roomId));
    }
  };
};

const mapStateToProps = (state) => {
  return {
    meterList: state.meters.meterList,
    roomMeterDetail: state.rooms.roomMeterDetail,
    roomDetail: state.rooms.selectedRoomDetail,
  };
};

const x =
  connect(mapStateToProps, mapDispatchprops)(AllocatedMeter);

export { x as AllocatedMeter };
