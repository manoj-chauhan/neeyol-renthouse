const LOCAL_STORAGE_KEY_AUTH_TOKEN = 'auth_token';

export const userData = {
    getAuthToken,
    setAuthToken,
    clear
}

function getAuthToken() {
    return localStorage.getItem(LOCAL_STORAGE_KEY_AUTH_TOKEN);
}

function setAuthToken(token) {
    localStorage.setItem(LOCAL_STORAGE_KEY_AUTH_TOKEN, token);
}

function clear() {
    localStorage.clear();
}