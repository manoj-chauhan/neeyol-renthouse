import { buildingService } from "../_services";
import { alertActions } from "./";
import { buildingConstants, loaderConstant } from "../_constants";
import { history } from "../_helpers";
import handleServiceError from "./error.handler";
import { fileUtil } from "../_services/file.service";

export const buildingActions = {
  getBuildings,
  getBuilding,
  createBuilding,
  updateBuilding,
  removeBuilding,
  uploadBuildingPhoto,
  uploadBuildingPhotoWithoutBuildingId,
  addBuildingWizard,
};

function getBuildings() {
  function request(buildingList) {
    return { type: buildingConstants.LOAD_BUILDINGS_REQUEST, buildingList };
  }
  function success(buildingList) {
    return { type: buildingConstants.LOAD_BUILDINGS_SUCCESS, buildingList };
  }
  function failure(error) {
    return { type: buildingConstants.LOAD_BUILDINGS_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getAllBuildings().then(
      (buildingList) => {
        dispatch(success(buildingList));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
function getBuilding(id) {
  function request(building) {
    return { type: buildingConstants.LOAD_BUILDING_REQUEST, building };
  }
  function success(building) {
    return { type: buildingConstants.LOAD_BUILDING_SUCCESS, building };
  }
  function failure(error) {
    return { type: buildingConstants.LOAD_BUILDING_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getBuilding(id).then(
      (building) => {
        dispatch(success(building));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function createBuilding(building) {
  return (dispatch) => {
    if (building.imageId) {
      buildingService.createBuilding(building, building.imageId).then(
        (response) => {
          history.push("/building");
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    } else {
      buildingService.createBuilding(building).then(
        (response) => {
          history.push("/building");
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function updateBuilding(building) {
  return (dispatch) => {
    buildingService.updateBuilding(building).then(
      (response) => {
        history.push("/building/");
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
function removeBuilding() {}

function uploadBuildingPhoto(buildingInfo) {
  function request() {
    return { type: buildingConstants.UPLOAD_BUILDING_PHOTO_REQUEST };
  }
  function success(buildingImage, caption) {
    return {
      type: buildingConstants.UPLOAD_BUILDING_PHOTO_SUCCESS,
      buildingImage,
      caption,
    };
  }
  function failure(error) {
    return { type: buildingConstants.UPLOAD_BUILDING_PHOTO_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request());
    fileUtil.uploadFile(buildingInfo.image).then((response) => {
      const imageId = response.photoId;
      buildingService
        .uploadBuildingPhoto(
          buildingInfo.caption,
          buildingInfo.buildingId,
          imageId
        )
        .then(
          (response) => {
            dispatch(success(imageId, buildingInfo.caption));
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
    });
  };
}

function uploadBuildingPhotoWithoutBuildingId(buildingPhotoId) {
  function loaderSuccess(isLoaderVisible) {
    return {
      type: buildingConstants.IMAGE_LOADER_VISIBLE_SUCCESS,
      isLoaderVisible,
    };
  }

  function success(buildingImageId) {
    return {
      type: buildingConstants.UPLOAD_BUILDING_IMAGE_WITHOUT_BUILDING_ID,
      buildingImageId,
    };
  }

  return (dispatch) => {
    dispatch(loaderSuccess(true));
    fileUtil.uploadFile(buildingPhotoId).then((response) => {
      const imageId = response.photoId;
      dispatch(success(imageId));
    });
  };
}

function addBuildingWizard(buildingWizardDetails) {
  return (dispatch) => {
    buildingService.addBuildindWizard(buildingWizardDetails).then(
      (response) => {
        history.push("/");
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
