import { buildingService } from "../_services";
import { alertActions } from "./";
import { roomConstants } from "../_constants";
import { history } from "../_helpers";
import handleServiceError from "./error.handler";
import { fileUtil } from "../_services/file.service";
import { RoomService } from "@material-ui/icons";
import { tenantActions } from "./tenant.actions";

export const roomActions = {
  getRooms,
  getRoom,
  addRoom,
  updateRoom,
  allocate,
  deallocate,
  getAllocationHistory,
  getAllocationDetail,
  removeRoom,
  getRoomMeterAllocationDetail,
  uploadRoomImage,
  getRoomList,
};

function getRooms(buildingId) {
  function request(roomList) {
    return { type: roomConstants.LOAD_ROOMS_REQUEST, roomList };
  }
  function success(roomList) {
    return { type: roomConstants.LOAD_ROOMS_SUCCESS, roomList };
  }
  function failure(error) {
    return { type: roomConstants.LOAD_ROOMS_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getRooms(buildingId).then(
      (roomList) => {
        dispatch(success(roomList));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getRoom(roomId) {
  function request(room) {
    return { type: roomConstants.LOAD_ROOM_DETAIL_REQUEST, room };
  }
  function success(room) {
    return { type: roomConstants.LOAD_ROOM_DETAIL_SUCCESS, room };
  }
  function failure(error) {
    return { type: roomConstants.LOAD_ROOM_DETAIL_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getRoom(roomId).then(
      (roomDetail) => {
        dispatch(success(roomDetail));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
function addRoom(room) {
  return (dispatch) => {
    if (room.roomImage) {
      fileUtil.uploadFile(room.roomImage).then((response) => {
        const photoId = response.photoId;
        buildingService.addRoom(room, photoId).then(
          (response) => {
            history.push("/building/" + room.buildingId);
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
      });
    } else {
      buildingService.addRoom(room).then(
        (response) => {
          history.push("/building/" + room.buildingId);
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}
function updateRoom(roomInfo) {
  return (dispatch) => {
    buildingService.updateRoom(roomInfo).then(
      (response) => {
        dispatch(getRoom(roomInfo.roomId));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function allocate(room) {
  return (dispatch) => {
    if (room.imageId) {
      fileUtil.uploadFile(room.imageId).then((response) => {
        const imageId = response.photoId;
        buildingService.allocateRoom(room, imageId).then(
          (response) => {
            dispatch(getAllocationDetail(room.roomId));
            dispatch(getAllocationHistory(room.roomId));
            dispatch(getRoom(room.roomId));
            dispatch(
              tenantActions.getSelectedTenantAllocatedRooms(room.tenantId)
            );
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
      });
    } else {
      buildingService.allocateRoom(room).then(
        (response) => {
          dispatch(getAllocationDetail(room.roomId));
          dispatch(getAllocationHistory(room.roomId));
          dispatch(getRoom(room.roomId));
          dispatch(
            tenantActions.getSelectedTenantAllocatedRooms(room.tenantId)
          );
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function deallocate(room) {
  return (dispatch) => {
    if (room.imageId) {
      fileUtil.uploadFile(room.imageId).then((response) => {
        const imageId = response.photoId;
        buildingService.deallocateRoom(room, imageId).then(
          (response) => {
            dispatch(getAllocationDetail(room.roomId));
            dispatch(getAllocationHistory(room.roomId));
            dispatch(getRoom(room.roomId));
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
      });
    } else {
      buildingService.deallocateRoom(room).then(
        (response) => {
          dispatch(getAllocationDetail(room.roomId));
          dispatch(getAllocationHistory(room.roomId));
          dispatch(getRoom(room.roomId));
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function getAllocationHistory(roomId) {
  function request(allocationHistory) {
    return {
      type: roomConstants.LOAD_ALLOCATION_HISTORY_REQUEST,
      allocationHistory,
    };
  }
  function success(allocationHistory) {
    return {
      type: roomConstants.LOAD_ALLOCATION_HISTORY_SUCCESS,
      allocationHistory,
    };
  }
  function failure(error) {
    return { type: roomConstants.LOAD_ALLOCATION_HISTORY_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getRoomAllocationHistory(roomId).then(
      (allocationHistory) => {
        dispatch(success(allocationHistory));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getAllocationDetail(roomId) {
  function request(allocationDetail) {
    return {
      type: roomConstants.LOAD_ALLOCATION_DETAIL_REQUEST,
      allocationDetail,
    };
  }
  function success(allocationDetail) {
    return {
      type: roomConstants.LOAD_ALLOCATION_DETAIL_SUCCESS,
      allocationDetail,
    };
  }
  function failure(error) {
    return { type: roomConstants.LOAD_ALLOCATION_DETAIL_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getRoomAllocationDetail(roomId).then(
      (allocationDetail) => {
        dispatch(success(allocationDetail));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        // dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function removeRoom() {}

function getRoomMeterAllocationDetail(roomId) {
  function request(allocationDetail) {
    return {
      type: roomConstants.LOAD_ROOM_METER_ALLOCATION_REQUEST,
      allocationDetail,
    };
  }
  function success(allocationDetail) {
    return {
      type: roomConstants.LOAD_ROOM_METER_ALLOCATION_SUCCESS,
      allocationDetail,
    };
  }
  function failure(error) {
    return { type: roomConstants.LOAD_ROOM_METER_ALLOCATION_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getRoomMeterDetail(roomId).then(
      (meterDetail) => {
        dispatch(success(meterDetail));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function uploadRoomImage(roomInfo) {
  function request() {
    return { type: roomConstants.UPLOAD_ROOM_PHOTO_REQUEST };
  }
  function success(roomImage, caption) {
    return {
      type: roomConstants.UPLOAD_ROOM_PHOTO_SUCCESS,
      roomImage,
      caption,
    };
  }
  function failure(error) {
    return { type: roomConstants.UPLOAD_ROOM_PHOTO_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request());
    fileUtil.uploadFile(roomInfo.image).then((response) => {
      const imageId = response.photoId;
      buildingService
        .uploadRoomImage(roomInfo.roomId, imageId, roomInfo.caption)
        .then(
          (response) => {
            dispatch(success(imageId, roomInfo.caption));
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
    });
  };
}

function getRoomList() {
  function request(roomList) {
    return { type: roomConstants.LOAD_ROOMSLIST_REQUEST, roomList };
  }
  function success(roomList) {
    return { type: roomConstants.LOAD_ROOMSLIST_SUCCESS, roomList };
  }
  function failure(error) {
    return { type: roomConstants.LOAD_ROOMSLIST_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    buildingService.getRoomList().then(
      (roomList) => {
        dispatch(success(roomList));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
