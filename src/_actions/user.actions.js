import { userConstants } from "../_constants";
import { userService } from "../_services";
import { alertActions } from "./";
import { history } from "../_helpers";
import handleServiceError from "./error.handler";
import { fileUtil } from "../_services/file.service";

export const userActions = {
  login,
  logout,
  register,
  userDetail,
  uploadUserPhoto,
  changeUserDetails,
  changePassword,
};

function login(username, password) {
  return (dispatch) => {
    // dispatch(request({ username }));
    userService.login(username, password).then(
      (user) => {
        // dispatch(success(user));
        history.push("/");
      },
      (error) => {
        // dispatch(failure(error.toString()));
        dispatch(alertActions.clear());
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  userService.logout();
  history.push("/auth/login");
  return { type: userConstants.LOGOUT };
}

function register(user) {
  function request(user) {
    return { type: userConstants.REGISTER_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.REGISTER_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.REGISTER_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request(user));
    if (user.profileImage) {
      fileUtil.uploadFile(user.profileImage).then((response) => {
        userService.register(user, response.photoId).then(
          (user) => {
            dispatch(success());
            history.push("/auth/login");
            dispatch(alertActions.success("Registration successful"));
          },
          (error) => {
            dispatch(failure(error.toString()));
            dispatch(alertActions.clear());
            dispatch(alertActions.error(error.toString()));
          }
        );
      });
    } else {
      userService.register(user).then(
        (user) => {
          dispatch(success());
          history.push("/auth/login");
          dispatch(alertActions.success("Registration successful"));
        },
        (error) => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.clear());
          dispatch(alertActions.error("Please refresh the page"));
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function userDetail() {
  function request(userDetail) {
    return { type: userConstants.USER_DETAIL_REQUEST, userDetail };
  }
  function success(userDetail) {
    return { type: userConstants.USER_DETAIL_SUCCESS, userDetail };
  }
  function failure(error) {
    return { type: userConstants.USER_DETAIL_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request());

    userService.userDetail().then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function uploadUserPhoto(userInfo) {
  function request(imageLoaderVisible) {
    return {
      type: userConstants.UPLOAD_USER_PHOTO_REQUEST,
      imageLoaderVisible,
    };
  }
  function success(userImage, imageLoaderVisible) {
    const userImageWithLoaderDetails = {
      userImage: userImage,
      imageLoaderVisible: imageLoaderVisible,
    };
    return {
      type: userConstants.UPLOAD_USER_PHOTO_SUCCESS,
      userImageWithLoaderDetails,
    };
  }
  function failure(error) {
    return { type: userConstants.UPLOAD_USER_PHOTO_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request(true));
    userService.uploadUserPhoto(userInfo).then(
      (response) => {
        dispatch(success(response.photoId, false));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function changeUserDetails(userInfo) {
  return (dispatch) => {
    userService.changeUserDetails(userInfo).then(
      (response) => {
        dispatch(userDetail());
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function changePassword(changePasswordInfo) {
  return (dispatch) => {
    userService.changePassword(changePasswordInfo).then(
      (response) => {},
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
