import { settingConstants } from "../_constants";
import { settingService } from "../_services";
import { alertActions } from "./alert.actions";
import handleServiceError from "./error.handler";

export const settingActions = {
  getSetting,
  setSetting,
};

function getSetting() {
  function request(setting) {
    return { type: settingConstants.USER_GET_SETTING_REQUEST, setting };
  }
  function success(userSettings) {
    return { type: settingConstants.USER_GET_SETTING_SUCCESS, userSettings };
  }
  function failure(error) {
    return { type: settingConstants.USER_GET_SETTING_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request());
    settingService.getSetting().then(
      (userSettings) => {
        dispatch(success(userSettings));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        // dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function setSetting(setting) {
  return (dispatch) => {
    settingService.setSetting(setting).then(
      (response) => {
        dispatch(getSetting());
      },
      (error) => {}
    );
  };
}
