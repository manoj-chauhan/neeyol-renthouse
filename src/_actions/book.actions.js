import handleServiceError from "./error.handler";
import { alertActions } from "./";
import { bookService } from "../_services";
import { history } from "../_helpers";
import { bookConstants } from "../_constants";

export const bookActions = {
  recordPayment,
  recordMaintanance,
  recordUtility,
  recordBalance,
  getPayments,
  getTransactions,
  getStatement,
  getMaintanance,
  getUtility,
  getDiscount,
  getRoomList,
  clearPaymentList,
  clearTransactions,
  clearUtilityList,
  clearMaintananceList,
};

function recordPayment(tenantId, amount, comment, Reading, Discount) {
  return (dispatch) => {
    bookService
      .recordPayment(tenantId, amount, comment, Reading, Discount)
      .then(
        (response) => {
          history.push("/tenant/" + tenantId);
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
  };
}
function recordMaintanance(maintanance) {
  return (dispatch) => {
    bookService.recordMaintanance(maintanance).then(
      (response) => {
        dispatch(clearMaintananceList());
        dispatch(getMaintanance(0));
      },
      (error) => {
        handleServiceError(error);
      }
    );
  };
}

function recordUtility(utility) {
  return (dispatch) => {
    bookService.recordUtility(utility).then(
      (response) => {
        dispatch(clearUtilityList());
        dispatch(getUtility());
      },
      (error) => {
        handleServiceError(error);
      }
    );
  };
}

function recordBalance(tenantBalanceDetails) {
  return (dispatch) => {
    bookService.recordBalance(tenantBalanceDetails).then(
      (response) => {},
      (error) => {
        handleServiceError(error);
      }
    );
  };
}

function getPayments(pageNo) {
  function request(paymentList) {
    return { type: bookConstants.LOAD_PAYMENTS_REQUEST, paymentList };
  }
  function success(paymentList) {
    return { type: bookConstants.LOAD_PAYMENTS_SUCCESS, paymentList };
  }
  function failure(error) {
    return { type: bookConstants.LOAD_PAYMENTS_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));
    bookService.getPayments(pageNo).then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getTransactions(accountId, type) {
  function request(transactionList) {
    return { type: bookConstants.LOAD_TRANSACTIONS_REQUEST, transactionList };
  }
  function success(transactionList) {
    return { type: bookConstants.LOAD_TRANSACTIONS_SUCCESS, transactionList };
  }
  function failure(error) {
    return { type: bookConstants.LOAD_TRANSACTIONS_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));
    bookService.getTransactions(accountId, type).then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getMaintanance(pageNo) {
  function request(maintananceList) {
    return {
      type: bookConstants.LOAD_ACCOUNT_MAINTANANCE_REQUEST,
      maintananceList,
    };
  }
  function success(maintananceList) {
    return {
      type: bookConstants.LOAD_ACCOUNT_MAINTANANCE_SUCCESS,
      maintananceList,
    };
  }
  function failure(error) {
    return { type: bookConstants.LOAD_ACCOUNT_MAINTANANCE_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));
    bookService.getMaintanance(pageNo).then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getUtility(pageNo) {
  function request(utilityList) {
    return { type: bookConstants.LOAD_ACCOUNT_UTILITY_REQUEST, utilityList };
  }
  function success(utilityList) {
    return { type: bookConstants.LOAD_ACCOUNT_UTILITY_SUCCESS, utilityList };
  }
  function failure(error) {
    return { type: bookConstants.LOAD_ACCOUNT_UTILITY_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));
    bookService.getUtility(pageNo).then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getDiscount() {
  function request(discount) {
    return { type: bookConstants.LOAD_DISCOUNT_REQUEST, discount };
  }
  function success(discount) {
    return { type: bookConstants.LOAD_DISCOUNT_SUCCESS, discount };
  }
  function failure(error) {
    return { type: bookConstants.LOAD_DISCOUNT_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));
    bookService.getDiscount().then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getStatement(accountId, pageNo) {
  function request(transList) {
    return { type: bookConstants.LOAD_ACCOUNT_STATEMENT_REQUEST, transList };
  }
  function success(transList) {
    return { type: bookConstants.LOAD_ACCOUNT_STATEMENT_SUCCESS, transList };
  }

  return (dispatch) => {
    dispatch(request([]));
    bookService.getStatement(accountId, pageNo).then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getRoomList(tenantId) {
  function request(roomList) {
    return { type: bookConstants.LOAD_TENANT_ROOMLIST_REQUEST, roomList };
  }
  function success(roomList) {
    return { type: bookConstants.LOAD_TENANT_ROOMLIST_SUCCESS, roomList };
  }

  return (dispatch) => {
    dispatch(request([]));
    bookService.getRoomList(tenantId).then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function clearPaymentList() {
  return (dispatch) => {
    dispatch({
      type: bookConstants.CLEAR_PAYMENT_LIST,
    });
  };
}

function clearTransactions() {
  return (dispatch) => {
    dispatch({
      type: bookConstants.CLEAR_TRANSACTIONS,
    });
  };
}

function clearUtilityList() {
  return (dispatch) => {
    dispatch({
      type: bookConstants.CLEAR_UTILITY_LIST,
    });
  };
}

function clearMaintananceList() {
  return (dispatch) => {
    dispatch({
      type: bookConstants.CLEAR_MAINTANANANCE_LIST,
    });
  };
}
