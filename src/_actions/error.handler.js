import { history } from '../_helpers';
import { errors as serviceErrors} from '../_services';
import { userService } from '../_services';

export default function handleServiceError(error) {
    if(error.code === serviceErrors.AUTH_FAILURE){
        userService.logout()
        history.push('/auth/login');
    }
}