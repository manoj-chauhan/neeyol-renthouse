import { meterService } from "../_services";
import { alertActions } from "./";
import { meterConstants } from "../_constants";
import { history } from "../_helpers";
import handleServiceError from "./error.handler";
import { fileUtil } from "../_services/file.service";
import { roomActions } from "./room.actions";

export const meterActions = {
  getMeters,
  getMeter,
  addMeter,
  allocateMeter,
  deallocatedMeter,
  getAllocationHistory,
  recordReading,
  MetereReadingDetail,
  uploadReadingPhoto,
  recordMeterReading,
  clearMeterReadingList,
  updateMeter,
};

function getMeters() {
  function request(meterList) {
    return { type: meterConstants.LOAD_METER_REQUEST, meterList };
  }
  function success(meterList) {
    return { type: meterConstants.LOAD_METER_SUCCESS, meterList };
  }
  function failure(error) {
    return { type: meterConstants.LOAD_METER_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    meterService.getAllMeters().then(
      (meterList) => {
        dispatch(success(meterList));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getMeter(meterId) {
  function request(selectedMeter) {
    return { type: meterConstants.LOAD_SELECTED_METER_REQUEST, selectedMeter };
  }
  function success(selectedMeter) {
    return { type: meterConstants.LOAD_SELECTED_METER_SUCCESS, selectedMeter };
  }
  function failure(error) {
    return { type: meterConstants.LOAD_SELECTED_METER_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request({}));

    meterService.getMeter(meterId).then(
      (meter) => {
        dispatch(success(meter));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function addMeter(meter) {
  return (dispatch) => {
    meterService.addMeter(meter).then(
      (response) => {
        history.push("/meter");
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function allocateMeter(meter) {
  return (dispatch) => {
    if (meter.imageId) {
      fileUtil.uploadFile(meter.imageId).then((response) => {
        const imageId = response.photoId;
        meterService.allocateMeter(meter, imageId).then(
          (response) => {
            dispatch(roomActions.getRoomMeterAllocationDetail(meter.roomId));
            dispatch(getMeter(meter.meterId));
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
      });
    } else {
      meterService.allocateMeter(meter).then(
        (response) => {
          dispatch(roomActions.getRoomMeterAllocationDetail(meter.roomId));
          dispatch(getMeter(meter.meterId));
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function deallocatedMeter(meter) {
  return (dispatch) => {
    if (meter.imageId) {
      fileUtil.uploadFile(meter.imageId).then((response) => {
        const imageId = response.photoId;
        meterService.deallocateMeter(meter, imageId).then(
          (response) => {
            dispatch(roomActions.getRoomMeterAllocationDetail(meter.roomId));
            dispatch(getMeter(meter.meterId));
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
      });
    } else {
      meterService.deallocateMeter(meter).then(
        (response) => {
          dispatch(meterActions.getMeter(meter.meterId));
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function getAllocationHistory(meterId) {
  function request(allocationHistory) {
    return {
      type: meterConstants.LOAD_ALLOCATION_HISTORY_REQUEST,
      allocationHistory,
    };
  }
  function success(allocationHistory) {
    return {
      type: meterConstants.LOAD_ALLOCATION_HISTORY_SUCCESS,
      allocationHistory,
    };
  }
  function failure(error) {
    return { type: meterConstants.LOAD_ALLOCATION_HISTORY_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    meterService.getRoomAllocationHistory(meterId).then(
      (allocationHistory) => {
        dispatch(success(allocationHistory));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function recordReading(meterReadingInfo) {
  function request(meterReading) {
    return { type: meterConstants.LOAD_METER_READING_REQUEST, meterReading };
  }
  function success(meterReading) {
    return { type: meterConstants.LOAD_METER_READING_SUCCESS, meterReading };
  }
  function failure(error) {
    return { type: meterConstants.LOAD_METER_READING_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request());
    if (meterReadingInfo.image) {
      fileUtil.uploadFile(meterReadingInfo.image).then((response) => {
        const imageId = response.photoId;
        meterService.meterReading(meterReadingInfo, imageId).then(
          (response) => {
            dispatch(success(meterReadingInfo.Reading));
            dispatch(
              roomActions.getRoomMeterAllocationDetail(meterReadingInfo.roomId)
            );
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
      });
    } else {
      meterService.meterReading(meterReadingInfo).then(
        (response) => {
          dispatch(success(meterReadingInfo.Reading));
          dispatch(
            roomActions.getRoomMeterAllocationDetail(meterReadingInfo.roomId)
          );
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function MetereReadingDetail(meterId, pageNo) {
  function request(Reading) {
    return { type: meterConstants.LOAD_METER_READING_REQUEST, Reading };
  }
  function success(meterReading) {
    return { type: meterConstants.LOAD_METER_READING_SUCCESS, meterReading };
  }
  function failure(error) {
    return { type: meterConstants.LOAD_METER_READING_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    meterService.getMetereReadingDetail(meterId, pageNo).then(
      (meterReading) => {
        dispatch(success(meterReading));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function uploadReadingPhoto(meterReadingInfo) {
  function request() {
    return { type: meterConstants.UPLOAD_METER_READING_PHOTO_REQUEST };
  }
  function success(meterReadingInfo) {
    return {
      type: meterConstants.UPLOAD_METER_READING_PHOTO_SUCCESS,
      meterReadingInfo,
    };
  }

  return (dispatch) => {
    dispatch(request());
    meterService.uploadReadingPhoto(meterReadingInfo).then(
      (response) => {
        const meterReadingDetails = {
          meterId: meterReadingInfo.meterId,
          meterPhotoId: response.photoId,
        };
        dispatch(success(meterReadingDetails));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function recordMeterReading(meterReadingInfo) {
  return (dispatch) => {
    dispatch({
      type: meterConstants.RECORD_METER_READING_SUCCESS,
      meterReadingInfo,
    });
  };
}

function clearMeterReadingList() {
  return (dispatch) => {
    dispatch({
      type: meterConstants.CLEAR_METER_READING_LIST,
    });
  };
}

function updateMeter(meterInfo) {
  return (dispatch) => {
    meterService.updateMeter(meterInfo).then(
      (response) => {
        history.push("/meter");
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
