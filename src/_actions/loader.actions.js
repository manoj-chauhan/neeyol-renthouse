export const loaderActions = {
  loaderVisible,
};

function loaderVisible(isLoaderVisible) {
  function success(isLoaderVisible) {
    return { type: "LOADER_VISIBLE_SUCCESS", isLoaderVisible };
  }

  return (dispatch) => {
    dispatch(success(isLoaderVisible));
  };
}
