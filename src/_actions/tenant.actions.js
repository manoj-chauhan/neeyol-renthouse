import { tenantService } from "../_services";
import { history } from "../_helpers";
import handleServiceError from "./error.handler";
import { alertActions } from ".";
import { tenantConstants } from "../_constants";
import { fileUtil } from "../_services/file.service";
import uuid from "react-uuid";

export const tenantActions = {
  getTenants,
  getTenant,
  addTenant,
  getSelectedTenantAllocatedRooms,
  getTenantdues,
  getTenantDocuments,
  uploadDocument,
  uploadContact,
  getContacts,
  delImage,
  delContact,
  deleteDocument,
  getAddresses,
  uploadAddress,
  deleteAddress,
  updateAddress,
  addRelative,
  getTenantRelatives,
  deleteRelative,
  getRelationships,
  uploadPhoto,
  updateTenantProfile,
  uploadTenantPhotoWithOutTenantId,
};

function getTenants() {
  function request(tenantList) {
    return { type: tenantConstants.LOAD_TENANT_REQUEST, tenantList };
  }
  function success(tenantList) {
    return { type: tenantConstants.LOAD_TENANT_SUCCESS, tenantList };
  }
  function failure(error) {
    return { type: tenantConstants.LOAD_TENANT_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    tenantService.getAllTenant().then(
      (tenantList) => {
        dispatch(success(tenantList));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
function getTenant(tenantId) {
  function request(selectedTenant) {
    return {
      type: tenantConstants.LOAD_SELECTED_TENANT_REQUEST,
      selectedTenant,
    };
  }
  function success(selectedTenant) {
    return {
      type: tenantConstants.LOAD_SELECTED_TENANT_SUCCESS,
      selectedTenant,
    };
  }
  function failure(error) {
    return { type: tenantConstants.LOAD_SELECTED_TENANT_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request({}));

    tenantService.getTenant(tenantId).then(
      (selectedTenant) => {
        dispatch(success(selectedTenant));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getContacts(tenantId) {
  function request(contact) {
    return { type: tenantConstants.LOAD_CONTACT_REQUEST, contact };
  }
  function success(contact) {
    return { type: tenantConstants.LOAD_CONTACT_SUCCESS, contact };
  }
  function failure(error) {
    return { type: tenantConstants.LOAD_CONTACT_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request({}));

    tenantService.getContacts(tenantId).then(
      (contact) => {
        dispatch(success(contact));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function addTenant(tenant) {
  return (dispatch) => {
    if (tenant.imageId) {
      tenantService.addTenant(tenant, tenant.imageId, tenant.caption).then(
        (response) => {
          history.push("/tenant");
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    } else {
      tenantService.addTenant(tenant).then(
        (response) => {
          history.push("/tenant");
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
    }
  };
}

function getSelectedTenantAllocatedRooms(tenantId) {
  function request(allocatedRoomList) {
    return {
      type: tenantConstants.GET_ALLOCATED_ROOMS_REQUEST,
      allocatedRoomList,
    };
  }
  function success(allocatedRoomList) {
    return {
      type: tenantConstants.GET_ALLOCATED_ROOMS_SUCCESS,
      allocatedRoomList,
    };
  }
  function failure(error) {
    return { type: tenantConstants.GET_ALLOCATED_ROOMS_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    tenantService.getAllocatedRooms(tenantId).then(
      (allocatedRoomList) => {
        dispatch(success(allocatedRoomList));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getTenantdues() {
  function request(tenantDue) {
    return { type: tenantConstants.GET_TENANT_DUES_REQUEST, tenantDue };
  }
  function success(tenantDue) {
    return { type: tenantConstants.GET_TENANT_DUES_SUCCESS, tenantDue };
  }
  function failure(error) {
    return { type: tenantConstants.GET_TENANT_DUES_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    tenantService.tenantdues().then(
      (response) => {
        dispatch(success(response));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getTenantDocuments(tenantId) {
  function request(documents) {
    return { type: tenantConstants.GET_TENANT_DOCUMENTS_REQUEST, documents };
  }
  function success(documents) {
    return { type: tenantConstants.GET_TENANT_DOCUMENTS_SUCCESS, documents };
  }
  function failure(error) {
    return { type: tenantConstants.GET_TENANT_DOCUMENTS_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request([]));

    tenantService.getTenantDocuments(tenantId).then(
      (documents) => {
        dispatch(success(documents));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function uploadDocument(tenantInfo) {
  function request() {
    return { type: tenantConstants.ADD_TENANT_DOCUMENTS_REQUEST };
  }
  function success(documentsDetails) {
    return {
      type: tenantConstants.ADD_TENANT_DOCUMENTS_SUCCESS,
      documentsDetails,
    };
  }
  function failure(error) {
    return { type: tenantConstants.ADD_TENANT_DOCUMENTS_FAILURE, error };
  }

  return (dispatch) => {
    fileUtil.uploadFile(tenantInfo.image).then((response) => {
      const imageId = response.photoId;
      const tenantDocumentDetails = {
        tenantDocumentId: uuid(),
        fileId: imageId,
        description: tenantInfo.description,
      };
      tenantService
        .uploadDocument(tenantDocumentDetails, tenantInfo.tenantId)
        .then(
          (response) => {
            dispatch(success(tenantDocumentDetails));
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
    });
  };
}
function uploadContact(tenant) {
  function request() {
    return { type: tenantConstants.ADD_TENANT_CONTACT_REQUEST };
  }
  function success(contactsDetails) {
    return {
      type: tenantConstants.ADD_TENANT_CONTACT_SUCCESS,
      contactsDetails,
    };
  }
  function failure(error) {
    return { type: tenantConstants.ADD_TENANT_CONTACT_FAILURE, error };
  }

  const tenantContactDetails = {
    tenantContactId: uuid(),
    phoneNumber: tenant.contact,
    type: tenant.type,
  };

  return (dispatch) => {
    tenantService.uploadContact(tenantContactDetails, tenant.tenantId).then(
      (response) => {
        dispatch(success(tenantContactDetails));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function delImage(tenant) {
  return (dispatch) => {
    tenantService.delImage(tenant).then(
      (response) => {},
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function delContact(tenant) {
  function request() {
    return { type: tenantConstants.DELETE_TENANT_CONTACT_REQUEST };
  }
  function success(contactId) {
    return { type: tenantConstants.DELETE_TENANT_CONTACT_SUCCESS, contactId };
  }
  function failure(error) {
    return { type: tenantConstants.DELETE_TENANT_CONTACT_FAILURE, error };
  }

  return (dispatch) => {
    tenantService.delContact(tenant).then(
      (response) => {
        dispatch(success(tenant.contactId));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
function deleteDocument(tenantId, documentId) {
  function request() {
    return { type: tenantConstants.DELETE_TENANT_DOCUMENT_REQUEST };
  }
  function success(documentId) {
    return { type: tenantConstants.DELETE_TENANT_DOCUMENT_SUCCESS, documentId };
  }
  function failure(error) {
    return { type: tenantConstants.DELETE_TENANT_DOCUMENT_FAILURE, error };
  }

  return (dispatch) => {
    tenantService.deleteDocument(tenantId, documentId).then(
      (response) => {
        dispatch(success(documentId));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getAddresses(tenantId) {
  function request(addresses) {
    return { type: tenantConstants.GET_TENANT_ADDRESSES_REQUEST, addresses };
  }
  function success(addresses) {
    return { type: tenantConstants.GET_TENANT_ADDRESSES_SUCCESS, addresses };
  }
  function failure(error) {
    return { type: tenantConstants.GET_TENANT_ADDRESSES_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request({}));

    tenantService.getAddresses(tenantId).then(
      (addresses) => {
        dispatch(success(addresses));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
function uploadAddress(address) {
  function request() {
    return { type: tenantConstants.ADD_TENANT_ADDRESS_REQUEST };
  }
  function success(addresses) {
    return { type: tenantConstants.ADD_TENANT_ADDRESS_SUCCESS, addresses };
  }
  function failure(error) {
    return { type: tenantConstants.ADD_TENANT_ADDRESS_FAILURE, error };
  }

  const tenantAddressDetails = {
    tenantAddressId: uuid(),
    address: address.address,
    type: address.type,
  };

  return (dispatch) => {
    dispatch(request());
    tenantService.uploadAddress(tenantAddressDetails, address.tenantId).then(
      (response) => {
        dispatch(success(tenantAddressDetails));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function deleteAddress(tenantId, addressId) {
  function request() {
    return { type: tenantConstants.DELETE_TENANT_ADDRESS_REQUEST };
  }
  function success(addressId) {
    return { type: tenantConstants.DELETE_TENANT_ADDRESS_SUCCESS, addressId };
  }
  function failure(error) {
    return { type: tenantConstants.DELETE_TENANT_ADDRESS_FAILURE, error };
  }

  return (dispatch) => {
    tenantService.deleteAddress(tenantId, addressId).then(
      (response) => {
        dispatch(success(addressId));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function updateAddress(tenant) {
  function request() {
    return { type: tenantConstants.UPDATE_TENANT_ADDRESS_REQUEST };
  }
  function success(tenantUpdateAddress) {
    return {
      type: tenantConstants.UPDATE_TENANT_ADDRESS_SUCCESS,
      tenantUpdateAddress,
    };
  }
  function failure(error) {
    return { type: tenantConstants.UPDATE_TENANT_ADDRESS_FAILURE, error };
  }

  const tenantUpdateAddress = {
    addressId: tenant.tenantAddressId,
    type: tenant.type,
    address: tenant.address,
  };

  return (dispatch) => {
    tenantService
      .updateAddress(
        tenant.tenantId,
        tenant.tenantAddressId,
        tenant.type,
        tenant.address
      )
      .then(
        (response) => {
          dispatch(success(tenantUpdateAddress));
          dispatch(getAddresses(tenant.tenantId));
        },
        (error) => {
          handleServiceError(error);
          dispatch(alertActions.error(error.toString()));
        }
      );
  };
}

function addRelative(tenantId, relativeId, relationshipId) {
  return (dispatch) => {
    tenantService.addRelative(tenantId, relativeId, relationshipId).then(
      (response) => {
        dispatch(getTenantRelatives(tenantId));
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getTenantRelatives(tenantId) {
  function request(relatives) {
    return { type: tenantConstants.GET_TENANT_RELATIVES_REQUEST, relatives };
  }
  function success(relatives) {
    return { type: tenantConstants.GET_TENANT_RELATIVES_SUCCESS, relatives };
  }
  function failure(error) {
    return { type: tenantConstants.GET_TENANT_RELATIVES_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request({}));

    tenantService.getTenantRelatives(tenantId).then(
      (relatives) => {
        dispatch(
          success(
            relatives.map((relation) => {
              if (relation.forwardRelationTenantId.toString() == tenantId) {
                return {
                  relationshipId: relation.tenantRelationshipId,
                  tenantId: tenantId,
                  relativeId: relation.backwardRelationTenantId,
                  relativeName: relation.backwardTenantName,
                  relation: relation.backwardRelation,
                  relativePhotoId: relation.photoIdOfTenantOfBackwardRelation,
                };
              } else {
                return {
                  relationshipId: relation.tenantRelationshipId,
                  tenantId: tenantId,
                  relativeId: relation.forwardRelationTenantId,
                  relativeName: relation.forwardTenantName,
                  relation: relation.forwardRelation,
                  relativePhotoId: relation.photoIdOfTenantOfForwardRelation,
                };
              }
            })
          )
        );
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}
function deleteRelative(tenantId, relationshipId) {
  return (dispatch) => {
    tenantService.deleteRelative(tenantId, relationshipId).then(
      (response) => {},
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function getRelationships() {
  function request(relationships) {
    return { type: tenantConstants.GET_RELATIONSHIPS_REQUEST, relationships };
  }
  function success(relationships) {
    return { type: tenantConstants.GET_RELATIONSHIPS_SUCCESS, relationships };
  }
  function failure(error) {
    return { type: tenantConstants.GET_RELATIONSHIPS_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request({}));

    tenantService.getRelationships().then(
      (relationships) => {
        dispatch(success(relationships));
      },
      (error) => {
        handleServiceError(error);
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function uploadPhoto(tenantInfo) {
  function request() {
    return { type: tenantConstants.UPLOAD_TENANT_PHOTO_REQUEST };
  }
  function success(tenantImage, caption) {
    return {
      type: tenantConstants.UPLOAD_TENANT_PHOTO_SUCCESS,
      tenantImage,
      caption,
    };
  }
  function failure(error) {
    return { type: tenantConstants.UPLOAD_TENANT_PHOTO_FAILURE, error };
  }

  return (dispatch) => {
    dispatch(request());
    fileUtil.uploadFile(tenantInfo.image).then((response) => {
      const imageId = response.photoId;
      tenantService
        .uploadPhoto(tenantInfo.caption, tenantInfo.tenantId, imageId)
        .then(
          (response) => {
            dispatch(success(imageId));
            // dispatch(getTenant(tenantInfo.tenantId));
          },
          (error) => {
            handleServiceError(error);
            dispatch(alertActions.error(error.toString()));
          }
        );
    });
  };
}

function updateTenantProfile(tenantProfileInfo) {
  return (dispatch) => {
    tenantService.updateTenantProfile(tenantProfileInfo).then(
      (response) => {
        history.push("/tenant");
      },
      (error) => {
        handleServiceError(error);
        dispatch(alertActions.error(error.toString()));
      }
    );
  };
}

function uploadTenantPhotoWithOutTenantId(tenantPhotoId) {
  function loaderVisible(loaderVisible) {
    return {
      type: tenantConstants.IMAGE_LOADER_VISIBLE_SUCCESS,
      loaderVisible,
    };
  }

  function success(tenantPhotoId) {
    return {
      type: tenantConstants.UPLOAD_TENANT_IMAGE_WITHOUT_TENANT_ID,
      tenantPhotoId,
    };
  }

  return (dispatch) => {
    dispatch(loaderVisible());
    fileUtil.uploadFile(tenantPhotoId).then((response) => {
      dispatch(success(response.photoId));
    });
  };
}
