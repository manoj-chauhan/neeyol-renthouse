export default {
  // Add common config values here
  MAX_ATTACHMENT_SIZE: 5000000,
  api: {
    BASE_URL: process.env.REACT_APP_BASE_URL
  }
};
