import { roomConstants } from "../_constants";

const initialState = {
  isLoading: false,
  roomList: [],
  roomDetail: {
    name: "",
  },
  selectedRoomAllocationHistory: [],
  selectedRoomAllocationDetail: null,
  roomMeterDetail: {},
  selectedRoomDetail: {},
};

export function rooms(state = initialState, action) {
  switch (action.type) {
    case roomConstants.LOAD_ROOMS_REQUEST:
      return {
        ...state,
        isLoading: true,
        roomList: action.roomList,
      };
    case roomConstants.LOAD_ROOMS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        roomList: action.roomList,
      };
    case roomConstants.LOAD_ROOMS_FAILURE:
      return {
        ...state,
        isLoading: false,
        roomList: [],
      };
    case roomConstants.LOAD_ROOM_DETAIL_REQUEST:
      return {
        ...state,
        isLoading: true,
        selectedRoomDetail: {},
      };
    case roomConstants.LOAD_ROOM_DETAIL_SUCCESS:
      return {
        ...state,
        isLoading: false,
        selectedRoomDetail: action.room,
      };
    case roomConstants.LOAD_ROOM_DETAIL_FAILURE:
      return {
        ...state,
        isLoading: false,
        selectedRoomDetail: {},
      };
    case roomConstants.LOAD_ALLOCATION_HISTORY_REQUEST:
      return {
        ...state,
        selectedRoomAllocationHistory: [],
      };
    case roomConstants.LOAD_ALLOCATION_HISTORY_SUCCESS:
      return {
        ...state,
        selectedRoomAllocationHistory: action.allocationHistory,
      };
    case roomConstants.LOAD_ALLOCATION_HISTORY_FAILURE:
      return {
        ...state,
        selectedRoomAllocationHistory: [],
      };
    case roomConstants.LOAD_ALLOCATION_DETAIL_REQUEST:
      return {
        ...state,
        selectedRoomAllocationDetail: null,
      };
    case roomConstants.LOAD_ALLOCATION_DETAIL_SUCCESS:
      return {
        ...state,
        selectedRoomAllocationDetail: action.allocationDetail,
      };
    case roomConstants.LOAD_ALLOCATION_DETAIL_FAILURE:
      return {
        ...state,
        selectedRoomAllocationDetail: null,
      };

    case roomConstants.LOAD_ROOM_METER_ALLOCATION_REQUEST:
      return {
        ...state,
        roomMeterDetail: {},
      };
    case roomConstants.LOAD_ROOM_METER_ALLOCATION_SUCCESS:
      return {
        ...state,
        roomMeterDetail: action.allocationDetail,
      };
    case roomConstants.LOAD_ROOM_METER_ALLOCATION_FAILURE:
      return {
        ...state,
        roomMeterDetail: {},
      };
    case roomConstants.UPLOAD_ROOM_PHOTO_REQUEST:
      return {
        ...state,
      };
    case roomConstants.UPLOAD_ROOM_PHOTO_SUCCESS:
      if (state.selectedRoomDetail.imageList == null) {
        state.selectedRoomDetail.imageList = [];
      }
      return {
        ...state,
        selectedRoomDetail: {
          ...state.selectedRoomDetail,
          imageList: [
            ...state.selectedRoomDetail.imageList,
            {
              imageId: action.roomImage,
              caption: action.caption,
            },
          ],
        },
      };
    case roomConstants.UPLOAD_ROOM_PHOTO_FAILURE:
      return {
        ...state,
      };
    case roomConstants.LOAD_ROOMSLIST_REQUEST:
      return {
        ...state,
        roomList: [],
      };
    case roomConstants.LOAD_ROOMSLIST_SUCCESS:
      return {
        ...state,
        roomList: action.roomList,
      };
    case roomConstants.LOAD_ROOMSLIST_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
}
