import { userConstants } from "../_constants";

const initialState = {
  userDetail: {},
  imageLoader: false,
};

export function user(state = initialState, action) {
  switch (action.type) {
    case userConstants.USER_DETAIL_REQUEST:
      return {
        ...state,
        userDetail: {},
      };
    case userConstants.USER_DETAIL_SUCCESS:
      return {
        ...state,
        userDetail: action.userDetail,
      };
    case userConstants.REGISTER_FAILURE:
      return {
        ...state,
        userDetail: {},
      };
    case userConstants.UPLOAD_USER_PHOTO_REQUEST:
      return {
        ...state,
        imageLoader: action.imageLoaderVisible,
      };
    case userConstants.UPLOAD_USER_PHOTO_SUCCESS:
      return {
        ...state,
        userDetail: {
          ...state.userDetail,
          photoId: action.userImageWithLoaderDetails.userImage,
        },
        imageLoader: action.userImageWithLoaderDetails.imageLoaderVisible,
      };
    case userConstants.UPLOAD_USER_PHOTO_FAILURE:
      return {
        ...state,
      };
    default:
      return {
        ...state,
      };
  }
}
