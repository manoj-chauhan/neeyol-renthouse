import { settingConstants } from "../_constants";

const initialState = {
  defaultSetting: {},
};

export function setting(state = initialState, action) {
  switch (action.type) {
    case settingConstants.USER_GET_SETTING_REQUEST:
      return {
        ...state,
        defaultSetting: {},
      };
    case settingConstants.USER_GET_SETTING_SUCCESS:
      return {
        ...state,
        defaultSetting: action.userSettings,
      };
    case settingConstants.USER_GET_SETTING_FAILURE:
      return {
        ...state,
        defaultSetting: {},
      };
    default:
      return {
        ...state,
      };
  }
}
