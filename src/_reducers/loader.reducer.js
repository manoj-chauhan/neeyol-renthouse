import { loaderConstant } from "../_constants";

const initialState = {
  isLoaderVisible: false,
};

export const loader = (state = initialState, action) => {
  switch (action.type) {
    case loaderConstant.LOADER_VISIBLE_SUCCESS:
      return {
        ...state,
        isLoaderVisible: action.isLoaderVisible,
      };
    default:
      return state;
  }
};
