import { bookConstants } from "../_constants";

const initialState = {
  statement: [],
  remainingTransactions: 0,
  paymentList: [],
  tenantRoomList: [],
  maintananceList: [],
  remainingMaintenanceEntries: 0,
  utilityList: [],
  discountList: [],
  remainingUtilityEntries: 0,
};

export function book(state = initialState, action) {
  switch (action.type) {
    case bookConstants.LOAD_ACCOUNT_STATEMENT_REQUEST:
      return {
        ...state,
      };
    case bookConstants.LOAD_ACCOUNT_STATEMENT_SUCCESS:
      const remainingTrans = action.transList.remainingTransactions;
      return {
        ...state,
        statement: [...state.statement, ...action.transList.transactions],
        remainingTransactions: remainingTrans,
      };
    case bookConstants.LOAD_ACCOUNT_STATEMENT_FAILURE:
      return {
        ...state,
        statement: [],
      };
    case bookConstants.LOAD_PAYMENTS_REQUEST:
      return {
        ...state,
      };
    case bookConstants.LOAD_PAYMENTS_SUCCESS:
      return {
        ...state,
        paymentList: [...state.paymentList, ...action.paymentList],
      };
    case bookConstants.LOAD_PAYMENTS_FAILURE:
      return {
        ...state,
        paymentList: [],
      };
    case bookConstants.LOAD_TENANT_ROOMLIST_REQUEST:
      return {
        ...state,
        tenantRoomList: [],
      };
    case bookConstants.LOAD_TENANT_ROOMLIST_SUCCESS:
      return {
        ...state,
        tenantRoomList: action.roomList,
      };
    case bookConstants.LOAD_ACCOUNT_MAINTANANCE_REQUEST:
      return {
        ...state,
      };
    case bookConstants.LOAD_ACCOUNT_MAINTANANCE_SUCCESS:
      const remainingEntries = action.maintananceList.remainingTransactions;
      return {
        ...state,
        maintananceList: [
          ...state.maintananceList,
          ...action.maintananceList.transactions,
        ],
        remainingMaintenanceEntries: remainingEntries,
      };

    case bookConstants.LOAD_ACCOUNT_MAINTANANCE_FAILURE:
      return {
        ...state,
        maintananceList: [],
      };
    case bookConstants.LOAD_ACCOUNT_UTILITY_REQUEST:
      return {
        ...state,
      };
    case bookConstants.LOAD_ACCOUNT_UTILITY_SUCCESS:
      return {
        ...state,
        utilityList: [...state.utilityList, ...action.utilityList.transactions],
        remainingUtilityEntries: action.utilityList.remainingTransactions,
      };
    case bookConstants.LOAD_ACCOUNT_UTILITY_FAILURE:
      return {
        ...state,
        utilityList: [],
      };
    case bookConstants.LOAD_DISCOUNT_REQUEST:
      return {
        ...state,
        discountList: [],
      };
    case bookConstants.LOAD_DISCOUNT_SUCCESS:
      return {
        ...state,
        discountList: action.discount,
      };
    case bookConstants.LOAD_DISCOUNT_FAILURE:
      return {
        ...state,
        discountList: [],
      };
    case bookConstants.CLEAR_PAYMENT_LIST:
      return {
        ...state,
        paymentList: [],
      };
    case bookConstants.CLEAR_TRANSACTIONS:
      return {
        ...state,
        statement: [],
      };
    case bookConstants.CLEAR_UTILITY_LIST:
      return {
        ...state,
        utilityList: [],
      };
    case bookConstants.CLEAR_MAINTANANANCE_LIST:
      return {
        ...state,
        maintananceList: [],
      };
    default:
      return state;
  }
}
