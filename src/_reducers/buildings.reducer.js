import { startOfWeek } from "date-fns";
import { buildingConstants } from "../_constants";

const initialState = {
  isLoading: false,
  buildingList: [],
  building: null,
  loaderVisible: false,
  imageFieldSize: 12,
  buildingPhotoId: null,
};

export function buildings(state = initialState, action) {
  switch (action.type) {
    case buildingConstants.LOAD_BUILDINGS_REQUEST:
      return {
        ...state,
        isLoading: true,
        buildingList: action.buildingList,
      };
    case buildingConstants.LOAD_BUILDINGS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        buildingList: action.buildingList,
      };
    case buildingConstants.LOAD_BUILDINGS_FAILURE:
      return {
        ...state,
        isLoading: false,
        buildingList: [],
      };
    case buildingConstants.LOAD_BUILDING_REQUEST:
      return {
        ...state,
        building: action.building,
      };
    case buildingConstants.LOAD_BUILDING_SUCCESS:
      return {
        ...state,
        building: action.building,
      };
    case buildingConstants.LOAD_BUILDING_FAILURE:
      return {
        ...state,
        building: null,
      };
    case buildingConstants.UPLOAD_BUILDING_PHOTO_REQUEST:
      return {
        ...state,
      };
    case buildingConstants.UPLOAD_BUILDING_PHOTO_SUCCESS:
      if (state.building.imageList == null) {
        state.building.imageList = [];
      }

      return {
        ...state,
        building: {
          ...state.building,
          imageList: [
            ...state.building.imageList,
            {
              imageId: action.buildingImage,
              caption: action.caption,
            },
          ],
        },
      };
    case buildingConstants.UPLOAD_BUILDING_PHOTO_FAILURE:
      return {
        ...state,
      };
    case buildingConstants.IMAGE_LOADER_VISIBLE_SUCCESS:
      return {
        ...state,
        loaderVisible: action.isLoaderVisible,
        imageFieldSize: 10,
      };
    case buildingConstants.UPLOAD_BUILDING_IMAGE_WITHOUT_BUILDING_ID:
      return {
        ...state,
        loaderVisible: false,
        imageFieldSize: 12,
        buildingPhotoId: action.buildingImageId,
      };
    default:
      return state;
  }
}
