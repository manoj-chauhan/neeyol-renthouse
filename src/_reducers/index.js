import { combineReducers } from "redux";

import { authentication } from "./authentication.reducer";
import { buildings } from "./buildings.reducer";
import { rooms } from "./rooms.reducer";
import { meters } from "./meters.reducer";
import { tenants } from "./tenants.reducer";
import { registration } from "./registration.reducer";
import { book } from "./book.reducer";
import { alert } from "./alert.reducer";
import { user } from "./user.reducer";
import { setting } from "./setting.reducer";
import { loader } from "./loader.reducer";

const rootReducer = combineReducers({
  authentication,
  buildings,
  rooms,
  meters,
  tenants,
  registration,
  book,
  alert,
  user,
  setting,
  loader,
});

export default rootReducer;
