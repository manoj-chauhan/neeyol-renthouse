import { tenantConstants } from "../_constants";

const initialState = {
  isLoading: false,
  tenantList: [],
  allocatedRooms: [],
  selectedTenant: {},
  statement: [],
  tenantDues: [],
  tenantDocuments: [],
  tenantContacts: [],
  tenantAddresses: [],
  tenantRelatives: [],
  relationships: [],
  loaderVisible: false,
  imageFieldSize: 12,
  tenantPhotoId: null,
};

export function tenants(state = initialState, action) {
  switch (action.type) {
    case tenantConstants.LOAD_TENANT_REQUEST:
      return {
        ...state,
        tenantList: [],
      };
    case tenantConstants.LOAD_TENANT_SUCCESS:
      return {
        ...state,
        tenantList: action.tenantList,
      };
    case tenantConstants.LOAD_TENANT_FAILURE:
      return {
        ...state,
        tenantList: [],
      };
    case tenantConstants.GET_ALLOCATED_ROOMS_REQUEST:
      return {
        ...state,
        allocatedRooms: [],
      };
    case tenantConstants.GET_ALLOCATED_ROOMS_SUCCESS:
      return {
        ...state,
        allocatedRooms: action.allocatedRoomList,
      };
    case tenantConstants.GET_ALLOCATED_ROOMS_FAILURE:
      return {
        ...state,
        allocatedRooms: [],
      };
    case tenantConstants.LOAD_SELECTED_TENANT_REQUEST:
      return {
        ...state,
        selectedTenant: {},
      };
    case tenantConstants.LOAD_SELECTED_TENANT_SUCCESS:
      return {
        ...state,
        selectedTenant: action.selectedTenant,
      };
    case tenantConstants.LOAD_SELECTED_TENANT_FAILURE:
      return {
        ...state,
        selectedTenant: {},
      };
    case tenantConstants.GET_TENANT_DUES_REQUEST:
      return {
        ...state,
        tenantDues: [],
      };
    case tenantConstants.GET_TENANT_DUES_SUCCESS:
      return {
        ...state,
        tenantDues: action.tenantDue,
      };
    case tenantConstants.GET_TENANT_DUES_FAILURE:
      return {
        ...state,
        tenantDues: [],
      };
    case tenantConstants.GET_TENANT_DOCUMENTS_REQUEST:
      return {
        ...state,
        tenantDocuments: [],
      };
    case tenantConstants.GET_TENANT_DOCUMENTS_SUCCESS:
      return {
        ...state,
        tenantDocuments: action.documents,
      };
    case tenantConstants.GET_TENANT_DOCUMENTS_FAILURE:
      return {
        ...state,
        tenantDocuments: [],
      };
    case tenantConstants.LOAD_CONTACT_REQUEST:
      return {
        ...state,
        tenantContacts: [],
      };
    case tenantConstants.LOAD_CONTACT_SUCCESS:
      return {
        ...state,
        tenantContacts: action.contact,
      };
    case tenantConstants.LOAD_CONTACT_FAILURE:
      return {
        ...state,
        tenantContacts: [],
      };
    case tenantConstants.GET_TENANT_ADDRESSES_REQUEST:
      return {
        ...state,
        tenantAddresses: [],
      };
    case tenantConstants.GET_TENANT_ADDRESSES_SUCCESS:
      return {
        ...state,
        tenantAddresses: action.addresses,
      };
    case tenantConstants.GET_TENANT_ADDRESSES_FAILURE:
      return {
        ...state,
        tenantAddresses: [],
      };
    case tenantConstants.GET_TENANT_RELATIVES_REQUEST:
      return {
        ...state,
        tenantRelatives: [],
      };
    case tenantConstants.GET_TENANT_RELATIVES_SUCCESS:
      return {
        ...state,
        tenantRelatives: action.relatives,
      };
    case tenantConstants.GET_TENANT_RELATIVES_FAILURE:
      return {
        ...state,
        tenantRelatives: [],
      };
    case tenantConstants.GET_RELATIONSHIPS_REQUEST:
      return {
        ...state,
        relationships: [],
      };
    case tenantConstants.GET_RELATIONSHIPS_SUCCESS:
      return {
        ...state,
        relationships: action.relationships,
      };
    case tenantConstants.GET_RELATIONSHIPS_FAILURE:
      return {
        ...state,
        relationships: [],
      };
    case tenantConstants.UPLOAD_TENANT_PHOTO_REQUEST:
      return {
        ...state,
      };
    case tenantConstants.UPLOAD_TENANT_PHOTO_SUCCESS:
      if (state.selectedTenant.imageList == null) {
        state.selectedTenant.imageList = [];
      }
      const array = [
        ...state.selectedTenant.imageList,
        {
          imageId: action.tenantImage,
          caption: action.caption,
        },
      ];

      return {
        ...state,
        selectedTenant: {
          ...state.selectedTenant,
          imageList: array,
        },
      };
    case tenantConstants.UPLOAD_TENANT_PHOTO_FAILURE:
      return {
        ...state,
      };
    case tenantConstants.ADD_TENANT_ADDRESS_REQUEST:
      return {
        ...state,
      };
    case tenantConstants.ADD_TENANT_ADDRESS_SUCCESS:
      return {
        ...state,
        tenantAddresses: [...state.tenantAddresses, action.addresses],
      };
    case tenantConstants.ADD_TENANT_ADDRESS_FAILURE:
      return {
        ...state,
      };
    case tenantConstants.DELETE_TENANT_ADDRESS_SUCCESS:
      return {
        ...state,
        tenantAddresses: state.tenantAddresses.filter(
          (address) => address.tenantAddressId !== action.addressId
        ),
      };
    case tenantConstants.UPDATE_TENANT_ADDRESS_SUCCESS:
      return {
        ...state,
        tenantAddresses: state.tenantAddresses.map((address) => {
          if (
            address.tenantAddressId ===
            action.tenantUpdateAddress.tenantAddressId
          ) {
            return {
              tenantAddressId: action.tenantUpdateAddress.tenantAddressId,
              type: action.tenantUpdateAddress.type,
              address: action.tenantUpdateAddress.address,
            };
          }
          return address;
        }),
      };
    case tenantConstants.ADD_TENANT_CONTACT_SUCCESS:
      return {
        ...state,
        tenantContacts: [...state.tenantContacts, action.contactsDetails],
      };
    case tenantConstants.DELETE_TENANT_CONTACT_SUCCESS:
      return {
        ...state,
        tenantContacts: state.tenantContacts.filter(
          (contactsDetails) =>
            contactsDetails.tenantContactId !== action.contactId
        ),
      };
    case tenantConstants.ADD_TENANT_DOCUMENTS_SUCCESS:
      return {
        ...state,
        tenantDocuments: [...state.tenantDocuments, action.documentsDetails],
      };
    case tenantConstants.DELETE_TENANT_DOCUMENT_SUCCESS:
      return {
        ...state,
        tenantDocuments: state.tenantDocuments.filter(
          (documentDetails) =>
            documentDetails.tenantDocumentId !== action.documentId
        ),
      };
    case tenantConstants.IMAGE_LOADER_VISIBLE_SUCCESS:
      return {
        ...state,
        loaderVisible: true,
        imageFieldSize: 10,
      };
    case tenantConstants.UPLOAD_TENANT_IMAGE_WITHOUT_TENANT_ID:
      return {
        ...state,
        loaderVisible: false,
        imageFieldSize: 12,
        tenantPhotoId: action.tenantPhotoId,
      };
    default:
      return state;
  }
}
