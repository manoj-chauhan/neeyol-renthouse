import { meterConstants } from "../_constants";

const initialState = {
  isLoading: false,
  meterList: [],
  selectedMeter: {
    id: 0,
    name: " ",
    currentReading: "0000",
    allocationDetail: null,
  },
  selectedmeterAllocationHistory: [],
  selectedMeterReadingList: [],
  meterReading: [],
};

export function meters(state = initialState, action) {
  switch (action.type) {
    case meterConstants.LOAD_METER_REQUEST:
      return {
        ...state,
        isLoading: true,
        meterList: action.meterList,
      };
    case meterConstants.LOAD_METER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        meterList: action.meterList,
      };
    case meterConstants.LOAD_METER_FAILURE:
      return {
        ...state,
        isLoading: false,
        meterList: [],
      };
    case meterConstants.LOAD_SELECTED_METER_REQUEST:
      return {
        ...state,
        selectedMeter: {
          id: 0,
          name: " ",
          currentReading: 11,
          allocationDetail: null,
        },
      };
    case meterConstants.LOAD_SELECTED_METER_SUCCESS:
      return {
        ...state,
        selectedMeter: action.selectedMeter,
      };
    case meterConstants.LOAD_SELECTED_METER_FAILURE:
      return {
        ...state,
        isLoading: false,
        selectedMeter: {
          id: 0,
          name: " ",
          currentReading: 11,
          allocationDetail: null,
        },
      };
    case meterConstants.LOAD_ALLOCATION_HISTORY_REQUEST:
      return {
        ...state,
        selectedmeterAllocationHistory: [],
      };
    case meterConstants.LOAD_ALLOCATION_HISTORY_SUCCESS:
      return {
        ...state,
        selectedmeterAllocationHistory: action.allocationHistory,
      };
    case meterConstants.LOAD_ALLOCATION_HISTORY_FAILURE:
      return {
        ...state,
        selectedmeterAllocationHistory: [],
      };
    case meterConstants.LOAD_METER_READING_REQUEST:
      return {
        ...state,
      };
    case meterConstants.LOAD_METER_READING_SUCCESS:
      return {
        ...state,
        selectedMeterReadingList: [
          ...state.selectedMeterReadingList,
          ...action.meterReading,
        ],
      };
    case meterConstants.LOAD_METER_READING_FAILURE:
      return {
        ...state,
        selectedMeterReadingList: [],
      };
    case meterConstants.UPLOAD_METER_READING_PHOTO_REQUEST:
      return {
        ...state,
      };
    case meterConstants.UPLOAD_METER_READING_PHOTO_SUCCESS:
    case meterConstants.RECORD_METER_READING_SUCCESS:
      const readings = state.meterReading.filter((r) => {
        return r.meterId == action.meterReadingInfo.meterId;
      });

      const meterReadingList =
        readings.length == 0
          ? [...state.meterReading, action.meterReadingInfo]
          : state.meterReading.map((r) => {
              if (r.meterId == action.meterReadingInfo.meterId) {
                return {
                  meterId: r.meterId,
                  meterReading: action.meterReadingInfo.reading
                    ? action.meterReadingInfo.reading
                    : r.meterReading,
                  meterPhotoId: action.meterReadingInfo.meterPhotoId
                    ? action.meterReadingInfo.meterPhotoId
                    : r.meterPhotoId,
                  // isSelected : (action.meterReadingInfo.isSelected) ? action.meterReadingInfo.isSelected : r.isSelected
                };
              }
              return r;
            });

      return {
        ...state,
        meterReading: meterReadingList,
      };
    case meterConstants.CLEAR_METER_READING_LIST:
      return {
        ...state,
        selectedMeterReadingList: [],
      };
    default:
      return state;
  }
}
