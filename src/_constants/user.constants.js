export const userConstants = {
    REGISTER_REQUEST: 'USERS_REGISTER_REQUEST',
    REGISTER_SUCCESS: 'USERS_REGISTER_SUCCESS',
    REGISTER_FAILURE: 'USERS_REGISTER_FAILURE',

    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    
    LOGOUT: 'USERS_LOGOUT',

    USER_DETAIL_REQUEST:'USER_DETAIL_REQUEST',
    USER_DETAIL_SUCCESS:'USER_DETAIL_SUCCESS',
    USER_DETAIL_FAILURE:'USER_DETAIL_FAILURE',

    UPLOAD_USER_PHOTO_REQUEST: 'UPLOAD_USER_PHOTO_REQUEST',
    UPLOAD_USER_PHOTO_SUCCESS: 'UPLOAD_USER_PHOTO_SUCCESS',
    UPLOAD_USER_PHOTO_FAILURE: 'UPLOAD_USER_PHOTO_FAILURE'


};