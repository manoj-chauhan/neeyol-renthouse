export const settingConstants = {

    USER_GET_SETTING_REQUEST:'USER_GET_SETTING_REQUEST',
    USER_GET_SETTING_SUCCESS:'USER_GET_SETTING_SUCCESS',
    USER_GET_SETTING_FAILURE:'USER_GET_SETTING_FAILURE',

};